﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;



public class AudioManager : MonoBehaviour
{

	public AudioClip debugAudioClip;

	static AudioManager _instance;
	public static AudioManager Instance {
		get { return _instance; }
	}

	AudioSource[] sources = new AudioSource[40];


	void Awake() {
		_instance = this;
	
		for (int i = 0; i < sources.Length; i++) {
			sources[i] = gameObject.AddComponent<AudioSource>();
			sources[i].playOnAwake = false;
		}
	}

	public void ReceiveAudioEvent(AudioObject audioObject) {
		print("#AudioEvent# " + audioObject.name);
		var source = GetFreeAudioSource();
		if(source == null) return;
		var clip = audioObject.audioClip;
		if(clip == null) return;
		source.clip = clip;
		source.volume = audioObject.volume;
		source.pitch = audioObject.pitch;
		if(audioObject.playbackDelay > 0)
			source.PlayDelayed(audioObject.playbackDelay);
		else
			source.Play();
	}

	AudioSource GetFreeAudioSource() {
		foreach (var item in sources) {
			if(!item.isPlaying) return item;
		}
		return null;
	}
}

