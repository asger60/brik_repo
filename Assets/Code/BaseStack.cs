﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BaseStack : MonoBehaviour {
	
	public List<BlockObject> blockObjects = new List<BlockObject>();
	public int stackSort;

	public float _overlapWidth = 28;
	public float overlapWidth {
		get {
			return _overlapWidth * ScreenManager.Instance.scale;
		}
		set {
			_overlapWidth = value;
		}
	}



	public virtual bool CanAdd(BlockObject blockObject) {
		return false;
	}
		

	public virtual void AddBlock(BlockObject blockObject) {
		blockObjects.Add(blockObject);
	}
		
	public virtual void RemoveBlock(BlockObject blockObject) {
		blockObjects.Remove (blockObject);
	}

	public virtual void CreateStack(BlockObject[] blocks) 
	{

	}

	public virtual void FadeOut() {
		
	}

	public virtual void StackArrive(BlockObject block) {
		
	}

	public virtual int GetOrderInStack(BlockObject block) {
		for (int i = 0; i < blockObjects.Count; i++) {
			if(blockObjects[i] == block) return (i+1) + stackSort;
		}
		return stackSort;
	}

	public virtual void UpdateDragability() {
		for (int i = blockObjects.Count-1; i >= 0; i--) {
			blockObjects[i].dragable = false;
			blockObjects[i].SetSortingOrder(i);

			if(!blockObjects[i].childObject) {
				blockObjects[i].dragable = true;
			} else {
				var childBlock = blockObjects[i].childObject;
				if(childBlock && childBlock.dragable) {
					if(childBlock.blockSettings.blockType != blockObjects[i].blockSettings.blockType) {
						if(!blockObjects[i].hidden) {
							blockObjects[i].dragable = true;
						}
					}
				}
			}
		}
	}

	public BlockObject GetTopBlock() {
		if(blockObjects.Count == 0) 
			return null;

		return blockObjects[blockObjects.Count-1];
	}

	public virtual void HighlightTopBlock(bool state) {
		if(blockObjects.Count == 0) 
			return;

		blockObjects[blockObjects.Count-1].HighLight(state);
	}

	public virtual void Reset() {
		if(this.gameObject != null)
			Destroy(this.gameObject);
	}

	public bool CanReceiveBlocks() {
		if(blockObjects.Count == 0) 
			return true;

		return true;
	}

	public virtual Vector3 GetNextStackPosition() {
		return transform.position;
	}
}
