﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class FloatMinMax {
	public float min = 1, max = 1;
	public enum ValueType {
		Random,
		Increasing,
		Decreasing
	}
	public ValueType valueType = ValueType.Random;
	float lastReturnTime = 0, lastReturnValue;

	public FloatMinMax(float defaultValue) {
		min = defaultValue;
		max = defaultValue;
	}

	public float GetValue() {
		

		switch (valueType) {
		case ValueType.Random:
			return UnityEngine.Random.Range(min, max);
		case ValueType.Increasing:
			if(SequencedEvents())
				lastReturnValue = Mathf.Lerp(lastReturnValue, max, 0.125f);
			else
				lastReturnValue = min;
			return lastReturnValue;
		case ValueType.Decreasing:
			if(SequencedEvents())
				lastReturnValue = Mathf.Lerp(lastReturnValue, min, 0.125f);
			else
				lastReturnValue = max;
			return lastReturnValue;

		}
		return UnityEngine.Random.Range(min, max);
	}

	bool SequencedEvents() {
		bool inSequence = false;
		if(lastReturnTime > Time.realtimeSinceStartup) lastReturnTime = 0;
		if(lastReturnTime + 1f > Time.realtimeSinceStartup) 
			inSequence = true;

		lastReturnTime = Time.realtimeSinceStartup;
		return inSequence;
	}
}


[CreateAssetMenu (menuName = "AudioObject")]
public class AudioObject : ScriptableObject {


	public AudioClip[] audioClips;

	public enum ClipSelectType {
		Random,
		RandomNoRepeat,
		Sequence
	}
	public ClipSelectType clipSelectType;

	public float minInterval = 0.5f;
	float lastPlay = 0;

	public FloatMinMax Volume = new FloatMinMax(1);
	public FloatMinMax Pitch = new FloatMinMax(1);

	public float playbackDelay;


	public void PostAudioEvent() {
		if(lastPlay > Time.realtimeSinceStartup) {
			ResetClipSelectCycle();
			lastPlay = 0;
		}
		if(lastPlay + minInterval < Time.realtimeSinceStartup) {
			AudioManager.Instance.ReceiveAudioEvent(this);
			lastPlay = Time.realtimeSinceStartup;
		}
	}

	public float volume {
		get {
			return Volume.GetValue();
		}
	} 

	public float pitch {
		get {
			return Pitch.GetValue();
		}
	}

	int lastReturnId = 0;
	public AudioClip audioClip {
		get {
			if(GameHandler.Instance.testBuild && audioClips.Length == 0) {
				return AudioManager.Instance.debugAudioClip;
			} 
			if(audioClips.Length == 0) 
				return null;
			if(audioClips.Length == 1) 
				return audioClips[0];
			

			switch (clipSelectType) {
			case ClipSelectType.Random:
				return audioClips[Random.Range(0,audioClips.Length-1)];
			case ClipSelectType.RandomNoRepeat:
				int returnId = Random.Range(0,audioClips.Length-1);
				while(returnId == lastReturnId) {
					returnId = Random.Range(0,audioClips.Length-1);
				}
				lastReturnId = returnId;
				return audioClips[lastReturnId];

			case ClipSelectType.Sequence:
				lastReturnId ++;
				if(lastReturnId >= audioClips.Length) lastReturnId = 1;
				return audioClips[lastReturnId-1];
			}

			return audioClips[Random.Range(0,audioClips.Length-1)];
		}
	}

	public void ResetClipSelectCycle() {
		lastReturnId = 0;
	}


}

