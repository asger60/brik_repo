﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighLighter : MonoBehaviour
{
	public Image scaleImage;
	public float hilightTime, hilightSize;
	public bool running;

	void Start() {
		scaleImage.color = Color.clear;
		running = false;
	}

	public void HighLight(Vector3 position) {
		transform.position = position;
		StartCoroutine(DoHighLight());
	}

	IEnumerator DoHighLight() {
		running = true;
		scaleImage.color = Color.white;
		float thisHilightSize = hilightSize * ScreenManager.Instance.scale;
		float t = 0;
		while (t < hilightTime) {
			t += Time.deltaTime;
			scaleImage.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one*thisHilightSize, (float)Easing.ExpoEaseOut(t, 0, 1, hilightTime));
			yield return new WaitForFixedUpdate();
		}
		scaleImage.color = Color.clear;
		running = false;
	}
}

