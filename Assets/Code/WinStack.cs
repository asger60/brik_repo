﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class WinStack : BaseStack {

	public int curId = 0, maxId;
	public BlockSettings settings;
	public bool completed;
	public Image borderImage, blockTypeIcon, fillImage;
	public Text numberText;
	public GameObject progressTemplate;
	WinstackProgress winstackProgress;
	public AudioObject[] addAudioObjects;
	AudioObject[] thisAddObjects;
	public AudioObject completeAudioObject;

	public void Init(BlockSettings settings) {
		this.settings = settings;
		curId = 0;
		maxId = (GameHandler.Instance.activeDeal as DealObject).maxBlockNumber;




		var thisColor = GameHandler.Instance.GetGameWorldSettings().greyBlock.c1;
		fillImage.color = GameHandler.Instance.GetGameWorldSettings().backgroundColor;
		borderImage.color = thisColor;
		numberText.color = thisColor;
		blockTypeIcon.color = new Color(thisColor.r, thisColor.g, thisColor.b, 0.5f);
		blockTypeIcon.sprite = GameHandler.Instance.GetGameWorldSettings().blockSprites[settings.blockType];

		thisAddObjects = new AudioObject[addAudioObjects.Length];
		for (int i = 0; i < addAudioObjects.Length; i++) {
			if(addAudioObjects[i] != null)
				thisAddObjects[i] = UnityEngine.Object.Instantiate(addAudioObjects[i]) as AudioObject;
		}

		
		CreateStack(null);
	}

	public void CreateProgressCircle() {
		var newProgress = (GameObject)Instantiate(progressTemplate, transform.position, Quaternion.identity);
		winstackProgress = newProgress.transform.GetComponent<WinstackProgress>();
		winstackProgress.Init(maxId, settings.c1);
	}

	void OnDestroy() {
		if(thisAddObjects == null) return;

		foreach (var item in thisAddObjects) {
			if(item != null)
				Destroy(item);
		}

	}


	public override bool CanAdd (BlockObject blockObject)
	{
		if(blockObject == null) return false;	
		if(blockObject.childObject != null) return false;
		if(blockObject.blockNumber == curId+1 && blockObject.blockSettings.blockType == settings.blockType) {
			return true;
		}
		return false;
	}

	public override void AddBlock (BlockObject blockObject)
	{
		base.AddBlock (blockObject);
		blockObject.dragable = false;
		blockObject.transform.SetParent(transform);
		curId++;
	}

	public void BlockArrive() {
//		if(thisAddObjects[settings.blockType] != null)
//			thisAddObjects[settings.blockType].PostAudioEvent();
		
//		winstackProgress.UpdateProgress(curId);
//		if(curId == maxId) {
//			LevelHandler.Instance.StackComplete(this);
//			if(completeAudioObject != null) completeAudioObject.PostAudioEvent();
//			StartCoroutine(AnimOut());
//		} 
	}

	IEnumerator AnimOut() {
		yield return new WaitForSeconds(0.5f);
		float t = 0;
		float animTime = 0.75f;
		Vector3 startPosition = transform.position, endPosition = startPosition - (Vector3.up*100*ScreenManager.Instance.scale);

		while (t < animTime) {
			transform.position = Vector3.Lerp (startPosition, endPosition, (float)Easing.SineEaseOut (t, 0, 1, animTime));
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}
		transform.position = endPosition;
	}


	public override Vector3 GetNextStackPosition ()
	{
		return transform.position;
	}

	public override void Reset ()
	{
		base.Reset();
		Destroy(winstackProgress.gameObject);
		curId = 0;
		//thisAddObject.ResetClipSelectCycle();
		foreach (var item in thisAddObjects) {
			if(item != null)
				Destroy(item);
		}
	}

}
