﻿using UnityEngine;
using System.Collections;

public class DevilStack : BaseStack {

	public override void CreateStack (BlockObject[] blocks)
	{
		base.CreateStack(blocks);
		foreach (var item in blocks) {
			item.AddToStack(this);
			item.devilBlock = true;
			item.dragable = false;
			item.childObject = null;
			item.inStartStack = false;
			item.transform.position = GetNextStackPosition();
			item.SetSortingOrder(blockObjects.Count);
			item.RevealSequence();
		}
		StartCoroutine(SetDragable());
	}

	IEnumerator SetDragable() {
		yield return new WaitForSeconds(1);
		foreach (var item in blockObjects) {
			item.dragable = false;
		}
		GetTopBlock().dragable = true;
	}

	public override bool CanAdd (BlockObject blockObject)
	{
		return false;
	}

	public override void AddBlock (BlockObject blockObject)
	{
		base.AddBlock (blockObject);
		blockObjects[blockObjects.Count-1].childObject = null;
	}



	public override void UpdateDragability ()
	{
		//base.UpdateDragability ();
	}

	public void RevealNextBlock() {
		if(blockObjects.Count == 0) return;
		blockObjects[blockObjects.Count-1].dragable = true;
	}

	public override void RemoveBlock (BlockObject blockObject)
	{
		base.RemoveBlock (blockObject);
		if(blockObjects.Count > 0)
			blockObjects[blockObjects.Count-1].childObject = null;
		UpdateDragability();
	}

	public override Vector3 GetNextStackPosition() {
		var returnPos = transform.position - new Vector3(transform.localScale.x,0,0) + ((Vector3.right * overlapWidth) * (blockObjects.Count));
		returnPos -= new Vector3(0,0,blockObjects.Count*2) * ScreenManager.Instance.scale;
		return returnPos;
	}
}
