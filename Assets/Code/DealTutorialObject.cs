﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu (menuName = "TutorialObject")]
public class DealTutorialObject : BaseDealObject
{
	public List<DealObject> tutorialDeals = new List<DealObject>();	
}

