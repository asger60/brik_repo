﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.UI;

public class ScreenManager : MonoBehaviour {

	public Transform canvasObject, worldHolder;

	public float scale = 1;
	public GameObject bgPlane;

	static ScreenManager _instance;
	public static ScreenManager Instance {
		get { 
			
			return _instance; 
		}
	}

	void Awake() {
		_instance = this;

	}


	public void Refresh ()
	{
		Camera.main.orthographicSize = Screen.height/2;

		worldHolder.localScale = canvasObject.localScale;
		worldHolder.position = new Vector3(-Screen.width/2, -Screen.height/2,0);


		scale = canvasObject.localScale.y;

		bgPlane.transform.localPosition = new Vector3(Screen.width/2, Screen.height/2, 0) / scale;
		bgPlane.transform.localPosition += Vector3.forward * 10;
		bgPlane.transform.localScale = (new Vector3(Screen.width, 1, Screen.height) * 0.1f) / scale;

	}

	public Vector3 GetScreenCenter() {
		return new Vector3(Screen.width/2, Screen.height/2, 0) / scale;
	}

	public Vector3 GetScreenPosition(Vector3 inputPosition) {
		return inputPosition/scale;
	}


}
	