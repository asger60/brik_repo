﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HintPathDot : MonoBehaviour
{
	public bool running;
	public Image image;

	void Start() {
		running = false;
		image.color = Color.clear;
	}

	public void EndDraw() {
		StopAllCoroutines();
		StartCoroutine(DoFadeOut(0.5f));
	}

	public void DrawDot(Vector3 position) {
		transform.position = position;
		CancelInvoke();
		image.color = Color.white;
		image.transform.localScale = Vector3.one;
		Invoke("SlowFadeOut", 0.5f);
		running = true;
	}

	void SlowFadeOut() {
		StartCoroutine(DoFadeOut(1.5f));
	}

	IEnumerator DoFadeOut(float time) {
		running = true;
		image.color = Color.white;
		image.transform.localScale = Vector3.one;
		//yield return new WaitForSeconds(0.2f);

		float t = 0, animTime = time;
		while (t < animTime) {
			//image.color = Color.Lerp(Color.white, new Color(1,1,1,0), t/animTime);
			image.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, t/animTime);
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		image.transform.localScale = Vector3.zero;

		image.color = Color.clear;
		running = false;
	}
}

