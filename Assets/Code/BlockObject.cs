﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;


public class BlockObject : MonoBehaviour
{

	public BlockSettings blockSettings;
	public int blockNumber;

	public BlockObject parentBlock;
	public BlockObject _childObject;

	public BlockObject childObject {
		get { 
			return _childObject;
		}
		set {
			if (value) value.parentBlock = this;
			//	blockTypeGridGroup.gameObject.SetActive (true);
			//else

			_childObject = value;
		}
	}



	public Transform blockTypeTransform;
	public GridLayoutGroup blockTypeGridGroup;
	//Quaternion initRoatation, winRotation;

	public Text numberText;
	public Image highLightImage, bgFrontImage, bgBackImage;
	public bool devilBlock;

	BlockObject hilightedObject;
	bool _hilighted;
	public Vector3 startPosition;

	public bool hilighted {
		get {
			return _hilighted;
		}
		set {
			HighLight (value);
			if (childObject != null)
				childObject.hilighted = value;
			
			if (hilightedObject != null) {
				hilightedObject.HighLight (value);
				if (!value)
					hilightedObject = null;
			}
			if (value && _hilighted)
				hilightedObject = ObjectSelector.Instance.selectedStacks[0].GetTopBlock ();
			
			_hilighted = value;
		}
	}

	bool _dragable;

	public bool dragable {
		get { return _dragable; }
		set { 
			hitBox.enabled = value;
			_dragable = value; 
		}
	}

	public BoxCollider hitBox;
	//public bool hasChildren;
	public Vector2 size;
	public Canvas canvasFront, canvasBack;
	public float dragZoomVal = 1.35f;
	public Transform gfxTransform;
	public bool inStartStack = true;

	public MeshRenderer objectRenderer;

	//PoleObject hoverPole;
	public BaseStack oldStack;
	BaseStack _currentStack;
	public AudioObject drawAudioObject, revealAudioObject, dropDealAudioObject, dropBlockAudioObject, dropStackAudioObject, dropWinAudioObject;


	public BaseStack currentStack {
		set { 
			_currentStack = value; 
		}
		get { 
			return _currentStack; 
		}
	}

	bool _selected;

	public bool selected {
		set { 
			if (!dragable) {
				_selected = false; 
			} else {
				if (value && !selected) {
					BeginDrag (); 
				}
				if (!value && selected) {
					EndDrag ();
				}

				_selected = value; 
			}
		}
		get { return _selected; }
	}




	bool _hidden = true;

	public bool hidden {
		get {
			return _hidden;
		}
		set {
			canvasFront.enabled = !value;
			if (value) {
				gfxTransform.rotation = Quaternion.Euler (0, 180, 0);
				dragable = false;

			} else {
				//borderImage.color = GameHandler.Instance.GetGameWorldSettings ().backgroundColor;
			}
			_hidden = value;
			SetSortingOrder(currentSortOrder);
		}
	}

	int currentSortOrder;
	public void SetSortingOrder (int order)
	{
		currentSortOrder = order;
		order *= 10;

		objectRenderer.sortingOrder = order - 1;
		canvasFront.sortingOrder = order;
		if (hidden)
			canvasBack.sortingOrder = order + 1;
		else
			canvasBack.sortingOrder = order - 2;
	}

	public Material uiMaterial;
	void Start ()
	{
		HighLight(false);
		selected = false;
		//numberText.color = blockSettings.t;
		numberText.color = Color.clear;

		foreach (var item in blockTypeTransform.GetComponentsInChildren<Image>()) {
			Destroy (item.gameObject);
		}

		var newGO = new GameObject ("Image");
		newGO.transform.SetParent (blockTypeTransform, false);
		var newImg = newGO.AddComponent<Image> ();
		newImg.sprite = GameHandler.Instance.GetGameWorldSettings ().blockSprites [blockSettings.blockType];
		newImg.color = GameHandler.Instance.colorsPalettes[blockSettings.blockType].c2;
		newImg.material = uiMaterial;
		blockTypeGridGroup.CalculateLayoutInputHorizontal ();
		blockTypeGridGroup.CalculateLayoutInputVertical ();
		Canvas.ForceUpdateCanvases ();



		numberText.text = blockNumber.ToString ();
		//bgImg.color = blockSettings.colorValue;
		gfxTransform.localRotation = Quaternion.Euler (0, 180, 0);
		SetColors ();
		highLightImage.gameObject.SetActive (false);

	}



	void SetColors ()
	{
		objectRenderer.materials [2].color = GameHandler.Instance.colorsPalettes[blockNumber].c1;
		objectRenderer.materials [1].color = GameHandler.Instance.GetGameWorldSettings ().greyBlock.c1;
		objectRenderer.materials [0].color = GameHandler.Instance.GetGameWorldSettings ().greyBlock.c1;

		bgFrontImage.color = GameHandler.Instance.colorsPalettes[blockNumber].c1;
		bgBackImage.color = GameHandler.Instance.GetGameWorldSettings ().greyBlock.c1;
		//canvasBack.transform.GetComponentInChildren<Image>().color = GameHandler.Instance.GetGameWorldSettings ().greyBlock.c1;
		//borderImage.color = GameHandler.Instance.GetGameWorldSettings ().backgroundColor;
	}

	BaseStack oldSelectedStack;
	void Update ()
	{
		if (selected) {
			var movePos = Camera.main.ScreenToWorldPoint (new Vector3 (ObjectSelector.Instance.inputPosition.x, ObjectSelector.Instance.inputPosition.y + 60, -Camera.main.transform.position.z));
			movePos.z = -100;
			DragObject (movePos, 1000);
			if (oldSelectedStack != ObjectSelector.Instance.selectedStacks[0]) {
				hilighted = false;
			}
			BaseStack selectedStack = CanAddCheck (ObjectSelector.Instance.selectedStacks.ToArray());
			if (selectedStack != null) {
				hilighted = true;
			} else {
				hilighted = false;
			}
			parentBlock = null;
			oldSelectedStack = selectedStack;
		} 
	}

	public void PingBlock() {
		StartCoroutine(DoPingBlock(0));
		StartCoroutine(DoPingBlock(0.2f));
	}

	IEnumerator DoPingBlock(float delay) {
		yield return new WaitForSeconds(delay);
		HighLight(true);
		yield return new WaitForSeconds(0.125f);
		HighLight(false);

	}

	public void HighLight (bool state)
	{
		highLightImage.gameObject.SetActive (state);
	}

	public void AddToStack (BaseStack stack)
	{
		
		bool didMove = false;
		if (stack != oldStack)
			didMove = true;
		

		currentStack = stack;

		if (stack != oldStack && oldStack is NormalStack)
			(oldStack as NormalStack).RevealNextBlock ();
		
		if (stack != oldStack && oldStack is DevilStack)
			(oldStack as DevilStack).RevealNextBlock ();
		

		oldStack = stack;

		if (stack.blockObjects.Count > 0)
			stack.blockObjects [stack.blockObjects.Count - 1].childObject = this;


		stack.AddBlock (this);

		if (didMove && childObject == null && stack is NormalStack) 
		{
			LevelHandler.Instance.AddPlayMove ();
		}
		
	}

	public void RemoveFromStack (BaseStack stack)
	{
		stack.RemoveBlock (this);
	}



	BaseStack CanAddCheck (BaseStack[] stacks)
	{
		foreach (var item in stacks) {
			if(item != null && item.CanAdd(this)) {
				return item;
			}
		}

		return null;
	}

	public void DoubleClick ()
	{
		if(hidden) 
			return;
		if (!dragable)
			return;
		if (childObject)
			return;
		

	}

	public void BeginDrag ()
	{
		RemoveFromStack (currentStack);
		if (childObject)
			childObject.BeginDrag ();

		drawAudioObject.PostAudioEvent ();
	}

	public BaseStack selectedStack;
	public void EndDrag ()
	{
		
		hilighted = false;

		selectedStack = null;
		if(parentBlock == null) {
			selectedStack = CanAddCheck (ObjectSelector.Instance.selectedStacks.ToArray());
		} else {
			selectedStack = parentBlock.selectedStack;
		}

		if (selectedStack != null) {
			AddToStack (selectedStack);
			if (selectedStack is DealStack) {
				MoveToDealStack ((DealStack)selectedStack);
			} else {
				MoveToStack (selectedStack);
			}
		} else {
			if (oldStack != null) {				
				AddToStack (oldStack);
				MoveToStack (oldStack);
			}
		}
		if (childObject)
			childObject.EndDrag ();
		
		//SetSortingOrder (currentStack.blockObjects.Count);
	}




	public void DragObject (Vector3 position, int sortOrder)
	{
		SetSortingOrder (sortOrder);
		transform.localScale = Vector3.Lerp (transform.localScale, Vector3.one * dragZoomVal, Time.deltaTime * 10);
		transform.position = Vector3.Lerp (transform.position, position, Time.deltaTime * 20);

		if (childObject) {
			childObject.DragObject ((position + Vector3.right * (40 * ScreenManager.Instance.scale)), sortOrder+10);
		}
	}


	public void DrawFailSequence (NormalStack stack)
	{
		StartCoroutine (DoDrawFailSequence (stack.GetNextStackPosition ()));
	}

	IEnumerator DoDrawFailSequence (Vector3 stackPosition)
	{
		//hidden = false;
		float t = 0, animTime = 0.5f, wiggleDuration = .4f, returnDuration = 0.5f;
		Quaternion rotateLeft = Quaternion.Euler (0, 180, -10), rotateRight = Quaternion.Euler (0, 180, 10), rotateMiddle = Quaternion.Euler (0, 180, 0);
		Vector3 startPosition = transform.position, endPosition = (stackPosition - Vector3.forward * 80) + Vector3.right * 30;
		drawAudioObject.PostAudioEvent ();
		while (t < animTime) {
			Vector3 gotoPos = Vector3.zero;
			gotoPos.x = Mathf.Lerp (startPosition.x, endPosition.x, (float)Easing.SineEaseIn (t, 0, 1, animTime));
			gotoPos.y = Mathf.Lerp (startPosition.y, endPosition.y, (float)Easing.SineEaseOut (t, 0, 1, animTime));
			gotoPos.z = Mathf.Lerp (startPosition.z, endPosition.z, (float)Easing.SineEaseInOut (t, 0, 1, animTime));
			transform.localScale = Vector3.Lerp (Vector3.one, Vector3.one * dragZoomVal, (float)Easing.SineEaseInOut (t, 0, 1, animTime));
			transform.position = gotoPos;
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}

		yield return new WaitForSeconds (0.3f);
		t = 0;
		while (t < wiggleDuration) {
			gfxTransform.rotation = Quaternion.Lerp (rotateLeft, rotateRight, ((Mathf.Sin ((t * 25) + 0.5f) + 1) * 0.5f));
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}

		t = 0;
		while (t < returnDuration) {
			Vector3 gotoPos = Vector3.zero;
			gfxTransform.rotation = Quaternion.Lerp (gfxTransform.rotation, rotateMiddle, t);
			gotoPos.x = Mathf.Lerp (endPosition.x, startPosition.x, (float)Easing.SineEaseOut (t, 0, 1, animTime));
			gotoPos.y = Mathf.Lerp (endPosition.y, startPosition.y, (float)Easing.SineEaseIn (t, 0, 1, animTime));
			gotoPos.z = Mathf.Lerp (endPosition.z, startPosition.z, (float)Easing.SineEaseInOut (t, 0, 1, animTime));
			transform.localScale = Vector3.Lerp (Vector3.one * dragZoomVal, Vector3.one, (float)Easing.SineEaseInOut (t, 0, 1, animTime));
			transform.position = gotoPos;
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
		gfxTransform.rotation = rotateMiddle;
		transform.position = startPosition;
		transform.localScale = Vector3.one;
	}


	public void DrawSequence (NormalStack stack)
	{
		AddToStack (stack);
		StartCoroutine (DoDrawSequence (stack));
	}

	IEnumerator DoDrawSequence (BaseStack stack)
	{
		SetSortingOrder (stack.GetOrderInStack(this) + 10);
		var stackPosition = stack.GetNextStackPosition ();

		float t = 0, animTime = 0.5f, rotateDuration = 0.4f, fallDuration = 0.5f;
		Quaternion rotateStart = Quaternion.Euler (0, 180, 0), rotateEnd = Quaternion.Euler (0, 0, 0);
		Vector3 startPosition = transform.position, endPosition = (stackPosition - Vector3.forward * 80) + Vector3.right * 30;
		drawAudioObject.PostAudioEvent ();
		while (t < animTime) {
			Vector3 gotoPos = Vector3.zero;
			gotoPos.x = Mathf.Lerp (startPosition.x, endPosition.x, (float)Easing.SineEaseIn (t, 0, 1, animTime));
			gotoPos.y = Mathf.Lerp (startPosition.y, endPosition.y, (float)Easing.SineEaseOut (t, 0, 1, animTime));
			gotoPos.z = Mathf.Lerp (startPosition.z, endPosition.z, (float)Easing.SineEaseInOut (t, 0, 1, animTime));
			transform.localScale = Vector3.Lerp (Vector3.one, Vector3.one * dragZoomVal, (float)Easing.SineEaseInOut (t, 0, 1, animTime));
			transform.position = gotoPos;
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}

		t = 0;
		while (t < rotateDuration) {
			gfxTransform.rotation = Quaternion.Lerp (rotateStart, rotateEnd, (float)Easing.SineEaseInOut (t, 0, 1, rotateDuration));
			if (t > rotateDuration / 2 && hidden) {
				hidden = false;
				//SetSortingOrder (stack.blockObjects.Count * 10);
			}
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}


		t = 0;
		while (t < fallDuration) {
			transform.position = Vector3.Lerp (endPosition, stackPosition, (float)Easing.QuintEaseIn (t, 0, 1, fallDuration));
			transform.localScale = Vector3.Lerp (Vector3.one * dragZoomVal, Vector3.one, (float)Easing.QuintEaseIn (t, 0, 1, fallDuration));
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
		transform.position = stackPosition;
		transform.localScale = Vector3.one;
		GetBlockSurfaceObject ().PostAudioEvent ();

		if (stack is NormalStack && stack.blockObjects.Count > 1) {
			BlockObject thisParent = (stack as NormalStack).GetParentBlock (this);
			//if (thisParent)
			//	thisParent.blockTypeGridGroup.gameObject.SetActive (false);
		}  
		dragable = true;
		SetSortingOrder (stack.GetOrderInStack(this) + 10);
	}

	public void RevealSequence ()
	{
		if (!hidden)
			return;
		
		StartCoroutine (DoRevealSequence ());
	}

	IEnumerator DoRevealSequence ()
	{
		dragable = false;
		yield return 0;
		revealAudioObject.PostAudioEvent ();

		float t = 0, rotateDuration = 0.3f, zoomDuration = .1f;
		Quaternion start = Quaternion.Euler (0, 180, 0), end = Quaternion.Euler (0, 0, 0);
		Vector3 scaleStart = Vector3.one, scaleEnd = Vector3.one * 1.4f;
		Vector3 startPosition = Vector3.zero, zoomPosition = startPosition - Vector3.forward * 80;

		while (t < 0.25f) {
			gfxTransform.localPosition = Vector3.Lerp (startPosition, zoomPosition, t / zoomDuration);
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
		t = 0;
		while (t < zoomDuration) {
			gfxTransform.localScale = Vector3.Lerp (scaleStart, scaleEnd, t / zoomDuration);
			gfxTransform.localPosition = Vector3.Lerp (startPosition, zoomPosition, t / zoomDuration);
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
		t = 0;
		while (t < rotateDuration) {
			
			if(t > rotateDuration/2 && hidden) {
				hidden = false;
				//SetSortingOrder (currentStack.blockObjects.Count);
			}
			gfxTransform.rotation = Quaternion.Lerp (start, end, t / rotateDuration);
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
		t = 0;
		while (t < zoomDuration) {
			gfxTransform.localScale = Vector3.Lerp (scaleEnd, scaleStart, t / zoomDuration);
			gfxTransform.localPosition = Vector3.Lerp (zoomPosition, startPosition, t / zoomDuration);

			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
		dragable = true;
		transform.localScale = Vector3.one;
		GetBlockSurfaceObject ().PostAudioEvent ();
	}

	void MoveToDealStack (DealStack stack)
	{
		StartCoroutine (DoScaleToDealStack ());
		StartCoroutine (DoMoveToDealStack (stack.GetBottomStackPosition ()));
		//hidden = true;
		dragable = false;
		inStartStack = true;
	}

	IEnumerator DoMoveToDealStack (Vector3 stackPosition)
	{
		float t = 0, animTime = 0.5f;
		Vector3 startPosition = transform.position - Vector3.forward * 100, hoverPosition = (stackPosition - Vector3.right * 150) - Vector3.forward * 100, endPosition = stackPosition;	
		while (t < animTime) {
			transform.position = Vector3.Lerp (startPosition, hoverPosition, (float)Easing.CubicEaseOut (t, 0, 1, animTime));
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
		transform.position = hoverPosition;
		t = 0;
		while (t < animTime) {
			transform.position = Vector3.Lerp (hoverPosition, endPosition, (float)Easing.CubicEaseIn (t, 0, 1, animTime));
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
		transform.position = endPosition;
		SetSortingOrder(0);
	}


	IEnumerator DoScaleToDealStack ()
	{

		float t = 0, animTime = 1f;
		Quaternion startRotation = Quaternion.Euler (0, 0, 0), endRotation = Quaternion.Euler (0, 180, 0);


		Vector3 startScale = transform.localScale;

		while (t < animTime) {
			
			if(t>animTime/2 && !hidden) hidden = true;
			transform.localScale = Vector3.Lerp (startScale, Vector3.one, (float)Easing.QuintEaseInOut (t, 0, 1, animTime));
			gfxTransform.rotation = Quaternion.Lerp (startRotation, endRotation, t / animTime);
			t += Time.deltaTime;

			yield return new WaitForFixedUpdate ();
		}
		transform.localScale = Vector3.one;
		gfxTransform.rotation = endRotation;
	}


	public void MoveToPosition(Vector3 position, Action callback = null) {
		StartCoroutine(DoMoveToPosition(position, callback));
	}

	IEnumerator DoMoveToPosition (Vector3 positon, Action callback = null)
	{
		yield return new WaitForSeconds(0.5f);
		float t = 0, animTime = 0.75f;
		Vector3 startPosition = transform.position;
		while (t < animTime) {
			transform.position = Vector3.Lerp(startPosition, positon, Easing.CircEaseInOut(t, 0, 1, animTime));
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
		if(callback != null) callback();
	}

	public void MoveToStack (BaseStack stack, Action callback = null)
	{
		transform.localScale = Vector3.one;
		gfxTransform.rotation = Quaternion.Euler (0, 0, 0);
		gfxTransform.localScale = Vector3.one;
		gfxTransform.localPosition = Vector3.zero;
		StopAllCoroutines ();
		StartCoroutine (DoMoveToStack (stack, callback));
	}

	IEnumerator DoMoveToStack (BaseStack stack, Action callback = null)
	{
		SetSortingOrder(stack.GetOrderInStack(this) * 100);
		Vector3 stackPosition = stack.GetNextStackPosition ();

		float t = 0, animTime = 0.5f, fallDuration = 0.25f;
		Vector3 startPosition = transform.position, hoverPosition = stackPosition - Vector3.forward * 80, endPosition = stackPosition;
		animTime = Vector3.Distance (startPosition, endPosition) * 0.001f / ScreenManager.Instance.scale;
		if (animTime < 0.1f)
			animTime = 0.1f;

		Vector3 startScale = Vector3.one * dragZoomVal;

		while (t < animTime) {
			Vector3 gotoPos = Vector3.zero;
			gotoPos.x = Mathf.Lerp (startPosition.x, hoverPosition.x, (float)Easing.SineEaseOut (t, 0, 1, animTime));
			gotoPos.y = Mathf.Lerp (startPosition.y, hoverPosition.y, (float)Easing.SineEaseIn (t, 0, 1, animTime));
			gotoPos.z = Mathf.Lerp (startPosition.z, hoverPosition.z, (float)Easing.SineEaseInOut (t, 0, 1, animTime));
			transform.position = gotoPos;
			transform.localScale = Vector3.Lerp (startScale, startScale, (float)Easing.QuintEaseIn (t, 0, 1, animTime));
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
			

		t = 0;
		while (t < fallDuration) {
			transform.position = Vector3.Lerp (hoverPosition, endPosition, (float)Easing.QuintEaseIn (t, 0, 1, fallDuration));
			transform.localScale = Vector3.Lerp (startScale, Vector3.one, (float)Easing.QuintEaseIn (t, 0, 1, fallDuration));
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
		transform.localScale = Vector3.one;
		transform.position = endPosition;



		if (stack is NormalStack && stack.blockObjects.Count > 1) {
			BlockObject thisParent = (stack as NormalStack).GetParentBlock (this);
			//if (thisParent)
			//	thisParent.blockTypeGridGroup.gameObject.SetActive (false);
		} 

		SetSortingOrder (stack.GetOrderInStack(this) + 10);
		GetBlockSurfaceObject ().PostAudioEvent ();
		stack.StackArrive (this);


		if(callback != null) callback();
	}

	AudioObject GetBlockSurfaceObject ()
	{
		if (currentStack is DealStack)
			return dropDealAudioObject;

		if (currentStack is WinStack)
			return dropWinAudioObject;

		if (currentStack.blockObjects.Count == 0)
			return dropStackAudioObject;


		return dropBlockAudioObject;

	}

}
