﻿using UnityEngine;
using System.Collections;


public class ParticleController : MonoBehaviour {
	ParticleSystem thisParticleSystem;
	ParticleSystem.Particle[] particleList;
	bool emitting;
	int[] rndNums;

	void Start () {
		thisParticleSystem = transform.GetComponent<ParticleSystem>();
	}

	public void Emit(int amount) {
		thisParticleSystem.Emit(amount);
		particleList = new ParticleSystem.Particle[thisParticleSystem.particleCount];
		emitting = true;
		rndNums = new int[particleList.Length];
		for (int i = 0; i < rndNums.Length; i++) {
			int[] values = {0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,4,4};
			int selected = values[Random.Range( 0, values.Length )];
			rndNums[i] = selected;
		}
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if(!emitting) return;
		thisParticleSystem.GetParticles(particleList);
		for(int i = 0; i < particleList.Length; ++i)
		{
			if(rndNums[i] == 0)
				particleList[i].startColor = GameHandler.Instance.GetGameWorldSettings().blockSettings[0].c1;
			if(rndNums[i] == 1)
				particleList[i].startColor = GameHandler.Instance.GetGameWorldSettings().blockSettings[2].c1;
			if(rndNums[i] == 2) {
				if(GameHandler.Instance.tutorialSequence)
					particleList[i].startColor = GameHandler.Instance.GetGameWorldSettings().blockSettings[1].c1;
				else
					particleList[i].startColor = GameHandler.Instance.GetGameWorldSettings().backgroundColor;
			}
			if(rndNums[i] == 3)
				particleList[i].startColor = Color.black;
			if(rndNums[i] == 4)
				particleList[i].startColor = Color.white;
		}   

		thisParticleSystem.SetParticles(particleList, thisParticleSystem.particleCount);
	}



}
