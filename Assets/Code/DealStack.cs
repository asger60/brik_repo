﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
public class DealStack : BaseStack
{
	//public Image[] borderImages;
	bool isRunning = false;
	//public Transform jokersLeftHolder;

	public GameObject jokerIconTemplate;
//	List<UIJokerIcon> jokers = new List<UIJokerIcon>(), freeJokers = new List<UIJokerIcon>();

	public void Init(GameWorldSettings settings, int maxBlockNumber) {
		isRunning = true;

		//var initIcons = jokersLeftHolder.GetComponentsInChildren<Image>();
//		foreach (var item in initIcons) {
//			if(item.transform == jokersLeftHolder) continue;
//			Destroy(item.gameObject);
//		}


		if(blockObjects.Count == 0 && (GameHandler.Instance.activeDeal as DealObject).maxJokers == 0) {
			//foreach (var item in borderImages) {
			//	item.color = Color.clear;
			//}
			return;
		}
		//foreach (var item in borderImages) {
		//	item.color = settings.greyBlock.c1;
		//}
		//CreateJokers();

	}

//	void CreateJokers() {
//
//		float delay = 1;
//
//		for (int i = 0; i < (GameHandler.Instance.activeDeal as DealObject).maxJokers; i++) {
//			var newJoker =(GameObject)Instantiate(jokerIconTemplate);
//			/newJoker.transform.SetParent(jokersLeftHolder.transform, false);
//			jokers.Add(newJoker.transform.GetComponent<UIJokerIcon>());
//			jokers[jokers.Count-1].Init(false, delay);
//			delay +=0.4f;
//		}
//
//		for (int i = 0; i < (GameHandler.Instance.activeDeal as DealObject).freeJokers; i++) {
//			var newJoker =(GameObject)Instantiate(jokerIconTemplate);
//			newJoker.transform.SetParent(jokersLeftHolder, false);
//			freeJokers.Add(newJoker.transform.GetComponent<UIJokerIcon>());
//			freeJokers[freeJokers.Count-1].Init(true, delay);
//			delay +=0.4f;
//		}
//
//
//
//
//		//StartCoroutine(DoCreateSequence());
//	}

//	IEnumerator DoCreateSequence() {
//		yield return new WaitForSeconds(1);
//		float pause = 0.4f;
//		for (int i = 0; i < (GameHandler.Instance.activeDeal as DealObject).maxJokers; i++) {
//			var newJoker =(GameObject)Instantiate(jokerIconTemplate);
//			newJoker.transform.SetParent(jokersLeftHolder.transform, false);
//			jokers.Add(newJoker.transform.GetComponent<UIJokerIcon>());
//			jokers[jokers.Count-1].Init(false);
//			yield return new WaitForSeconds(pause);
//		}
//		for (int i = 0; i < (GameHandler.Instance.activeDeal as DealObject).freeJokers; i++) {
//			var newJoker =(GameObject)Instantiate(jokerIconTemplate);
//			newJoker.transform.SetParent(jokersLeftHolder, false);
//			freeJokers.Add(newJoker.transform.GetComponent<UIJokerIcon>());
//			freeJokers[freeJokers.Count-1].Init(true);
//			yield return new WaitForSeconds(pause);
//		}
//	}

//	public void UpdateJokers() {
//		
//
//
//		if(LevelHandler.Instance.GetFreeJokersLeft() >= 0) {
//			for (int i = 0; i < freeJokers.Count; i++) {
//				if(i >= LevelHandler.Instance.GetFreeJokersLeft() && freeJokers[i] != null) {
//					freeJokers[i].Use(0.5f);
//				} 
//			}
//			return;
//		}
//
//
//		for (int i = 0; i < jokers.Count; i++) {
//			if(i >= LevelHandler.Instance.GetJokersLeft() && jokers[i] != null) {
//				jokers[i].Use(0.5f);
//			} 
//		}
//	}

	public override bool CanAdd(BlockObject blockObject) 
	{
		if(LevelHandler.Instance.GetJokersLeft() > 0)
			return true;

		return false;
	}

	public override void AddBlock (BlockObject blockObject)
	{
		if(isRunning) {
			blockObjects.Insert(0, blockObject);
			LevelHandler.Instance.jokersUsed++;
			//UpdateJokers();
			blockObject.dragable = false;
		}
		else {
			base.AddBlock (blockObject);
		}

	}

	public override void CreateStack (BlockObject[] blocks)
	{
		base.CreateStack (blocks);
		foreach (var item in blocks) {
			item.inStartStack = true;
			item.AddToStack(this);
			item.transform.position = GetNextStackPosition();
			item.SetSortingOrder(blockObjects.Count);
		}
	}

	public Vector3 GetBottomStackPosition() {
		var returnPos = transform.position;
		//returnPos += new Vector3(0,0,-(blockObjects.Count * 3));
		return returnPos;
	}

	public override Vector3 GetNextStackPosition() {
		var returnPos = transform.position;
		//returnPos += new Vector3(0,0,-(blockObjects.Count * 3));
		return returnPos;
	}

	public override void FadeOut ()
	{
//		StartCoroutine(DoFadeOut());
//		foreach (var item in jokers) {
//			item.FadeOut();
//		}
//		foreach (var item in freeJokers) {
//			item.FadeOut();
//		}
	}
//
//	IEnumerator DoFadeOut() {
//		float t = 0, animTime = 0.75f;
//		Color borderStartColor = borderImages[0].color;
//		while(t<animTime) {
//			foreach (var item in borderImages) {
//				item.color = Color.Lerp(borderStartColor, new Color(borderStartColor.r,borderStartColor.g,borderStartColor.b, 0), t/animTime);
//			}
//			yield return new WaitForFixedUpdate();
//			t+=Time.fixedDeltaTime;
//		}
//		foreach (var item in borderImages) {
//			item.color = Color.clear;
//		}
//	}
//
	public void Draw (Action callback = null)
	{
		if(ObjectSelector.Instance.selectedObject != null) return;
		if (blockObjects.Count <= 0)
			return;
		StartCoroutine (DrawSequence (callback));
	}

	public override void Reset ()
	{
		base.Reset ();
		isRunning = false;
	}

	IEnumerator DrawSequence (Action callback = null)
	{
		var normalStacks = LevelHandler.Instance.normalStacks;
		//int bottomCard = 0;
		for (int i = 0; i < normalStacks.Length; i++) {
			
			if(blockObjects.Count <= 0) break;
			DisplayAmountOfBlocks(blockObjects.Count);

			if (normalStacks [i].CanReceiveBlocks ()) {
				var thisBlock = blockObjects [blockObjects.Count-1];
				thisBlock.DrawSequence(normalStacks [i]);
				thisBlock.inStartStack = false;
				//thisBlock.dragable = true;
				thisBlock.childObject = null;
				this.RemoveBlock(thisBlock);
				yield return new WaitForSeconds (0.2f);
			} 
		}
		if(callback != null) callback();
	}

	void DisplayAmountOfBlocks(int currentAmount) {
		//if(currentAmount > 5) return;
//		for (int i = 1; i < borderImages.Length; i++) {
//			if(i >= currentAmount)
//				borderImages[i].gameObject.SetActive(false);
//			else
//				borderImages[i].gameObject.SetActive(true);
//		}

	}
}

