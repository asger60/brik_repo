﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

[System.Serializable]
public class WorldProgressBar {
	public bool isHidden;
	float curProgress;
	Image progressBg, progressFill;
	public AudioObject progressUpdate, progressComplete;
	Color bgColor, progressColor;
	public RectTransform progressRect;
	UIWorldPanelHandler worldHandler;

	public WorldProgressBar(Color bgColor, Color progressColor, Image progressBg, Image progressFill, float curProgress, UIWorldPanelHandler handler) {
		this.curProgress = 0;
		worldHandler = handler;
		this.bgColor = bgColor;
		this.progressColor = progressColor;
		this.progressBg = progressBg;
		this.progressFill = progressFill;
		progressBg.color = Color.clear;
		progressFill.color = Color.clear;
		isHidden = true;
		SetProgressBar(0);
	}

	public void Show() {
		Show(0, null);
	}

	public void Show(float delay, Action callback) {
		if(curProgress >= 1 || !isHidden) return; 
		isHidden = false;
		worldHandler.StartCoroutine(DoFadeInProgressBar(delay, callback));
	}

	public void Hide() {
		if(isHidden) return;
		worldHandler.StartCoroutine(FadeOutProgressBar());
	}

	public void SetProgressBar(float amount) {
		progressFill.fillAmount = amount;
	}

	public void UpdateProgress(float delay, float toAmount, Action callback) {
		if(isHidden) return;
		if(curProgress >= 1) {
			Hide();
			return;
		}
		worldHandler.StartCoroutine(HilightProgressBar(delay));
		worldHandler.StartCoroutine(DoProgressFill(delay, toAmount, callback));
	}

	IEnumerator DoProgressFill(float delay, float fillAmount, Action callback = null) {
		yield return new WaitForSeconds(delay);

		float initProgress = progressFill.fillAmount;
		curProgress = fillAmount;
		//progressUpdate.PostAudioEvent();
		float t = 0, animTime = 1.25f;
		while(t<animTime) {
			progressFill.fillAmount = (float)Easing.CubicEaseInOut(t, initProgress, fillAmount, animTime);
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		//progressFill.fillAmount = fillAmount;
		if(fillAmount >= 1) {
			//progressComplete.PostAudioEvent();
			worldHandler.StartCoroutine(FadeOutProgressBar());
		}
		if(callback != null)
			callback();
	}

	IEnumerator DoFadeInProgressBar(float delay, Action callback = null) {
		yield return new WaitForSeconds(delay);	
		float t=0, animTime = 1;
		Color colorTo = bgColor, colorFrom = new Color(colorTo.r, colorTo.r, colorTo.b, 0f);
		while(t < animTime) {
			yield return new WaitForFixedUpdate();
			t+=Time.deltaTime;
			progressBg.color = Color.Lerp(colorFrom, colorTo, t/animTime);
		}
		progressBg.color = bgColor;
		progressFill.color = progressColor;

		if(callback != null)
			callback();
	}

	IEnumerator FadeOutProgressBar() {
		isHidden = true;
		float t = 0, animTime = 1.25f;
		Color colorFrom1 = progressFill.color, colorTo1 = new Color(colorFrom1.r,colorFrom1.g,colorFrom1.b,0);
		Color colorFrom2 = progressBg.color, colorTo2 = new Color(colorFrom2.r,colorFrom2.g,colorFrom2.b,0);

		while(t<animTime) {
			progressFill.color = Color.Lerp(colorFrom1, colorTo1, (float)Easing.CubicEaseInOut(t, 0, 1, animTime));
			progressBg.color = Color.Lerp(colorFrom2, colorTo2, (float)Easing.CubicEaseInOut(t, 0, 1, animTime));

			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		progressFill.color = colorTo1;
		progressBg.color = colorTo2;
	}

	IEnumerator HilightProgressBar(float delay) {
		yield return new WaitForSeconds(delay);
		float timer = 0, animationTime = 2.25f;

		while(timer < animationTime) {
				progressFill.color = Color.Lerp(Color.white, progressColor, Easing.QuadEaseIn(timer, 0, 1, animationTime));
			timer += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		progressFill.color = progressColor;
	}

}

public class UIWorldPanelHandler : MonoBehaviour
{
	public Transform levelGridTransform;
	public GameObject levelButtonTemplate;
	public Image bgImage;
	public Text worldNameText;
	public RectTransform progressRect;
	public Image progressBg, progressFill, progressHiLight;
	public World world;
	public List<UIDealInfoPanel> dealInfoPanels = new List<UIDealInfoPanel> ();
	public Image worldLockedImage;
	bool _isLocked = true;
	bool progressBarHidden;
	public WorldProgressBar progressBar;
	//public bool unlockInSequence;
	public float currentProgress {
		get {
			return ((float)GetBeatenDealsCount() / (float)world.completedDealsToUnlockNext);
		}
	}
	public int currentDeal = 0;
	GameWorldSettings thisSettings;
	public bool tutorialView;

	public bool isLocked {
		get { return _isLocked; }
		set { 
			_isLocked  = value; 
		}
	}


	void Awake ()
	{
		UIDealInfoPanel[] dealButtons = levelGridTransform.transform.GetComponentsInChildren<UIDealInfoPanel> ();
		foreach (var item in dealButtons) {
			Destroy (item.gameObject);
		}
	}

	public void Init (World world)
	{
		this.world = world;

		thisSettings = GameHandler.Instance.GetGameWorldSettings (world);
		bgImage.color = thisSettings.backgroundColor;
		worldNameText.text = world.worldName;

		progressBar = new WorldProgressBar(thisSettings.levelSelectSettings.c3, thisSettings.blockSettings [1].c1, progressBg, progressFill, currentProgress, this);


		if(world.tutorialObject != null) {
			var stats = DealHandler.Instance.GetUserStats (world.tutorialObject);
			if(stats == null || stats.wins == 0) {
				var newGO = Instantiate (levelButtonTemplate);
				newGO.transform.SetParent (levelGridTransform, false);
				dealInfoPanels.Add (newGO.transform.GetComponent<UIDealInfoPanel> ());
				newGO.transform.GetComponent<UIDealInfoPanel> ().Init (world.tutorialObject, world, 0);
				tutorialView = true;
				StartCoroutine(DoHelperHiLight(4));
				return;	
			}
		}

		int index = 0;
		foreach (var item in world.dealList) {
			var newGO = Instantiate (levelButtonTemplate);
			newGO.transform.SetParent (levelGridTransform, false);
			dealInfoPanels.Add (newGO.transform.GetComponent<UIDealInfoPanel> ());
			newGO.transform.GetComponent<UIDealInfoPanel> ().Init (item, world, index);
			index++;
		}

		//var nextWorld = UIWorldSelector.Instance.GetNextWorldPanel (world);
		if(world.worldNum < UIWorldSelector.Instance.worldPanels.Count && currentProgress < 1) {
			progressBar.Show(2, ()=> {
				progressBar.UpdateProgress(0.2f, currentProgress, null);
			});	

		}
		
		tutorialView = false;
	}

	public void UpdateLockedState() {
		var nextWorld = UIWorldSelector.Instance.GetNextWorldPanel (world);
		if (nextWorld != null) {
			if(GetBeatenDealsCount() >= world.completedDealsToUnlockNext) {
				nextWorld.isLocked = false;
				nextWorld.worldLockedImage.transform.gameObject.SetActive(false);
			}
			else {
				nextWorld.isLocked = true; 
				nextWorld.worldLockedImage.transform.gameObject.SetActive(true);
			}
		} 
		if(world.worldNum == 0) {
			isLocked = false;
			worldLockedImage.transform.gameObject.SetActive(false);
		}
	}


	int GetBeatenDealsCount() {
		int beatenCount = 0;
		foreach (var item in dealInfoPanels) {
			if(item.thisStats.wins > 0)
				beatenCount++;
		}
		return beatenCount;
	}



	IEnumerator DoHelperHiLight(float delay) {
		yield return new WaitForSeconds(delay);
		dealInfoPanels[0].HighLightPanel();
		yield return new WaitForSeconds(1);
		dealInfoPanels[0].HighLightPanel();
		StartCoroutine(DoHelperHiLight(3));
	}


	public void RevealAllDeals() {
		dealInfoPanels[0].DestroyThis(()=>{
			dealInfoPanels.Clear();
			Init(world);
		});
	}


	public void UnlockWorld(Action callback) {
		StartCoroutine(DoFadeLockedImage(()=>{
			worldLockedImage.transform.gameObject.SetActive(false);
			isLocked = false;
			callback();
		}));
	}

	IEnumerator DoFadeLockedImage(Action callback) {
		Color colorFrom = worldLockedImage.color, colorTo = new Color(colorFrom.r,colorFrom.g,colorFrom.g,0);
		float t = 0, duration = .5f;
		while (t < duration) {
			worldLockedImage.color = Color.Lerp(colorFrom, colorTo, Easing.Linear(t, 0, 1, duration));
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		worldLockedImage.color = Color.clear;
		callback();
	}


	public void Hide() {
		foreach (var item in GetComponentsInChildren<UIWorldPanelHandler>()) {
			item.ResetDealButtons();
		}
		progressBar.SetProgressBar(0);
	}

	public void Show() {
		float delay = 0;
		foreach (var item in dealInfoPanels) {
			item.transform.localScale = Vector3.one;
			item.UpdateProgressPanel(delay);
			delay += 0.1f;
		}
		progressBar.UpdateProgress(0.5f, currentProgress, null);
	}

	public void UpdateWorldProgress (Action callback)
	{
		progressBar.UpdateProgress(0.5f, currentProgress, callback);
	}

	public AudioObject progressUpdate, progressComplete;



	public void ResetDealButtons() {
		foreach (var item in levelGridTransform.GetComponentsInChildren<UIDealInfoPanel>()) {
			item.ResetProgress ();
		}
	}


	public UIDealInfoPanel GetDealButton(BaseDealObject deal) {
		foreach (var item in dealInfoPanels) {
			if(item.thisDeal == deal) {
				return item;
			}
		}
		Debug.LogWarning("did not find a deal button");
		return null;
	}



	public void UpdateDealButton(DealObject deal) {
		foreach (var item in dealInfoPanels) {
			if(item.thisDeal == deal) {
				item.UpdateDevPanel ();
				item.UpdateProgressPanel(0);
				return;
			}
		}
		Debug.LogWarning("did not find a deal to update");
	}


}
