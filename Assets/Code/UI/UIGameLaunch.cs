﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIGameLaunch : UIScreenBase {

	public Transform zoomTransform;
	public Image fillImage;

	public override void OnEnable ()
	{
		base.OnEnable ();
		StartCoroutine(ZoomLoadImage());
	}


	IEnumerator ZoomLoadImage() {
		yield return new WaitForSeconds(1);
		float fillTime = 0.75f, zoomTime = 1.5f;
		float t =0;
		while (t<= fillTime) {
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate();
			fillImage.fillAmount = Mathf.Lerp(0.5f, 1f, Easing.CircEaseInOut(t, 0, 1, fillTime));
		}
		t = 0;
		while (t<= zoomTime) {
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate();
			zoomTransform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 7, Easing.CircEaseInOut(t, 0, 1, zoomTime));
		}



		GameHandler.Instance.ShowIntroScreen();
	}
}
