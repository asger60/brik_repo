﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIJokerIcon : MonoBehaviour
{
	public Image iconImage;


	public void Use() {
		Use(0);
	}

	public void Init(bool freeJoker) {
		Init(freeJoker, 0);
	}

	public void Init(bool freeJoker, float delay) {
		iconImage.transform.localScale = Vector3.zero;
		if(freeJoker) {
			iconImage.color = new Color(0,0,0,0.3f); 
		}
		else {
			iconImage.color = Color.white;
		}
		StartCoroutine(DoAppear(delay));
	}


	IEnumerator DoAppear(float delay) {
		yield return new WaitForSeconds(delay);
		Color initColor = iconImage.color;
		float t = 0, duration = 0.75f;
		while(t < duration) {
			iconImage.transform.localScale = Vector3.Lerp(Vector3.one * 0.2f, Vector3.one, (float)Easing.BackEaseOut(t, 0, 1, duration));
			t+=Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
	}


	bool isUsed;
	public void Use(float delay) {
		if(isUsed) return;
		isUsed = true;
		StartCoroutine(DoUse(delay));
	}

	IEnumerator DoUse(float delay) {
		if(delay > 0) yield return new WaitForSeconds(delay);
		float t = 0, duration = 0.75f;
		while(t < duration) {
			iconImage.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.one * 0.2f, (float)Easing.BackEaseIn(t, 0, 1, duration));
			t+=Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		//iconImage.color = Color.clear;

	}

	public void FadeOut() {
		StartCoroutine(DoFadeOut());
	}

	IEnumerator DoFadeOut() {
		float t = 0, duration = 0.75f;
		Color colorFrom = iconImage.color, colorTo = new Color(1,1,1, 0f);
		while(t < duration) {
			iconImage.color = Color.Lerp(colorFrom, colorTo, t/duration);
			t+=Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		iconImage.color = Color.clear;
	}

}

