﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UILevelIntro : UIScreenBase
{
	
	public Image backGroundImage;

	GameWorldSettings settings;

	public UIElementAnimator[] dealLogos;

	public override void OnEnable() {
		base.OnEnable();
		foreach (var item in dealLogos) {
			item.gameObject.SetActive(false);
		}
		settings = GameHandler.Instance.GetGameWorldSettings();

		backgroundColor = GameHandler.Instance.GetGameWorldSettings().blockSettings[1].c1;
		backGroundImage.color = backgroundColor;

	}

	public override void OnTransitionDone() {
		
		base.OnTransitionDone();
		GetLevelLogo().Init(settings);

	}

	UIElementAnimator GetLevelLogo() {
		foreach (var item in dealLogos) {
			if(item.colorPaletteId == settings.paletteId)
				return item;
		}
		return null;
	}




	public override void NextScreen() {
		if(transitionSettings.progressing) return;
		base.NextScreen();
		GameHandler.Instance.StartLevel();
	}

}

