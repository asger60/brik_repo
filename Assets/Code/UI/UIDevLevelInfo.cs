﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class UIDevLevelInfo : MonoBehaviour {
	public Text titleText;
//	public Image likeImageBG;
	public Slider difficultySlider, entertaintmentSlider;
//	bool like;
	DealObject thisDeal;
	Action progressAction;

//	public void Start()
//	{
//		difficultySlider.onValueChanged.AddListener (delegate {DifficultySlider ();});
//	}

	public void Init(DealObject deal, Action callback) {
		thisDeal = deal;
		var dealSettings = DealHandler.Instance.GetUserStats(thisDeal);
		titleText.text = thisDeal.dealName;
		difficultySlider.value = dealSettings.difficulty;
		entertaintmentSlider.value = dealSettings.entertaintment;
		gameObject.SetActive(true);
		transform.GetComponent<Canvas>().overrideSorting = true;
		progressAction = callback;
	} 
		

	public void Progress() {
		gameObject.SetActive(false);
		progressAction();
	}

	public void DifficultySlider()
	{
		var dealSettings = DealHandler.Instance.GetUserStats(thisDeal);
		dealSettings.difficulty = (int)difficultySlider.value;
		dealSettings.Save();
	}

	public void EntertaintmentSlider() {
		var dealSettings = DealHandler.Instance.GetUserStats(thisDeal);
		dealSettings.entertaintment = (int)entertaintmentSlider.value;
		dealSettings.Save();
	}
}
