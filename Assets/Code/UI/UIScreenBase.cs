﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TransitionSettings {
	public enum TransitionType {
		none,
		bullet,
		dissolve,
		dissolveAditive,
		bulletMask,
	}
	public TransitionType transitionType = TransitionType.dissolve;
	[HideInInspector]
	public float timer = 0;
	[HideInInspector]
	public bool progressing;
	public float autoTransitionTime = 2;
	public AudioObject screenEnterAudioObject, screenExitAudioObject;


	public void OnScreenEnter() {
		if(screenEnterAudioObject != null) 
			screenEnterAudioObject.PostAudioEvent();
	}

	public void OnScreenExit() {
		if(screenExitAudioObject != null) 
			screenExitAudioObject.PostAudioEvent();
	}
}

public class UIScreenBase : MonoBehaviour
{
	public TransitionSettings transitionSettings;

	Color _backgroundColor;
	public Color backgroundColor {
		get {
			if(_backgroundColor == Color.clear) 
				return GameHandler.Instance.GetGameWorldSettings().backgroundColor;
			return _backgroundColor;
		}
		set { _backgroundColor = value; }
	}
	public bool inputEnabled = true;



	public virtual void OnTransitionDone() {
		transitionSettings.timer = 0;
		inputEnabled = true;
		if(transitionSettings.progressing) return;
	}

	public virtual void OnEnable() {
		transitionSettings.timer = 0;
		transitionSettings.progressing = false;
		transitionSettings.OnScreenEnter();
	}

	public virtual void OnDisable() {
		
	}

	public virtual void Update() {
		if(transitionSettings.autoTransitionTime == -1) return;
		transitionSettings.timer += Time.deltaTime;
		if(transitionSettings.timer > transitionSettings.autoTransitionTime && !transitionSettings.progressing) {
			NextScreen();
		}
	}

	public virtual void NextScreen() {
		transitionSettings.OnScreenExit();
		inputEnabled = false;
		transitionSettings.progressing = true;
	}
		

	public virtual void Show() {
		inputEnabled = false;
		gameObject.SetActive(true);
	}

	public virtual void Hide() {
		gameObject.SetActive(false);
	}
}

