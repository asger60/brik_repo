﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;


public class Navigator {
	List<Image> navigatorImages = new List<Image>();
	bool enabled, visible;
	int currentIndex;
	Color activeColor = Color.white, inActiveColor = new Color(0,0,0,0.2f), clearColor = new Color(1,1,1,0);
	UIWorldSelector parentHandler;
	Sprite sprite;
	Transform parent;
	public Navigator(Transform parent, Sprite sprite, int pages, int currentPage, UIWorldSelector parentHandler) {
		this.sprite = sprite;
		this.parent = parent;
		for (int i = 0; i < pages; i++) {
			AddPageIcon();
		}
		this.currentIndex = currentPage;

		//UpdatePage(currentIndex);
		enabled = false;
		visible = false;
		this.parentHandler = parentHandler;
	}

	void AddPageIcon() {
		var navGo = new GameObject("NavIcon");
		navGo.transform.SetParent(parent, false);
		navigatorImages.Add(navGo.AddComponent<Image>());
		navigatorImages[navigatorImages.Count-1].color = clearColor;
		navigatorImages[navigatorImages.Count-1].sprite = sprite;
	}

	public void Show(float delay) {
		if(visible) return;
		parentHandler.StartCoroutine(DoShow(delay));
	}

	IEnumerator DoShow(float delay) {
		yield return new WaitForSeconds(delay);
		for (int i = 0; i < navigatorImages.Count; i++) {
			SetNavImageState(navigatorImages[i], i == currentIndex, 1f);
		}
		enabled = true;
		visible = true;
		//UpdatePage(currentIndex);
	}

	public void Hide(float delay) {
		
	}

	public void UpdatePage(int index) {
		if(!enabled) return;
		if(!visible) return;
		currentIndex = index;
		for (int i = 0; i < navigatorImages.Count; i++) {
			SetNavImageState(navigatorImages[i], i == currentIndex, 0.25f);
		}
	}

	void SetNavImageState(Image image, bool state, float animTime) {
		parentHandler.StartCoroutine(DoSetImageState(image, state, animTime));
	}

	IEnumerator DoSetImageState(Image image, bool state, float animTime) {
		Debug.Log("DoSetImageState");
		float t = 0;
		Color startColor = image.color;
		Color endColor;
		if(state) endColor = activeColor; else endColor = inActiveColor;
		while(t <= animTime) {
			image.color = Color.Lerp(startColor, endColor, t/animTime);
			t+=Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		image.color = endColor;
	}

	public void AddPages(int amount) {
		for (int i = 0; i < amount; i++) {
			AddPageIcon();
		}
	}

}

public class SwipeController {
	public GameObject worldPanelTemplate;
	public RectTransform worldPanelTransform;
	float _dragDelta, dragDeltaOldPos, dragScale = 1f;
	float dragDelta;

	public float GetDragDelta() {
		_dragDelta = ObjectSelector.Instance.inputPosition.x - dragDeltaOldPos;
		dragDeltaOldPos = ObjectSelector.Instance.inputPosition.x;
		return _dragDelta * dragScale; 
	}
}

public class UIWorldSelector : UIScreenBase {


	public GameObject groundPlane;

	public GameObject worldPanelTemplate;
	public RectTransform worldPanelTransform;
	public Image bgImage;
	[NonSerialized]
	public List<UIWorldPanelHandler> worldPanels = new List<UIWorldPanelHandler>();
	public RectTransform pageNavTransform;

	public AudioObject swipeStartAudioObject, swipeDoneAudioObject, worldUnlockAudioObject;
	public Transform devTestUserPanel;

	float _dragDelta, dragDeltaOldPos, dragScale = 1f;
	float dragDelta;
	bool navigatorEnabled, navigatorVisible;
	public Sprite navigatorSprite;

	Navigator navigator;

	float GetDragDelta() {
		_dragDelta = ObjectSelector.Instance.inputPosition.x - dragDeltaOldPos;
		dragDeltaOldPos = ObjectSelector.Instance.inputPosition.x;
		return _dragDelta * dragScale; 
	}
	public World currentWorldObject;
	int _curWorldIndex = 0, _oldWorldIndex;
	public int currentWorldpanelIndex {
		get { return _curWorldIndex; }
		set { 
			value = Mathf.Clamp(value, 0, GetNumWorldUnlocked()-1);
			if(value == _curWorldIndex) {
				return;
			}


			_curWorldIndex = value; 
			navigator.UpdatePage(_curWorldIndex);

			worldPanels[_curWorldIndex].Hide();

			UpdateBackgroundColor();
				
			currentWorldObject = worldPanels[_curWorldIndex].world;
			GameHandler.Instance.SetGameWorld(currentWorldObject);
		}
	}
	bool _isSwiping;
	public bool isSwiping {
		get { return _isSwiping; }
		set { 
			if(!value && _isSwiping) {
				swipeDoneAudioObject.PostAudioEvent();
			}
			if(value && !_isSwiping) {
				swipeStartAudioObject.PostAudioEvent();
			}

			_isSwiping = value;
		}
	}

	static UIWorldSelector _instance;
	public static UIWorldSelector Instance {
		get {
			return _instance;
		}
	}

	void Awake() {
		_instance = this;
		foreach (var item in worldPanelTransform.GetComponentsInChildren<UIWorldPanelHandler>()) {
			Destroy(item.gameObject);
		}
		Start();
	}

	void Start() {
		//GameHandler.Instance.updateCurrentWorld.AddListener(UpdateBackgroundColor);
		foreach (var item in pageNavTransform.GetComponentsInChildren<Image>()) {
			if(item.transform == pageNavTransform) continue;
			Destroy(item.gameObject);
		}
		CreateWorldPanels();
		//CreateNavigator();
		navigator = new Navigator(pageNavTransform, navigatorSprite, GetNumWorldUnlocked(), 0, this);

		currentWorldpanelIndex = 0;
		if(!worldPanels[currentWorldpanelIndex].tutorialView) {
			navigator.Show(2);
		}
		inputEnabled = true;
		backgroundColor = worldPanels[_curWorldIndex].bgImage.color;

		UIWorldSelector.Instance.UpdateBackgroundColor();
	}

	public void ReInit() {
		worldPanels.Clear();
		//navigatorImages.Clear();
		foreach (var item in worldPanelTransform.GetComponentsInChildren<UIWorldPanelHandler>()) {
			DestroyImmediate(item.gameObject);
		}
		foreach (var item in pageNavTransform.GetComponentsInChildren<Image>()) {
			if(item.transform == pageNavTransform) continue;
			DestroyImmediate(item.gameObject);
		}
		CreateWorldPanels();
		navigator = new Navigator(pageNavTransform, navigatorSprite, GetNumWorldUnlocked(), 0, this);
		//CreateNavigator();
		currentWorldpanelIndex = 0;
	}


	bool _doDrag;
	bool DoDrag {
		get { return _doDrag; }
		set { 
			if(value && !_doDrag) DragStart();
			if(!value && _doDrag) DragEnd();
			_doDrag = value; 
		}
	}

	float startPosition;
	void DragStart() {
		dragDeltaOldPos = ObjectSelector.Instance.inputPosition.x;
		startPosition = ObjectSelector.Instance.inputPosition.x;
		StopAllCoroutines();
	}

	bool currentWorldUpdated;
	void DragEnd() {
		int thisIndex = currentWorldpanelIndex;



		if(Mathf.Abs(dragDelta) > 20) {
			if(dragDelta > 0) 
				currentWorldpanelIndex--;
			else
				currentWorldpanelIndex++;
		} else {
			if(ObjectSelector.Instance.inputPosition.x > startPosition + Screen.width * 0.25f) {
				currentWorldpanelIndex--;
			}

			if(ObjectSelector.Instance.inputPosition.x < startPosition - Screen.width * 0.25f) {
				currentWorldpanelIndex++;
			}
		}


		currentWorldUpdated = (thisIndex != currentWorldpanelIndex);
		SwipeWorldPanels(0.5f, ()=> {
			if(currentWorldUpdated)
				worldPanels[currentWorldpanelIndex].Show();
		});
	}

	void SwipeWorldPanels(float duration, Action callback = null) {
		StartCoroutine(DoMoveToPosition(-(worldPanels[currentWorldpanelIndex].GetComponent<RectTransform>().anchoredPosition.x), duration, callback));
	}


	IEnumerator DoMoveToPosition(float xPos, float animTime, Action callback = null) 
	{
		float t = 0, initPos = worldPanelTransform.anchoredPosition.x;
		while(t<animTime) {
			float thisPos = Mathf.Lerp(initPos, xPos, (float)Easing.ExpoEaseOut(t, 0,1, animTime));
			worldPanelTransform.anchoredPosition = new Vector2(thisPos, worldPanelTransform.anchoredPosition.y);
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		worldPanelTransform.anchoredPosition = new Vector2(xPos, worldPanelTransform.anchoredPosition.y);
		if(callback != null) callback();
	}



	public override void Update() {
		if(!inputEnabled) return;

		if(Input.GetKeyDown(KeyCode.LeftArrow))	{
			currentWorldpanelIndex--;
			SwipeWorldPanels(0.5f, ()=>{
				if(currentWorldUpdated)
					worldPanels[currentWorldpanelIndex].Show();
			});
		}

		if(Input.GetKeyDown(KeyCode.RightArrow))	{
			currentWorldpanelIndex++;

			SwipeWorldPanels(0.5f, ()=>{
				if(currentWorldUpdated)
					worldPanels[currentWorldpanelIndex].Show();
			});
		}
		if(Input.GetKeyDown(KeyCode.U))	{
			UnLockNextWorld(null);
		}


		DoDrag = ObjectSelector.Instance.inputSelect;
		dragDelta = GetDragDelta();

		if(DoDrag) {
			worldPanelTransform.anchoredPosition += new Vector2(dragDelta, 0);
		} 
		Vector2 targetPosition = new Vector2(-(worldPanels[currentWorldpanelIndex].GetComponent<RectTransform>().anchoredPosition.x), worldPanelTransform.anchoredPosition.y);
		if(Vector2.Distance(worldPanelTransform.anchoredPosition, targetPosition) > 1) {
			isSwiping = true;
		} else 
			isSwiping = false;
	}

	public void UpdateBackgroundColor() {
		backgroundColor = worldPanels[_curWorldIndex].bgImage.color;
		bgImage.color = backgroundColor;
		groundPlane.transform.GetComponent<MeshRenderer>().material.color = backgroundColor;
	}


	public int GetNumWorldUnlocked() {
		int i = 0;
		foreach (var item in worldPanels) {
			if(!item.isLocked) i++;
		}
		return i;
	}



	void CreateWorldPanels() {
		for (int i = 0; i < DealHandler.Instance.worldList.Count; i++) {
			var item = DealHandler.Instance.worldList[i];
			item.worldNum = i;
			var go = Instantiate(worldPanelTemplate);
			go.transform.SetParent(worldPanelTransform, false);
			var rect = go.transform.GetComponent<RectTransform>();
			rect.anchoredPosition = new Vector2(worldPanels.Count*rect.rect.width,0);
			worldPanels.Add(go.transform.GetComponent<UIWorldPanelHandler>());
			worldPanels[worldPanels.Count-1].Init(item);
		}
		foreach (var item in worldPanels) {
			item.UpdateLockedState();
		}
	}




	public void UnLockNextWorld(Action callback) {
		
		GetNextWorldPanel(currentWorldObject).isLocked = true;
		print("UnLockNextWorld");
		worldPanels[currentWorldpanelIndex+1].isLocked = false;
		currentWorldpanelIndex++;
		SwipeWorldPanels(0.5f, ()=> {
			worldUnlockAudioObject.PostAudioEvent();
			worldPanels[currentWorldpanelIndex].UnlockWorld(()=>{
				navigator.AddPages(1);
				navigator.UpdatePage(currentWorldpanelIndex);

				if(callback != null) callback();
			});
		});
		GameHandler.Instance.UnlockWorld(currentWorldpanelIndex);
	}
		
	public UIWorldPanelHandler GetPreviousWorldPanel(World thisWorld) {
		if(thisWorld.worldNum == 0) return null;
		return worldPanels[thisWorld.worldNum-1];
	}

	public UIWorldPanelHandler GetNextWorldPanel(World thisWorld) {
		if(thisWorld.worldNum == worldPanels.Count-1) return null;
		return worldPanels[thisWorld.worldNum+1];
	}


	public float GetWorldProgress(World world) {
		float progress = 0f;
		int numberOfDeals = world.dealList.Count;
		int winCount = 0; 
		foreach (var item in world.dealList) {
			var stats = DealHandler.Instance.GetUserStats (item);
			if(stats == null) continue;
			if (stats.wins > 0)
				winCount++;
		}

		progress = ((float)winCount / (float)numberOfDeals)/world.completedDealsToUnlockNext;
		return progress;
	}

	bool showDevInfo;
	public void ToggleDevInfo() {
		showDevInfo = !showDevInfo;
		devTestUserPanel.gameObject.SetActive(showDevInfo);
		foreach (var item in worldPanels[currentWorldpanelIndex].dealInfoPanels) {
			item.ToggleDevInfo(showDevInfo);
		}
	}

	public override void OnTransitionDone ()
	{
		base.OnTransitionDone ();
		inputEnabled = false;

		var curButton = worldPanels[currentWorldpanelIndex].GetDealButton(GameHandler.Instance.dealObject);
		if(curButton == null) {
			inputEnabled = true;
			return;
		}

		//returning from tutorial
		if(GameHandler.Instance.dealObject is DealTutorialObject) {
			var stats = DealHandler.Instance.GetUserStats (GameHandler.Instance.dealObject);
			if(stats.wins >= 1) { 
				worldPanels[currentWorldpanelIndex].RevealAllDeals();
				navigator.Show(3);
			}
			inputEnabled = true;
			return;
		}

		//didnt complete the deal
		if(!GameHandler.Instance.levelCompleted) {
			inputEnabled = true;
			curButton.HighLightPanel();
			curButton.FlashText();
			curButton.SetProgressPanel();
			return;
		}


		curButton.HighLightPanel();
		curButton.FlashText();
		curButton.UpdateProgressPanel(0, ()=> {
			
			if(worldPanels[currentWorldpanelIndex].currentProgress > 1) {
				inputEnabled = true;
				return;
			}

			worldPanels[currentWorldpanelIndex].UpdateWorldProgress(()=>{
				if(currentWorldpanelIndex+1 == worldPanels.Count) {
					inputEnabled = true;
					return;
				}
				if(worldPanels[currentWorldpanelIndex].currentProgress >= 1f && worldPanels[currentWorldpanelIndex+1].isLocked) {
					UnLockNextWorld(()=> {
						inputEnabled = true;
					});
				} else 
					inputEnabled = true;
			});
		});
	}


	public override void Show() {
		gameObject.SetActive(true);
		ResetAll();
	}

	void ResetAll() {
		foreach (var item in worldPanels) {
			foreach (var subItem in item.dealInfoPanels) {
				subItem.ResetEffects();
			}
		}
	}

	public override void Hide() {
		gameObject.SetActive(false);
	}




}


