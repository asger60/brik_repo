﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIInGameMenu : UIScreenBase {

	public Transform inGameMenuTransform;
	public Transform inGameMenuButtonTransform;
	public GameObject devDealInfoGameObjectTemplate;
	UIDevLevelInfo uiDevLevelInfo;
	public Image bgImage;
	public Image menuButtonImage, restartButtonImage, restarButtonIcon;
	public Image[] menuButtonIcons;
	public UIElementAnimator[] worldIcons;

	bool _menuActive;
	bool MenuActive {
		get {
			return _menuActive;
		}
		set {
			_menuActive = value;
			inGameMenuTransform.gameObject.SetActive(_menuActive);
		}
	}

	public override void OnEnable ()
	{
		base.OnEnable ();
		var thisSettings = GameHandler.Instance.GetGameWorldSettings();

		foreach (var item in worldIcons) {
			if(item.colorPaletteId == GameHandler.Instance.GetGameWorldSettings().paletteId) 
			{
				item.gameObject.SetActive(false);
				item.Init(GameHandler.Instance.GetGameWorldSettings());
			}
			else
				item.gameObject.SetActive(false);
		}


		if(uiDevLevelInfo == null) {
			var devDealInfoGameObject = (GameObject)Instantiate(devDealInfoGameObjectTemplate);
			devDealInfoGameObject.transform.SetParent(inGameMenuTransform, false);
			uiDevLevelInfo = devDealInfoGameObject.transform.GetComponent<UIDevLevelInfo>();
			var thisRect = uiDevLevelInfo.transform.GetComponent<RectTransform>();
			thisRect.sizeDelta = new Vector2(Screen.width, Screen.height)/ScreenManager.Instance.scale;
			thisRect.anchoredPosition = thisRect.sizeDelta/2;
			uiDevLevelInfo.gameObject.SetActive(false);
		}
		var bgColor = GameHandler.Instance.GetGameWorldSettings().backgroundColor;
		var darkColor = new Color(bgColor.r*0.75f,bgColor.g*0.75f,bgColor.b*0.75f,1);
		bgImage.color = darkColor;


		menuButtonImage.color = thisSettings.greyBlock.c3;
		restartButtonImage.color = thisSettings.greyBlock.c3;



		foreach (var item in menuButtonIcons) {
			item.color = bgColor;
		}
		//menuButtonIcon.color = darkColor;
		restarButtonIcon.color = bgColor;
	}


	public void RestartDeal() {
		if(!GameHandler.Instance.testBuild) {
			GameHandler.Instance.RestartLevel();
			return;
		}
			
		uiDevLevelInfo.Init(GameHandler.Instance.activeDeal as DealObject, ()=>{
			GameHandler.Instance.RestartLevel();
		});

	}

	public void GotoMenu() {
		if(!GameHandler.Instance.testBuild) {
			GameHandler.Instance.ShowLevelSelect();
			return;
		}

		uiDevLevelInfo.Init(GameHandler.Instance.activeDeal as DealObject, ()=>{
			GameHandler.Instance.ShowLevelSelect();
		});

	}
}
