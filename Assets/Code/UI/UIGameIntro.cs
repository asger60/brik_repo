﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIGameIntro : UIScreenBase
{
	public Image hilightImage;

	public Image[] fillLetters;
	public AudioObject letterFill, letterBlink;

	public override void OnTransitionDone ()
	{
		base.OnTransitionDone ();
		StopAllCoroutines();
		StartCoroutine(DoTapHilight(5));
		for (int i = 0; i < fillLetters.Length; i++) {
			fillLetters[i].fillAmount = 0;
			float fillAmount = 1;
			if(i == 1) fillAmount = .5f;
			StartCoroutine(DoFillLetter(fillLetters[i], fillAmount, i*0.5f));
		}
	}



	IEnumerator DoFillLetter(Image targetImage, float fillAmount, float delay) {
		targetImage.fillAmount = 0;
		yield return new WaitForSeconds(delay);
		letterFill.PostAudioEvent();
		float t = 0, animTime = 2.5f;
		while (t < animTime) {
			targetImage.fillAmount = Easing.ExpoEaseOut(t, 0, fillAmount, animTime);
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}
		targetImage.fillAmount = fillAmount;
	}




	IEnumerator DoTapHilight(float delay) {
		yield return new WaitForSeconds(delay);
		StartCoroutine(DoHilight());
		yield return new WaitForSeconds(1);
		StartCoroutine(DoHilight());
		StartCoroutine(DoTapHilight(delay));
	}

	IEnumerator DoHilight ()
	{
		letterBlink.PostAudioEvent();
		float t = 0, animTime = .5f;
		hilightImage.rectTransform.localScale = Vector2.one;
		hilightImage.color = new Color (1, 1, 1, 1);
		while (t < animTime) {
			hilightImage.rectTransform.localScale = Vector2.one * (float)Easing.SineEaseOut (t, 1, 1.25f, animTime);
			hilightImage.color = Color.Lerp (new Color (1, 1, 1, 1), new Color (1, 1, 1, 0), (float)Easing.SineEaseOut (t, 0, 1, animTime));
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}
		hilightImage.color = new Color (1, 1, 1, 0);
	}

	public void GoToLevelSelect() {
		GameHandler.Instance.ShowLevelSelect();
	}
}

