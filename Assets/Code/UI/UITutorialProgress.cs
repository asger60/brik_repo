﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;


public class UITutorialProgress : UIScreenBase
{

	public Image backGroundImage, progressFillImage, progressBgImage;
	float curProgress;
	GameWorldSettings settings;
	public ParticleController confetti;
	public AudioObject fillObject, confettiObject;


	public override void OnEnable() {
		base.OnEnable();
		curProgress =(float)((float)GameHandler.Instance.currentTutorialDeal / (float)GameHandler.Instance.tutorialObject.tutorialDeals.Count);
		settings = GameHandler.Instance.GetGameWorldSettings();

		progressFillImage.fillAmount = curProgress;
		progressBgImage.sprite = settings.worldSprite;
		progressFillImage.sprite = settings.worldSprite;


		backGroundImage.color = settings.backgroundColor;
		progressBgImage.color = settings.levelSelectSettings.c2;
		progressFillImage.color = settings.levelSelectSettings.c1;
		float progress = (float)((float)(GameHandler.Instance.currentTutorialDeal +1) / (float)GameHandler.Instance.tutorialObject.tutorialDeals.Count);
		if(progress >= 1) transitionSettings.autoTransitionTime = 4;
	}

	public override void OnTransitionDone() {
		base.OnTransitionDone();
		float progress = (float)((float)(GameHandler.Instance.currentTutorialDeal +1) / (float)GameHandler.Instance.tutorialObject.tutorialDeals.Count);
		StartCoroutine(DoFill(curProgress, progress, 0f, 0.75f, ()=>{
			if(progress >= 1) {
				confetti.Emit(200);
				if(confettiObject != null) confettiObject.PostAudioEvent();
			}
		}));
	}




	IEnumerator DoFill(float from, float to, float delay, float animDuration, Action callBack) {
		if(delay > 0)
			yield return new WaitForSeconds(delay);

		if(fillObject != null) fillObject.PostAudioEvent();

		float t = 0;
		while(t < animDuration) {
			progressFillImage.fillAmount = (float)Easing.CubicEaseInOut(t, from, to, animDuration);
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		progressFillImage.fillAmount = to;
		if(callBack != null) callBack();
	}



	public void GotoNextScreen() {
		if(!inputEnabled) return;
		NextScreen();
	}

	public override void NextScreen() {
		base.NextScreen();
		GameHandler.Instance.NextTutorialDeal();
	}

}

