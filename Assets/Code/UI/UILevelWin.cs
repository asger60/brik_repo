﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;


public class UILevelWin : UIScreenBase
{

	public Image backGroundImage, progressFillImage, progressBgImage;
	public Transform jokerHolderTransform;
	public GameObject jokerIconTemplate;
	List<UIJokerIcon> jokerIcons = new List<UIJokerIcon>(), freeJokerIcons = new List<UIJokerIcon>();

	public Text nameText;
	GameWorldSettings settings;
	DealObject thisDeal;
	DealUserStats stats;
	public ParticleController confetti;
	public AudioObject confettiAudioObject, countUpAudioObject, subtractAudioObject;

	public GameObject devDealInfoGameObjectTemplate;
	UIDevLevelInfo uiDevDelInfo;

	public override void OnEnable() {
		base.OnEnable();
		settings = GameHandler.Instance.GetGameWorldSettings();
		thisDeal = GameHandler.Instance.activeDeal as DealObject;
		progressBgImage.sprite = settings.worldSprite;
		progressFillImage.sprite = settings.worldSprite;
		progressFillImage.fillAmount = 0f;

		nameText.rectTransform.anchoredPosition = Vector2.zero;
		if(progressBgImage.sprite.name == "Triangle512") {
			nameText.rectTransform.anchoredPosition -= new Vector2(0,40);
		}

		backgroundColor = GameHandler.Instance.GetGameWorldSettings().blockSettings[1].c1;
		backGroundImage.color = backgroundColor;

		nameText.text = (DealHandler.Instance.GetDealNumInWorld(thisDeal)+1).ToString();
		stats = DealHandler.Instance.GetUserStats(thisDeal);

		foreach (var item in jokerHolderTransform.GetComponentsInChildren<UIJokerIcon>()) {
			Destroy(item.gameObject);
		}

		freeJokerIcons.Clear();
		jokerIcons.Clear();
		float delay = 0.75f;
		for (int i = 0; i < thisDeal.freeJokers; i++) {
			var newJoker = (GameObject)Instantiate(jokerIconTemplate);
			newJoker.transform.SetParent(jokerHolderTransform);
			var thisJoker = newJoker.transform.GetComponent<UIJokerIcon>();
			thisJoker.transform.GetComponentInChildren<Image>().material = null;
			freeJokerIcons.Add(thisJoker);
			thisJoker.Init(true, delay);
			delay +=0.2f;
		}

		for (int i = 0; i < thisDeal.maxJokers; i++) {
			var newJoker = (GameObject)Instantiate(jokerIconTemplate);
			newJoker.transform.SetParent(jokerHolderTransform);
			var thisJoker = newJoker.transform.GetComponent<UIJokerIcon>(); 
			thisJoker.transform.GetComponentInChildren<Image>().material = null;
			jokerIcons.Add(thisJoker);
			thisJoker.Init(false, delay);
			delay +=0.2f;
		}


		progressBgImage.color = (settings.levelSelectSettings.c1 + new Color(0.1f,0.1f,0.1f,0f)) - new Color(0,0,0,0.5f);
		progressFillImage.color = settings.levelSelectSettings.c1;

		if(uiDevDelInfo == null) {
			var devDealInfoGameObject = (GameObject)Instantiate(devDealInfoGameObjectTemplate);
			devDealInfoGameObject.transform.SetParent(transform, false);
			uiDevDelInfo = devDealInfoGameObject.transform.GetComponent<UIDevLevelInfo>();

			var thisRect = uiDevDelInfo.transform.GetComponent<RectTransform>();
			thisRect.sizeDelta = new Vector2(Screen.width, Screen.height)/ScreenManager.Instance.scale;
			thisRect.anchoredPosition = thisRect.sizeDelta/2;

			uiDevDelInfo.gameObject.SetActive(false);
		}
	}

	public override void OnTransitionDone() {
		base.OnTransitionDone();
		StartCoroutine(DoFill(0f, 1f, 0.5f, 1f, ()=>{
			StartCoroutine(DoFreeJokerSubtract(()=> {
				if(stats.jokersUsed <= thisDeal.freeJokers) {
					confettiAudioObject.PostAudioEvent();
					confetti.Emit(200);
				}
				else 
				{
					StartCoroutine(DoJokerSubtract());
				}
			}));
		}));
	}

	IEnumerator DoFreeJokerSubtract(Action callback) {
		yield return new WaitForSeconds(0.5f);
		for (int i = 0; i < (int)Mathf.Min(freeJokerIcons.Count, stats.jokersUsed); i++) {
			subtractAudioObject.PostAudioEvent();
			freeJokerIcons[i].Use();
			yield return new WaitForSeconds(0.25f);
		}
		callback();
	}

	IEnumerator DoJokerSubtract() {
		yield return new WaitForSeconds(0.5f);
		for (int i = 0; i < stats.jokersUsed - thisDeal.freeJokers; i++) {
			subtractAudioObject.PostAudioEvent();
			jokerIcons[i].Use(0);
			StartCoroutine(DoFill(thisDeal.GetProgress(i+thisDeal.freeJokers), thisDeal.GetProgress(i+1+thisDeal.freeJokers), 0.7f, 0.5f, null)); 
			yield return new WaitForSeconds(0.75f);
		}
	}

	IEnumerator DoFill(float from, float to, float delay, float animDuration, Action callBack) {
		if(delay > 0)
			yield return new WaitForSeconds(delay);

		countUpAudioObject.PostAudioEvent();
		print(from);
		float t = 0;
		while(t < animDuration) {
			progressFillImage.fillAmount = Easing.QuadEaseInOut(t, from, to, animDuration);
			t+=Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		progressFillImage.fillAmount = to;
		if(callBack != null) callBack();
	}


	public void GotoNextScreen() {
		if(!inputEnabled) return;
		NextScreen();
	}

	public override void NextScreen() {
		base.NextScreen();
		if(!GameHandler.Instance.testBuild) {
			GameHandler.Instance.ShowLevelSelect();
			return;
		}

		uiDevDelInfo.Init(thisDeal, ()=>{
			base.NextScreen();
			GameHandler.Instance.ShowLevelSelect();
		});

	}


}

