﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

[System.Serializable]
public class DevDealPanel
{
	public Text dealNumberText;
	public Text triesText, completedText, difficultyText, movesText, jokersText, funfactorText;
	public Image likeImage, bg;
	public Transform devDealTransform;
}

public class UIDealInfoPanel : MonoBehaviour
{

	public DevDealPanel devDealPanel;

	public AudioObject click, fill, appear, quickFill;
	public Image fillImage, bgImage, highLighter;
	public Text numberText;
	GameWorldSettings thisSettings;

	//	bool _isLocked;
	//	public bool isLocked {
	//		get {
	//			return _isLocked;
	//		}
	//		set {
	//			_isLocked = value;
	//			lockImage.gameObject.SetActive(_isLocked);
	//		}
	//	}
	//public Image lockImage;

	public DealUserStats thisStats;


	public BaseDealObject thisDeal;
	//public DealTutorialObject thisTutorial;

	//World thisWorld;
	public void Init (BaseDealObject deal, World world, int index)
	{
		thisStats = DealHandler.Instance.GetUserStats (deal);
		thisSettings = GameHandler.Instance.GetGameWorldSettings (world);
		thisDeal = deal;
		if (deal is DealTutorialObject) {
			
		} else {
			UpdateDevPanel ();
		}
			
		fillImage.sprite = thisSettings.worldSprite;
		numberText.rectTransform.anchoredPosition = Vector2.zero;
		if (fillImage.sprite.name == "Triangle512") {
			numberText.rectTransform.anchoredPosition -= new Vector2 (0, 15);
		}

		bgImage.sprite = thisSettings.worldSprite;
		highLighter.sprite = thisSettings.worldSprite;


		fillImage.color = thisSettings.levelSelectSettings.c1;

		int dealNumber = (DealHandler.Instance.GetDealNumInWorld (deal) + 1);
		if(dealNumber != 0)
			numberText.text = dealNumber.ToString ();
		else
			numberText.text = "";


		numberText.color = Color.clear;


		SetProgress();
//		UpdateProgressPanel (0);

		transform.localScale = Vector3.zero;
		StartCoroutine (DoScale (0, 1, index * 0.125f, () => {
			
		}));
	}


	IEnumerator DoScale (float from, float to, float delay, Action callback)
	{
		
		float t = 0, animTime = .85f;
		yield return new WaitForSeconds (delay);
		appear.PostAudioEvent();
		while (t <= animTime) {
			transform.localScale = Vector3.one * Easing.BackEaseOut (t, from, to, animTime);
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}
		transform.localScale = Vector3.one * to;
		callback ();
	}

	public void DestroyThis (Action callback)
	{
		StartCoroutine (DoScale (1f, 0f, 0f, () => {
			Destroy (gameObject);
			callback ();
		}));
	}

	public void ResetProgress ()
	{
		StopAllCoroutines ();
		fillImage.fillAmount = 0f;
		numberText.color = Color.clear;
	}

	public void SetProgressPanel() {
		thisStats = DealHandler.Instance.GetUserStats (thisDeal);
		UpdateDevPanel ();

		if (thisStats.tries > 0) {
			bgImage.color = thisSettings.levelSelectSettings.c2;
		} else {
			bgImage.color = thisSettings.levelSelectSettings.c3;
		}
	
		if(thisStats.wins > 0)
			fillImage.fillAmount = (thisDeal as DealObject).GetProgress (thisStats.jokersUsed);

	}


	public void ResetEffects() {
		highLighter.color = Color.clear;
		numberText.color = Color.clear;
		transform.localScale = Vector3.one;
	}

	void SetProgress() {
		thisStats = DealHandler.Instance.GetUserStats (thisDeal);
		UpdateDevPanel ();

		if (thisStats.tries > 0) {
			bgImage.color = thisSettings.levelSelectSettings.c2;
		} else {
			bgImage.color = thisSettings.levelSelectSettings.c3;
		}
		fillImage.fillAmount = 0;
		if (thisStats.wins > 0) {
			if ((thisDeal as DealObject).maxJokers + (thisDeal as DealObject).freeJokers == 0)
				fillImage.fillAmount = 1f;
			else
				fillImage.fillAmount = (thisDeal as DealObject).GetProgress (thisStats.jokersUsed);
		}
	}

	public void UpdateProgressPanel (float delay, Action callback = null)
	{
		thisStats = DealHandler.Instance.GetUserStats (thisDeal);
		UpdateDevPanel ();

		if (thisStats.tries > 0) {
			bgImage.color = thisSettings.levelSelectSettings.c2;
		} else {
			bgImage.color = thisSettings.levelSelectSettings.c3;
		}
			
		float fillAmount = 0;
		if (thisDeal is DealObject) {
			if (thisStats.wins > 0) {
				if ((thisDeal as DealObject).maxJokers + (thisDeal as DealObject).freeJokers == 0)
					fillAmount = 1f;
				else
					fillAmount = (thisDeal as DealObject).GetProgress (thisStats.jokersUsed);
			}
		}
		fillImage.fillAmount = 0f;
		StartCoroutine (DoFill (delay, fillAmount, callback));
		
	}

	public void FlashText ()
	{
		if (thisSettings == null)
			return;
		StartCoroutine (DoTextColorFadeOut ());
	}

	public void HighLightPanel ()
	{
		if (thisSettings == null)
			return;
		StartCoroutine (DoHilight ());
	}

	IEnumerator DoTextColorFadeOut ()
	{
		float t = 0, animTime = 4.5f;
		Color colorFrom = thisSettings.levelSelectSettings.t, colorTo = new Color (colorFrom.r, colorFrom.g, colorFrom.b, 0);
		numberText.color = colorFrom;
		while (t < animTime) {
			numberText.color = Color.Lerp (colorFrom, colorTo, (float)Easing.SineEaseOut (t, 0, 1, animTime));
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}
		numberText.color = colorTo;
	}

	IEnumerator DoHilight ()
	{
		float t = 0, animTime = .5f;
		highLighter.rectTransform.localScale = Vector2.one;
		highLighter.color = new Color (1, 1, 1, 1);
		while (t < animTime) {
			highLighter.rectTransform.localScale = Vector2.one * (float)Easing.SineEaseOut (t, 1, 1.25f, animTime);
			highLighter.color = Color.Lerp (new Color (1, 1, 1, 1), new Color (1, 1, 1, 0), (float)Easing.SineEaseOut (t, 0, 1, animTime));
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}
		highLighter.color = new Color (1, 1, 1, 0);
	}

	IEnumerator DoFill (float delay, float fillAmount, Action callback = null)
	{
		

		yield return new WaitForSeconds (delay);

		if(fillAmount > 0) {
			if(callback == null)
				quickFill.PostAudioEvent();
			else
				fill.PostAudioEvent();
		}
		float t = 0, animTime = 1f, initFill = fillImage.fillAmount;
		while (t < animTime) {
			fillImage.fillAmount = Mathf.Lerp (initFill, fillAmount, (float)Easing.CubicEaseOut (t, 0, 1, animTime));
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}
		fillImage.fillAmount = fillAmount;
		if (callback != null)
			callback ();
	}

	public void UpdateDevPanel ()
	{
		if (thisDeal == null)
			return;
		devDealPanel.dealNumberText.text = thisDeal.dealName.ToString ();
		devDealPanel.triesText.text = "Tries: " + thisStats.tries.ToString ();
		devDealPanel.completedText.text = "Wins: " + thisStats.wins.ToString ();
		devDealPanel.difficultyText.text = "Difficulty: " + thisStats.difficulty.ToString ();
		devDealPanel.movesText.text = "Moves: " + thisStats.movesToComplete.ToString ();
		devDealPanel.jokersText.text = "Discards: " + thisStats.jokersUsed.ToString ();
		devDealPanel.funfactorText.text = "Funfactor: " + thisStats.entertaintment.ToString ();
	}

	public void Click ()
	{
		click.PostAudioEvent();
		if (!UIWorldSelector.Instance.inputEnabled)
			return; 
		if (UIWorldSelector.Instance.isSwiping)
			return;
		//if(isLocked) return;
		UIWorldSelector.Instance.inputEnabled = false;

		if (Input.GetKey (KeyCode.W)) {
			GameHandler.Instance.activeDeal = thisDeal as DealObject;
			GameHandler.Instance.dealObject = thisDeal;
			DealHandler.Instance.DevRandomWin (thisDeal as DealObject);
			UIWorldSelector.Instance.OnTransitionDone ();
			return;
		}
		if (Input.GetKey (KeyCode.E)) {
			DealHandler.Instance.DevEmptyDeal (thisDeal as DealObject);
			GameHandler.Instance.activeDeal = thisDeal as DealObject;
			GameHandler.Instance.dealObject = thisDeal;

			UIWorldSelector.Instance.OnTransitionDone ();
			return;
		}
		HighLightPanel ();
		FlashText ();
		StartCoroutine (WaitAndLoad ());
	}

	IEnumerator WaitAndLoad ()
	{
		yield return new WaitForSeconds (0.5f);
		GameHandler.Instance.currentWorldId = UIWorldSelector.Instance.currentWorldpanelIndex;
		GameHandler.Instance.InitLevel (thisDeal);
	}

	public void ToggleDevInfo (bool state)
	{
		devDealPanel.devDealTransform.gameObject.SetActive (state);
	}
}
