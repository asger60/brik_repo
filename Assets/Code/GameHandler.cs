﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.Analytics;
using UnityEngine.Serialization;
using System.Linq;


[System.Serializable]
public class GameWorldSettings
{
	public string paletteId;
	public BlockSettings[] blockSettings;
	public BlockSettings greyBlock;
	public BlockSettings levelSelectSettings;
	public Sprite worldSprite;
	public Color backgroundColor = Color.white;
	public Sprite[] blockSprites;
}

[System.Serializable]
public class BlockSettings
{
	public int blockType = 0;
	[FormerlySerializedAs("colorValue")]
	public Color c1 = Color.white; 
	[FormerlySerializedAs("colorValue2")]
	public Color c2 = Color.white; 
	[FormerlySerializedAs("colorValue3")]
	public Color c3 = Color.white; 
	[FormerlySerializedAs("textColor")]
	public Color t = Color.black;
	//public Sprite blockTypeSprite;
}


public class GameHandler : MonoBehaviour
{
	public bool testBuild;
	public Canvas mainCanvas;
	public UIInGameMenu inGameMenuController;
	int _currentWorld;
	public int currentWorldId {
		get { return _currentWorld; }
		set { 
			_currentWorld = value; 
			updateCurrentWorld.Invoke ();
		}
	}

	public enum GameState {
		InGameMenu,
		Splash,
		Menu,
		LevelSelect,
		LevelTitle,
		InGame,
		LevelEnd,
		StateTransition,
		TutorialProgress,
		GameLaunch
	}

	public BlockSettings[] colorsPalettes;


	public GameState gameState;
	public bool tutorialSequence;
	public int currentTutorialDeal = 0;


	public GameWorldSettings[] worldSettings;
	public UnityEvent updateCurrentWorld;

	static GameHandler _instance;

	public BaseDealObject dealObject;
	public DealObject activeDeal;


	World currentWorld;

	public static GameHandler Instance {
		get {
			return _instance;
		}
	}

	void Awake ()
	{
		_instance = this;
		foreach (var item in Resources.FindObjectsOfTypeAll<UIScreenBase>()) {
			item.Hide();
		}
	}

	void OnEnable() {}

	public DealObject testDeal;

	void Start ()
	{
		//if(Application.isEditor) {
		activeDeal = testDeal;
		SetGameState(GameState.InGame);
		//}
		//else
		//	SetGameState(GameState.GameLaunch);
		LevelHandler.Instance.Init();
	}


	void SetGameState(GameState newGameState) {
		
		TransitionManager.Instance.SetScreen(newGameState, ()=> {
			
			switch (newGameState) {
			case GameState.InGame:
				if(gameState != GameState.InGameMenu)
					LevelHandler.Instance.NewLevel(activeDeal);
				break;
			case GameState.LevelSelect:
				if(gameState == GameState.InGameMenu)
					AbortedLevel();
				break;
			}
			gameState = newGameState;
		});

	}

	public void StartLevel() {
		HideInGameMenu();
		SetGameState(GameState.InGame);
	}

	string GetLevelFormatedNum(BaseDealObject deal) {
		
		return (DealHandler.Instance.GetWorldNum(deal)+1).ToString() + "-" + (DealHandler.Instance.GetDealNumInWorld(deal)+1).ToString();
	}

	public DealTutorialObject tutorialObject;
	public void InitLevel(BaseDealObject deal) 
	{
		levelCompleted = false;
		dealObject = deal;
		var thisStats = DealHandler.Instance.GetUserStats (deal);
		thisStats.tries++;
		thisStats.Save ();


		if(deal is DealObject) {
			activeDeal = deal as DealObject;
			tutorialSequence = false;
			Analytics.CustomEvent ("Deal Start", new Dictionary<string, object> {
				{ "deal_name", deal.dealName },
				{ "deal_num", GetLevelFormatedNum(deal) },
			});
		} else {
			tutorialSequence = true;
			tutorialObject = deal as DealTutorialObject;
			currentTutorialDeal = 0;
			activeDeal = (deal as DealTutorialObject).tutorialDeals[currentTutorialDeal];
			Analytics.CustomEvent ("Tutorial Deal Start", new Dictionary<string, object> {
				{ "num", "0" },
			});
		}
		SetGameState(GameState.LevelTitle);
	}



	public bool levelCompleted;
	public void CompleteLevel(DealObject deal) {
		levelCompleted = true;

		var thisStats = DealHandler.Instance.GetUserStats (deal);
		thisStats.wins++;
		thisStats.jokersUsed = LevelHandler.Instance.jokersUsed;
		thisStats.movesToComplete = LevelHandler.Instance.playMoves;
		thisStats.Save ();

		Analytics.CustomEvent ("Deal Won", new Dictionary<string, object> {
			{ "deal_name", deal.dealName },
			{ "deal_num", GetLevelFormatedNum(deal) },
			{ "tries", thisStats.tries },
			{ "moves", thisStats.movesToComplete },
		});

		Invoke("GoToWinScreen", 1.5f);
	}

	void AbortedLevel() {
		Analytics.CustomEvent ("Deal Aborted", new Dictionary<string, object> {
			{ "deal_name", activeDeal.dealName },
			{ "deal_num", GetLevelFormatedNum(activeDeal) },
		});
	}

	public void UnlockWorld(int worldId) {
		Analytics.CustomEvent ("Unlock World", new Dictionary<string, object> {
			{ "world_num", worldId },
		});
	}

	public void NextTutorialDeal() {
		currentTutorialDeal++;

		if(currentTutorialDeal >= tutorialObject.tutorialDeals.Count) {
			currentTutorialDeal = 0;
			tutorialSequence = false;

			var thisStats = DealHandler.Instance.GetUserStats (dealObject);
			thisStats.wins++;
			thisStats.Save ();

			//GoToWinScreen();
			SetGameState(GameState.LevelSelect);
			return;
		}
		activeDeal = tutorialObject.tutorialDeals[currentTutorialDeal];

		Analytics.CustomEvent ("Tutorial Deal Start", new Dictionary<string, object> {
			{ "num", currentTutorialDeal.ToString() },
		});


		SetGameState(GameState.InGame);
	}



	void GoToWinScreen() {
		if(tutorialSequence) {
			SetGameState(GameState.TutorialProgress);
		} else {
			SetGameState(GameState.LevelEnd);
		}
	}


	public void SetGameWorld(World world) {
		currentWorld = world;
	}

	public GameWorldSettings GetGameWorldSettings ()
	{
		return GetGameWorldSettings(currentWorld);
	}

	public GameWorldSettings GetGameWorldSettings (World world)
	{
		if(world == null)
			return worldSettings[0];
		
		foreach (var item in worldSettings) {
			if(item.paletteId == world.colorPaletteId) return item;
		}
		return null;
	}

	public UIDevLevelInfo devInfo;
	public void ShowLevelSelect ()
	{
		LevelHandler.Instance.Reset();
		SetGameState(GameState.LevelSelect);
	}

	public void ShowIntroScreen() {
		SetGameState(GameState.Splash);
	}


	bool inGameMenuState;
	public void ToggleInGameMenu() {
		if(ObjectSelector.Instance.selectedObject != null) return;
		if(!inGameMenuState) 
			ShowInGameMenu();
		else
			HideInGameMenu();
	}

	void ShowInGameMenu() {
		SetGameState(GameState.InGameMenu);
		inGameMenuState = true;
	}

	void HideInGameMenu() {
		SetGameState(GameState.InGame);
		inGameMenuState = false;
	}

	public void RestartLevel() {
		
		var thisStats = DealHandler.Instance.GetUserStats (activeDeal);
		thisStats.tries++;
		thisStats.Save ();


		Analytics.CustomEvent ("Deal ReStart", new Dictionary<string, object> {
			{ "deal", activeDeal.dealName },
			{ "deal_num", GetLevelFormatedNum(activeDeal) },
			{ "tries", thisStats.tries },
		});
		LevelHandler.Instance.NewLevel (activeDeal as DealObject);
		HideInGameMenu();
		SetGameState(GameState.InGame);
	}

	void Update() {
		if(gameState == GameState.InGame) {
			if(Input.GetKeyDown(KeyCode.W))
				CompleteLevel(activeDeal);
		}
		if(gameState == GameState.LevelSelect && Input.GetKeyDown(KeyCode.B)) SetGameState(GameState.Splash);
	}
}

public static class Extensions
{

	public static T[] Shuffle<T> (this T[] array)
	{
		// Knuth shuffle algorithm :: courtesy of Wikipedia :)
		//UnityEngine.Random.seed = randomSeed;
		for (int t = 0; t < array.Length; t++) {
			T tmp = array [t];
			int r = UnityEngine.Random.Range (t, array.Length);
			array [t] = array [r];
			array [r] = tmp;
		}
		return array;
	}


	public static T[] Slice<T> (this T[] source, int start, int end)
	{
		// Handles negative ends.
		if (end < 0) {
			end = source.Length + end;
		}
		int len = end - start;
		
		// Return new array.
		T[] res = new T[len];
		for (int i = 0; i < len; i++) {
			res [i] = source [i + start];
		}
		return res;
	}

	public static IEnumerable<IGrouping<int, T>> GroupConsecutive<T>(this IEnumerable<T> set, Func<T, T, bool> predicate)
	{
		var i = 0;
		var k = 0;
		var ranges = from e in set
			let idx = ++i
			let next = set.ElementAtOrDefault(idx)
			let key = (predicate(e, next)) ? k : k++
			group e by key into g
			select g;
		return ranges;
	}

	public static IEnumerable<T> FindConsecutive<T>(this IEnumerable<T> data, Func<T,T,bool> comparison)
	{
		return Enumerable.Range(0, data.Count() - 1)
			.Select( i => new { a=data.ElementAt(i), b=data.ElementAt(i+1)})
			.Where(n => comparison(n.a, n.b)).Select(n => n.a);
	}


	 static IEnumerable<TSource> WhereRepeated<TSource>(this IEnumerable<TSource> source)
	{
		return WhereRepeated<TSource,TSource>(source, x => x);
	}

	public static IEnumerable<TSource> WhereRepeated<TSource, TValue>(this IEnumerable<TSource> source, Func<TSource, TValue> selector)
	{
		using (var iter = source.GetEnumerator())
		{
			if (iter.MoveNext())
			{
				var comparer = EqualityComparer<TValue>.Default;
				TValue lastValue = selector(iter.Current);
				while (iter.MoveNext())
				{
					TValue currentValue = selector(iter.Current);
					if (comparer.Equals(lastValue, currentValue))
					{
						yield return iter.Current;
					}
					lastValue = currentValue;
				}
			}
		}
	}
}
