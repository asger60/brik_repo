﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectSelector : MonoBehaviour
{

	public bool inputSelect;
	public Vector3 inputPosition;
	Vector3 localPosition, screenSize;
	public List<BaseStack> selectedStacks = new List<BaseStack>();

	float lastObjectSelect;
	BlockObject _selectedObject, oldSelectedObject, clickedObject;
	public float selectTimeThreshold = 0.1f, doubleTapTime = 0.25f;
	Vector3 startTouchPosition;

	public BlockObject selectedObject {
		get { 
			return _selectedObject; 
		}
		set {	
			if (value != null) {
				if (!clickedObject) {
					startTouchPosition = inputPosition;
					clickedObject = value;
				}

				if (Vector3.Distance (startTouchPosition, inputPosition) > 5) {
					_selectedObject = clickedObject;
					_selectedObject.selected = true;
					return;
				}

//				if (value == oldSelectedObject && Time.time < lastObjectSelect + doubleTapTime) {
//					value.DoubleClick ();
//					oldSelectedObject = null;
//					lastObjectSelect = Time.time;
//					return;
//				}


			} else {
				if (clickedObject != null && clickedObject == oldSelectedObject && Time.time < lastObjectSelect + doubleTapTime) {
					clickedObject.DoubleClick ();
					oldSelectedObject = null;
					//lastObjectSelect = Time.time;
					return;
				}


				if (_selectedObject) {
					_selectedObject.selected = false;
				}

				if (clickedObject) {
					lastObjectSelect = Time.time;
					oldSelectedObject = clickedObject;
					clickedObject = null;
				}

				_selectedObject = null;
			}
		}
	}

	public LayerMask blockMask, poleMask;


	static ObjectSelector _instance;

	public static ObjectSelector Instance {
		get {
			return _instance;
		}
	}

	void Awake ()
	{
		_instance = this;
	}

	void Start ()
	{
		screenSize = new Vector3 (Screen.width, Screen.height, 0);
	}

	// Update is called once per frame

	void Update ()
	{
		
		inputSelect = Input.GetMouseButton (0);
		inputPosition = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0);


		localPosition = inputPosition - screenSize * 0.5f;
		#if UNITY_IPHONE
		if(Input.touchCount > 0) {
			if(Input.GetTouch(0).phase == TouchPhase.Began)
				inputSelect = true;
			if(Input.GetTouch(0).phase == TouchPhase.Ended)
				inputSelect = false;
		
			inputPosition = Input.GetTouch(0).position;
			localPosition = inputPosition - screenSize*0.5f;
			
		}
		#endif



		if (GameHandler.Instance.gameState != GameHandler.GameState.InGame)
			return;



		if (inputSelect) {
			var ray = Camera.main.ScreenPointToRay (inputPosition);
			var hit = new RaycastHit ();

			BlockObject thisObj = null;
				
			float maxDist = 85 * ScreenManager.Instance.scale;
			foreach (var item in LevelHandler.Instance.gameBlocks) {
				if (!item.dragable)
					continue;
				Vector3 itemAnchor = new Vector3 (item.transform.position.x - (25 * ScreenManager.Instance.scale), item.transform.position.y, 0);
				float thisDist = Vector3.Distance (itemAnchor, localPosition);
				if (thisDist < maxDist) {
					maxDist = thisDist;
					thisObj = item;
				}
			}


			if (thisObj && selectedObject == null) {
				selectedObject = thisObj;
			}
			if (selectedObject) {
				ray = Camera.main.ScreenPointToRay (inputPosition + Vector3.up * 40);
				RaycastHit[] hitList;
				hitList = (Physics.RaycastAll (ray, 5000, poleMask)) ;
				if(hitList.Length > 0) {
					selectedStacks.Clear();
					foreach (var item in hitList) {
						if (item.transform.gameObject && item.transform.GetComponent<BaseStack> () != null) {
							selectedStacks.Add(item.transform.GetComponent<BaseStack> ());
						}
					}
				}
			}
		} else {
			selectedObject = null;
		}
	}

	void DrawCross (Vector3 position, Color color)
	{
		Debug.DrawLine (position - Vector3.right * 100, position + Vector3.right * 100, color); 
		Debug.DrawLine (position - Vector3.up * 100, position + Vector3.up * 100, color); 
	}
}
