﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu (menuName = "DealObject")]
public class DealObject : BaseDealObject {

	//public string name;
	//public string dealName;

	//public string id;
	public int randomSeed;
	public int blocksPerDeck, decksPerType;
	public int /*blocksInDeck, */blockTypes/*, decks*/, devilBlocks, stacks;
	public int maxJokers = 5;
	public int minimumStartBlocksPerStack = 0;
	public bool tutorialDeal = false;
	public float hintDelay = 2;
	public int freeJokers;
	public int funfactor;


	public int maximumStartBlocksPerStack {
		get {
			int returnInt = 5;
			if(version != 0) {
				returnInt = (int)((blocksPerDeck * decksPerType * blockTypes) / 5) - 1;
			} else {
				returnInt = (int)((blocksPerDeck * decksPerType * blockTypes) / stacks);
			}
			return returnInt;
		}
	}

	public int maxBlockNumber {
		get {
			return blocksPerDeck;
		}
	}

	[HideInInspector]
	public int version;

	public float GetProgress(int jokersUsed) {
		if(maxJokers == 0) return 1f;
		jokersUsed -= freeJokers;
		return 1f - (float)((float)jokersUsed / (float)(maxJokers+0.5f));
	}

	public DealOrderBlock[] fixedDealOrder;
	public int[] fixedStackSize;

	public DealHint[] hints;
}

[System.Serializable]
public class DealOrderBlock {
	public int blockNumber = -1;
	public int blockType = -1;
}

[System.Serializable]
public class DealOrderStack {
	public enum StackType {
		none,
		normalStack,
		dealStack,
		devilStack,
		winStack
	}
	public StackType stackType = StackType.none;
	public int stackNum = -1;
}

[System.Serializable]
public class DealHint {
	public int moveNumber;
	public enum HintConditions {
		none,
		CardsInStack,
	}
	public HintConditions hintCondition;

	public DealOrderBlock fromBlock, toBlock;
	public DealOrderStack fromStack, toStack;
	public float delay;
	public int repeats;
	public float repeatRate;

	public bool EvaluateCondition() {
		switch (hintCondition) {
		case HintConditions.none:
			return true;
		case HintConditions.CardsInStack:
			if(LevelHandler.Instance.dealStack[0].blockObjects.Count > 0) 
				return true;
			else
				return false;
		}
		return true;
	}
}

