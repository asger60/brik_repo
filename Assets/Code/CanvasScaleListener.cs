﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasScaleListener : MonoBehaviour {

	RectTransform thisTransform;

	void Start() {
		thisTransform = transform.GetComponent<RectTransform>();
		Canvas.ForceUpdateCanvases();
		OnRectTransformDimensionsChange();
		//OnRectTransformDimensionsChange();
		Invoke("OnRectTransformDimensionsChange", 0.1f);
	}

	void OnRectTransformDimensionsChange()
	{
		if(!thisTransform) return;
		if(ScreenManager.Instance)
			ScreenManager.Instance.Refresh();
	}

}
