﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HintHandler : MonoBehaviour
{
	public GameObject hintPingTemplate, hintDotTemplate;
	public List<HighLighter> highLighters = new List<HighLighter>();
	public List<HintPathDot> dots = new List<HintPathDot>();

	public static HintHandler _instance;
	public static HintHandler Instance {
		get {
			return _instance;
		}
	}

	void Awake() {
		_instance = this;
		for (int i = 0; i < 5; i++) {
			var newHighlighter = (Instantiate(hintPingTemplate).transform.GetComponent<HighLighter>());
			highLighters.Add(newHighlighter);
			highLighters[i].transform.SetParent(transform);
			highLighters[i].transform.localPosition = Vector3.zero + Vector3.forward*0.1f;
		}

		for (int i = 0; i < 30; i++) {
			var newDot = (Instantiate(hintDotTemplate).transform.GetComponent<HintPathDot>());
			dots.Add(newDot);
			dots[i].transform.SetParent(transform);
			dots[i].transform.localPosition = Vector3.zero + Vector3.forward*0.1f;
		}
	}

	DealHint currentHint = null;
	//Transform fromTransform, toTransform;
	//BlockObject moveBlock;
	//BaseStack moveToStack;
	public float generalRepeatRate = 1;

	bool _inLevel;
	bool inLevel {
		get { return _inLevel; }
		set {
			if(value != _inLevel && !value) {
				StopAllCoroutines();
			}
			_inLevel = value;
		}
	}

	void Update() {
		if(LevelHandler.Instance.levelState == LevelHandler.LevelState.running)
			inLevel = true;
		else
			inLevel = false;
	}



	public void ShowHint(DealHint hint) {
		if(hint == null) {
			CancelActiveHints();
			return;
		}

		if(hint != currentHint) {
			CancelActiveHints();
		}

		if(!hint.EvaluateCondition()) {
			CancelActiveHints();
			return;
		}

		currentHint = hint;
		BlockObject targetBlock = null;
		BaseStack targetStack = null;
//
//		if(hint.toBlock.blockNumber != 0) {
//			targetStack = (LevelHandler.Instance.GetBlockObjectFromOrder(hint.toBlock));
//		}

		if(hint.fromBlock.blockNumber != 0) {
			targetBlock = (LevelHandler.Instance.GetBlockObjectFromOrder(hint.fromBlock));
		}

		if(hint.toStack.stackType != DealOrderStack.StackType.none) {
			targetStack = GetStack(hint.toStack);
		}
//		if(hint.fromStack.stackType != DealOrderStack.StackType.none) {
//			targetBlock = GetStack(hint.fromStack);
//		}


		if(targetBlock != null && targetStack != null) {
			DrawConnection(targetBlock, targetStack, (GameHandler.Instance.activeDeal as DealObject).hintDelay); 
			//Invoke("RepeatHint", generalRepeatRate);
		} 
//		else if(targetStack != null) {
//			StartCoroutine(DoHighLight((GameHandler.Instance.activeDeal as DealObject).hintDelay, targetStack.transform.position));
//			//Invoke("RepeatHint", generalRepeatRate);
//		}

	}


	void CancelActiveMoves() {
		CancelInvoke("RepeatMove");
		StopAllCoroutines();
		foreach (var item in dots) {
			if(item.running) item.EndDraw();
		}
	}

	void CancelActiveHints() {
		currentHint = null;
		CancelInvoke("RepeatHint");
		StopAllCoroutines();
		foreach (var item in dots) {
			if(item.running) item.EndDraw();
		}
	}



	void RepeatHint() {
		ShowHint(currentHint);
	}

	void DrawConnection(BlockObject targetBlock, BaseStack targetStack, float delay) {
		StartCoroutine(DoDrawConnection(targetBlock, targetStack, delay));
	}

	IEnumerator DoDrawConnection(BlockObject targetBlock, BaseStack targetStack, float delay) {
		yield return new WaitForSeconds(delay);
		targetBlock.PingBlock();
		float t = 0, animTime = 1f;

		Transform targetTransform = null;

		if(targetStack.GetTopBlock() != null) 
			targetTransform = targetStack.GetTopBlock().transform;
		else
			targetTransform = targetStack.transform;

		float middleY = targetBlock.transform.position.y + (targetTransform.position.y - targetBlock.transform.position.y) / 2f;



		Vector3 startPosition = targetBlock.transform.position; 
		Vector3 endPosition = targetTransform.position;
		Vector3 middlePosition = new Vector3(0, middleY, 0);


		animTime = (Vector3.Distance (startPosition, middlePosition) / ScreenManager.Instance.scale) * 0.002f;
		float dotRate = 0.1f, dotRateTimer = 0;
		if (animTime < 0.1f)
			animTime = 0.1f;


		while (t < animTime) {
			Vector3 gotoPos = Vector3.zero;
			gotoPos.x = Mathf.Lerp (startPosition.x, middlePosition.x, (float)Easing.SineEaseOut (t, 0, 1, animTime));
			gotoPos.y = Mathf.Lerp (startPosition.y, middlePosition.y, (float)Easing.SineEaseIn (t, 0, 1, animTime));
			gotoPos.z = Mathf.Lerp (startPosition.z, middlePosition.z, (float)Easing.SineEaseInOut (t, 0, 1, animTime));
			dotRateTimer += Time.deltaTime;
			//Debug.DrawLine(gotoPos, gotoPos + Vector3.up*10, Color.red, 5);
			if(dotRateTimer > dotRate) {
				GetFreeDot().DrawDot(gotoPos);
				dotRateTimer = 0;
			}
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}

		t = 0;
		animTime = Vector3.Distance (middlePosition, endPosition) * 0.002f / ScreenManager.Instance.scale;
		while (t < animTime) {
			Vector3 gotoPos = Vector3.zero;
			gotoPos.x = Mathf.Lerp (middlePosition.x, endPosition.x, (float)Easing.SineEaseIn (t, 0, 1, animTime));
			gotoPos.y = Mathf.Lerp (middlePosition.y, endPosition.y, (float)Easing.SineEaseOut (t, 0, 1, animTime));
			gotoPos.z = Mathf.Lerp (middlePosition.z, endPosition.z, (float)Easing.SineEaseInOut (t, 0, 1, animTime));
			dotRateTimer += Time.deltaTime;
			//Debug.DrawLine(gotoPos, gotoPos + Vector3.up*10, Color.red, 5);

			if(dotRateTimer > dotRate) {
				GetFreeDot().DrawDot(gotoPos);
				dotRateTimer = 0;
			}
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}


		GetFreeHighLighter().HighLight(endPosition);
		DrawConnection(targetBlock, targetStack, delay);
	}

	void Hilight(BaseStack targetStack, float delay) {
		StartCoroutine(DoHighLight(targetStack, delay));
	}

	IEnumerator DoHighLight(BaseStack targetStack, float delay) {
		yield return new WaitForSeconds(delay);
		GetFreeHighLighter().HighLight(targetStack.transform.position);
		Hilight(targetStack, delay);
	}

	HintPathDot GetFreeDot() {
		foreach (var item in dots) {
			if(!item.running) return item;
		}
		return null;
	}

	HighLighter GetFreeHighLighter() {
		foreach (var item in highLighters) {
			if(!item.running) return item;
		}
		return null;
	}

	BaseStack GetStack(DealOrderStack dealStack) {
		switch (dealStack.stackType) {
		default:
		case DealOrderStack.StackType.dealStack:
			return LevelHandler.Instance.dealStack[dealStack.stackNum];

		case DealOrderStack.StackType.devilStack:
			return LevelHandler.Instance.devilStacks[dealStack.stackNum];

		case DealOrderStack.StackType.normalStack:
			return LevelHandler.Instance.normalStacks[dealStack.stackNum];

		}
	}
}

