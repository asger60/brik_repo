﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using System.Runtime.Serialization.Formatters.Binary; 
using System.IO;
using UnityEngine.SceneManagement;
using System;
using Malee;
using Assets.ReorderableList;
using Assets.ReorderableList.Helpers;
using UnityEngine.Serialization;

[System.Serializable]
public class WorldUserStats { 
	public string worldName;
	public string worldId;

}

[System.Serializable]
public class DealUserStats {
	public string dealName;
	public string dealId;
	public int tries = 0;
	public int wins = 0;
	public int movesToComplete = 0;
	public bool like = false;
	public int difficulty = 0;
	public int entertaintment = 0;
	public int jokersUsed;
	bool tmpUser;

	public DealUserStats (string dealId, string name, bool tmpUser) {
		this.dealId = dealId;
		this.dealName = name;
		this.tmpUser = tmpUser;
		Load();
	}

	public void Save() {
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (GetFileName(tmpUser));
		bf.Serialize(file, this);
		file.Close();
	}

	public void Load() {
		if(File.Exists(GetFileName(tmpUser))) {
			BinaryFormatter bf = new BinaryFormatter();

			if(!File.Exists(GetFileName(tmpUser))) {
				Debug.Log("nothing to open");
				return;
			}
			FileStream file = File.Open(GetFileName(tmpUser), FileMode.Open);
			if(file.Length == 0) {
				Debug.Log("nothing in file");
				file.Close();
				return;
			}
			var thisStats = (DealUserStats)bf.Deserialize(file);
			file.Close();
			PopulateObject(thisStats);
		}
	}

	void PopulateObject(DealUserStats thisStats) {
		dealName = thisStats.dealName;
		dealId = thisStats.dealId;
		tries = thisStats.tries;
		wins = thisStats.wins;
		like = thisStats.like;
		difficulty = thisStats.difficulty;
		movesToComplete = thisStats.movesToComplete;
		jokersUsed = thisStats.jokersUsed;
		entertaintment = thisStats.entertaintment;
		tmpUser = thisStats.tmpUser;
	}

	string GetFileName(bool tmpUser) {
		string extension = ".ioio";
		if(tmpUser)
			extension = "tmpioio";
		return Application.persistentDataPath + "/"+dealId.ToString()+extension;
	}

	public void Delete() {
		File.Delete(GetFileName(tmpUser));
	}

}


[System.Serializable]
public class World { 
	public string worldName;
	[HideInInspector]
	public int version;
	public int worldNum;
	public string colorPaletteId;
	public DealTutorialObject tutorialObject;

	public int completedDealsToUnlockNext = 16;





	//public List<BaseDealObject> dealObjects = new List<BaseDealObject>();

	[Serializable]
	public class ListOfDeals : ReorderableList.Of<BaseDealObject> { }

	public ListOfDeals dealList;


}

public class DealHandler : MonoBehaviour {

	[Serializable]
	public class ListOfWorld : ReorderableList.Of<World> { }

	//[FormerlySerializedAs("worldList")]
	public ListOfWorld worldList;




	//public World[] worlds = new World[3];

	//public List<DealObject> tutorialDeals = new List<DealObject>();

	public List<DealUserStats> dealUserStats;
	public bool tmpUser = false;
	public int currentVersion = 0;
	static DealHandler _instance;
	public static DealHandler Instance {
		get {
			return _instance;
		}
	}

	void Awake() {
		_instance = this;
	}



	public DealUserStats GetUserStats(BaseDealObject deal) {	
		if(deal == null) return null;

		foreach (var item in dealUserStats) {
			if(item.dealId == deal.id) 
				return item;
		}
		var thisStats = new DealUserStats(deal.id, deal.name, tmpUser);
		dealUserStats.Add(thisStats);
		return thisStats;
	}

	public int GetDealNumInWorld(BaseDealObject deal) {
		foreach (var item in worldList) {
			for (int i = 0; i < item.dealList.Count; i++) {
				if((item.dealList[i]).id == deal.id) 
					return i;
			}
		}
		return -1;
	}

	public int GetWorldNum(BaseDealObject deal) {
		for (int iy = 0; iy < worldList.Count; iy++) {
			
			var item = worldList[iy];

			for (int i = 0; i < item.dealList.Count; i++) {
				if((item.dealList[i]).id == deal.id) 
					return iy;
			}
		}
		return -1;
	}


	public void DevDeleteAllSaveFiles() {
		foreach (string f in Directory.GetFiles(Application.persistentDataPath,"*.ioio"))
		{
			File.Delete(f);
		}
		//File.Delete(Application.persistentDataPath + "/*.ioio");
	}

	public void DevStartTestUser() {
		tmpUser = true;
		dealUserStats.Clear();
		foreach (var item in worldList) {
			foreach (var subItem in item.dealList) {
				var stats = GetUserStats((DealObject)subItem);
				stats.Delete();
			}
		}
		dealUserStats.Clear();
		UIWorldSelector.Instance.ReInit();
	}

	public void DevRandomWin(DealObject deal) {
		var thisStats = GetUserStats(deal);
		thisStats.wins++;
		thisStats.tries++;
		thisStats.jokersUsed = UnityEngine.Random.Range(0,4);
		thisStats.Save();
	}

	public void DevEmptyDeal(DealObject deal) {
		var thisStats = GetUserStats(deal);
		thisStats.wins = 0;
		thisStats.tries = 0;
		thisStats.Save();
	}
}

