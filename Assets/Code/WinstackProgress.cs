﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinstackProgress : MonoBehaviour {

	public Image progressImage;
	int maxCount;
	Color thisColor;

	public void Init(int maxCount, Color color) {
		this.maxCount = maxCount;
		thisColor = new Color(color.r, color.g, color.b, 0.25f);
		progressImage.rectTransform.sizeDelta = Vector2.zero;
	}


	public void UpdateProgress(int currentCount) {
		StartCoroutine(ShowProgress((float)Screen.height * (float)currentCount/(float)maxCount));
	}


	IEnumerator ShowProgress(float height) {
		float t = 0, animDuration = 1.5f;
		Color fadeToColor = new Color(thisColor.r, thisColor.g, thisColor.b, 0);
		progressImage.color = fadeToColor;
		Vector3 startSize = progressImage.rectTransform.sizeDelta;
		progressImage.rectTransform.sizeDelta = Vector2.one;
		float thisSize = (height * ScreenManager.Instance.scale)*0.75f;
		while(t < animDuration) {
			progressImage.rectTransform.sizeDelta = Vector2.Lerp(startSize, Vector2.one*thisSize, (float)Easing.CircEaseOut(t, 0, 1, animDuration));
			progressImage.color = Color.Lerp(thisColor, fadeToColor, (float)Easing.BackEaseIn(t, 0, 1, animDuration));
			t += Time.deltaTime;
			yield return new WaitForFixedUpdate ();
		}
		progressImage.color = fadeToColor;
		progressImage.rectTransform.sizeDelta = Vector3.one;
	}
}
