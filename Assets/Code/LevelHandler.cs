﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.Analytics;
using System;
using System.Linq;

[Serializable]
public class StackSettings {
	public GameObject template;
	public float space;
	public Transform parent;
	public enum Aligment {
		Horizontal,
		Vertical
	}
	public Aligment alignment;
}

[System.Serializable]
public class BlockMatch {
	public List<BlockObject> blocks;
	public BlockMatch(List<BlockObject> blocks) {
		this.blocks = blocks;
	}


}

public class LevelHandler : MonoBehaviour
{
	static LevelHandler instance;

	public static LevelHandler Instance {
		get { return instance; }
	}

	void Awake ()
	{
		instance = this;
	}

	public StackSettings normalStackSettings, devilStackSettings, winStackSettings, dealStackSettings;


	public Canvas mainCanvas;
	public NormalStack[] normalStacks;
	public DevilStack[] devilStacks;
	public DealStack[] dealStack;
	public WinStack[] winstack;
	public int playMoves;
	public int jokersUsed = 0;


	public BlockObject blockTemplate;
	public BlockObject[] gameBlocks;

	public Transform worldScaleTransform;


	int blocksTotal;
	DealObject activeDeal;
	public enum LevelState {
		building,
		running,
		ending
	}
	public LevelState levelState = LevelState.building;

	public List<WinStack> completedStacks = new List<WinStack> ();

	public void Init ()
	{
		foreach (var item in GameObject.FindObjectsOfType<BaseStack>()) {
			Destroy (item.gameObject);
		}
		levelState = LevelState.building;

	}

	public int GetFreeJokersLeft() {
		return activeDeal.freeJokers-jokersUsed;
	}

	public int GetJokersLeft() {
		return (activeDeal.maxJokers+activeDeal.freeJokers) -jokersUsed;
	}

	int matchChain = 0;
	public List<BlockMatch> blockMatches = new List<BlockMatch>();
	public void CheckForMatches() {
		if(levelState != LevelState.running) return;
		print("CheckForMatches");
		blockMatches.Clear();


		for (int columnIndex = 0; columnIndex < 10; columnIndex++) {
			List<BlockObject> matchedBlocks = new List<BlockObject>();

			for (int rowIndex = 0; rowIndex < normalStacks.Length; rowIndex++) {
				if(columnIndex >= normalStacks[rowIndex].blockObjects.Count) {
					matchedBlocks.Add(null);
					continue;
				}
				matchedBlocks.Add(normalStacks[rowIndex].blockObjects[columnIndex]);
			}

			var groups = matchedBlocks.GroupConsecutive((b1, b2) => (CheckMatch(b1, b2)));
			foreach (var g in groups)
			{
				if(g.Count() >= 3) {
					blockMatches.Add(new BlockMatch(g.ToList()));
					print("Vertical " + g.Key);
					foreach (var b in g)
						print("\t" + b);
				}
			}
		}


		for (int rowIndex = 0; rowIndex < normalStacks.Length; rowIndex++) {
			List<BlockObject> matchedBlocks = new List<BlockObject>();
			for (int columnIndex = 0; columnIndex < normalStacks[rowIndex].blockObjects.Count; columnIndex++) {
				matchedBlocks.Add(normalStacks[rowIndex].blockObjects[columnIndex]);
			}
			var groups = matchedBlocks.GroupConsecutive((b1, b2) => (CheckMatch(b1, b2)));
			foreach (var g in groups)
			{
				if(g.Count() >= 3) {
					blockMatches.Add(new BlockMatch(g.ToList()));
					print("Horizontal" + g.Key);
					foreach (var b in g)
						print("\t" + b);
				}
			}
		}



		if(blockMatches.Count() > 0) {
			StartCoroutine(HilightAndMoveBLocks(blockMatches, ()=>{
				ReorderStacks(()=>{
					matchChain++;
					CheckForMatches();
				});
			}));





		} else {
			if(matchChain > 0) {
				dealStack[0].Draw(()=>{
					CheckForMatches();
				});
			}
			matchChain = 0;
		}
	}

	IEnumerator HilightAndMoveBLocks(List<BlockMatch> blockMatches, Action callBack) {
		foreach (var item in blockMatches) {
			foreach (var block in item.blocks) {
				block.HighLight(true);
			}
		}
		yield return new WaitForSeconds(0.25f);
		foreach (var item in blockMatches) {
			foreach (var block in item.blocks) {
				block.HighLight(false);
				block.currentStack.RemoveBlock(block);
				block.AddToStack(winstack[0]);
				block.MoveToStack(winstack[0]);
				block.dragable = false;
			}
		}
		callBack();
	}

	void ReorderStacks(Action callback) {
		foreach (var item in normalStacks) {
			item.ReorderBlockPositions(()=> {
				
			});
		}
		StartCoroutine(DoReorderStacks(callback));
	}

	IEnumerator DoReorderStacks(Action callback) {
		yield return new WaitForSeconds(1);
		callback();
	}

	bool CheckMatch(BlockObject c1, BlockObject c2) {
		if(c1 == null || c2 == null) return false;
			
		if(c1.blockNumber == c2.blockNumber) 
			return true;
		return false;
	}

	public void AddPlayMove() {
		if(levelState != LevelState.running) return; 
		playMoves++;
		var thisHint = GetHint(playMoves);
		HintHandler.Instance.ShowHint(thisHint);
	}


	DealHint GetHint(int move) {
		if(activeDeal.hints.Length == 0) return null;
		foreach (var item in activeDeal.hints) {
			if(item.moveNumber == move) return item;
		}
		return null;
	}

	public void StackComplete (WinStack stack)
	{
		if(stack.completed) {
			print("Stack already completed: " + stack.name);
			return;
		}
		print("Stack Complete: " + stack.name);
		completedStacks.Add (stack);
		stack.completed = true;

		if (completedStacks.Count >= activeDeal.decksPerType * activeDeal.blockTypes) {

			levelState = LevelState.ending;
			print ("game won");
			GameHandler.Instance.CompleteLevel(activeDeal);
			foreach (var item in normalStacks) {
				item.FadeOut();
			}
			dealStack[0].FadeOut();
			devilStacks[0].FadeOut();
		}
	}



	BlockObject CreateBlock (int id, BlockSettings settings, string name)
	{
		var newBlock = Instantiate (blockTemplate);
		newBlock.name = name;

		//newBlock.transform.position = GetDrawButtonPosition();
		newBlock.transform.SetParent(worldScaleTransform, false);

		var block = newBlock.transform.GetComponent<BlockObject> ();
		//block.hidden = true;
		block.blockNumber = id;
		//block.blockNumber = 1;
		block.blockSettings = settings;
		return block;
	}





	public void NewLevel (DealObject deal)
	{
		levelState = LevelState.building;
		jokersUsed = 0;
		Reset();
		activeDeal = deal;
		completedStacks.Clear ();
		CreateLevel ();
		playMoves = 0;
	}

	T[] CreateStacks<T>(int amount, StackSettings settings) 
	{
		List<T> returnPoles = new List<T>();

		float curOfset = 0;
		for (int i = 0; i < amount; i++) {
			var newStack = Instantiate(settings.template);
			returnPoles.Add(newStack.transform.GetComponent<T>());
			newStack.transform.name = typeof(T).ToString() + "_Stack"+i.ToString();
			newStack.transform.SetParent(settings.parent, false);


			if(settings.alignment == StackSettings.Aligment.Vertical) {
				newStack.transform.localPosition = new Vector3(0, curOfset, 0);
				curOfset -= LevelHandler.Instance.blockTemplate.size.y + settings.space * ScreenManager.Instance.scale;
			}
			else if(settings.alignment == StackSettings.Aligment.Horizontal) {
				newStack.transform.localPosition = new Vector3(curOfset, 0, 0);
				if(typeof(T) == typeof(WinStack)) {
					if(amount > 6) {
						curOfset += LevelHandler.Instance.blockTemplate.size.x - 75;
					} else {
						curOfset += LevelHandler.Instance.blockTemplate.size.x - 50;
					}
				} 
				else 
				{
					curOfset += LevelHandler.Instance.blockTemplate.size.x + settings.space;
				}
			}


		}
		return returnPoles.ToArray();
	}



	void AlignStack(BaseStack[] stack, StackSettings.Aligment direction, float centerVal) {

		float stackSize = 0;
		if(direction == StackSettings.Aligment.Vertical) 
			stackSize = stack[stack.Length-1].transform.position.y - stack[0].transform.position.y;
		else if(direction == StackSettings.Aligment.Horizontal)
			stackSize = stack[stack.Length-1].transform.position.x - stack[0].transform.position.x;
		
		float centerOffset = centerVal - (stackSize * 0.5f);

		foreach (var item in stack) {
			if(direction == StackSettings.Aligment.Vertical) 
				item.transform.position = new Vector3(item.transform.position.x, item.transform.position.y + centerOffset, item.transform.position.z);
			
			else if(direction == StackSettings.Aligment.Horizontal)
				item.transform.position = new Vector3(item.transform.position.x + centerOffset, item.transform.position.y, item.transform.position.z);
					
		}
	}

	void PositionStack(BaseStack stack, Vector2 initPosition, Vector2 offsetPosition) {
		initPosition /= ScreenManager.Instance.scale;
		stack.transform.localPosition = new Vector3(initPosition.x, initPosition.y, 0) + new Vector3(offsetPosition.x, offsetPosition.y, 0);
	}

	int stackIndent = 80;
	void CreateLevel ()
	{

		normalStacks =  CreateStacks <NormalStack>(activeDeal.stacks, normalStackSettings);
		AlignStack(normalStacks, StackSettings.Aligment.Vertical, Screen.height*0.5f);


		dealStack = CreateStacks<DealStack>(1, dealStackSettings);
		winstack = CreateStacks<WinStack>(1, winStackSettings);
		PositionStack(dealStack[0], new Vector2(Screen.width, Screen.height), new Vector2(-stackIndent, -stackIndent));


		int blockTypeCounter = 0;



		blocksTotal = activeDeal.blocksPerDeck * activeDeal.decksPerType * activeDeal.blockTypes;


		//creating game blocks
		blockTypeCounter = 0;
		gameBlocks = new BlockObject[blocksTotal];
		int counter = 1;
		for (int i = 0; i < gameBlocks.Length; i++) {
			gameBlocks [i] = CreateBlock (counter, GameHandler.Instance.GetGameWorldSettings ().blockSettings [blockTypeCounter], "block_" + counter);

			counter++;
			if (counter > (GameHandler.Instance.activeDeal as DealObject).maxBlockNumber) {
				counter = 1;
				blockTypeCounter++;
				if (blockTypeCounter >= activeDeal.blockTypes) {
					blockTypeCounter = 0;
				}
			}
		}


		UnityEngine.Random.InitState (activeDeal.randomSeed);
		if(activeDeal.fixedDealOrder.Length > 0) {
			gameBlocks = GetSortedList(activeDeal.fixedDealOrder); 
		}
		else
			gameBlocks.Shuffle ();

		//creating devil stacks
		int startIndex = 0, endIndex = activeDeal.devilBlocks;
		if(activeDeal.devilBlocks > 1) {
			float width = activeDeal.devilBlocks * devilStacks[0].overlapWidth + 75;
			float center = (Screen.width * 0.5f) - (width * 0.5f);
			PositionStack(devilStacks[0], new Vector2(center, Screen.height), new Vector2(0,-stackIndent));
		}
		if(activeDeal.devilBlocks > 0) {
			devilStacks[0].CreateStack(gameBlocks.Slice (startIndex, endIndex));
			print("step random function, " + (int)UnityEngine.Random.Range (0, 24));
		}

		startIndex = endIndex;

		int[] stackLengths = new int[normalStacks.Length];
		for (int i = 0; i < normalStacks.Length; i++) {
			stackLengths[i] = (int)UnityEngine.Random.Range (activeDeal.minimumStartBlocksPerStack, activeDeal.maximumStartBlocksPerStack);
			if(i==0 && activeDeal.devilBlocks <= 0) {
				print("step random function, " + (int)UnityEngine.Random.Range (0, 24));	
			}
		}

		//creating normal stacks
		for (int i = 0; i < normalStacks.Length; i++) {
			//todo 
			if(activeDeal.fixedStackSize.Length > 0) 
				endIndex = startIndex + activeDeal.fixedStackSize[i];
			else
				endIndex = startIndex + stackLengths[i];

			var stackBlocks = gameBlocks.Slice (startIndex, endIndex);



			normalStacks [i].CreateStack (stackBlocks);

			normalStacks [i].Init(GameHandler.Instance.GetGameWorldSettings(), activeDeal.maxBlockNumber);
			if (i != normalStacks.Length - 1) {
				startIndex = endIndex;
			}
		}

		List<BlockObject> dealStackObjects = new List<BlockObject>();


		for (int i = 0; i < gameBlocks.Length; i++) {
			if(!gameBlocks [i].inStartStack) continue;
			dealStackObjects.Add(gameBlocks[i]);
		}
		dealStackObjects.Reverse();
		dealStack[0].CreateStack(dealStackObjects.ToArray());
		dealStack[0].Init(GameHandler.Instance.GetGameWorldSettings(), activeDeal.maxBlockNumber);

		levelState = LevelState.running;
		playMoves = -1;
		AddPlayMove();
		CheckForMatches();
	}

	BlockObject[] GetSortedList(DealOrderBlock[] order) {
		List<BlockObject> returnList = new List<BlockObject>();
		foreach (var item in order) {
			
			returnList.Add(GetBlockObjectFromOrder(item));
		}
		return returnList.ToArray();
	}

	public BlockObject GetBlockObjectFromOrder(DealOrderBlock thisOrder) {
		foreach (var item in gameBlocks) {
			if(item.blockNumber == thisOrder.blockNumber && item.blockSettings.blockType == thisOrder.blockType)
				return item;
		}	
		Debug.LogWarning("could not find that block " + thisOrder.blockNumber + " " + thisOrder.blockType);
		return null;
	}

	public void Reset ()
	{
		levelState = LevelState.ending;
		if(normalStacks != null) {
			foreach (var item in normalStacks) {
					item.Reset ();
			}
			normalStacks = null;
		}

		if(dealStack != null && dealStack.Length > 0 && dealStack[0].gameObject != null) {
			dealStack[0].Reset();
			dealStack = null;
		}

		if(devilStacks != null && devilStacks.Length > 0) {
			devilStacks[0].Reset();
			devilStacks = null;
		}



		if(gameBlocks != null) {
			foreach (var item in gameBlocks) {
				if(item != null && item.gameObject != null)
					Destroy (item.gameObject);
			}
			gameBlocks = null;
		}

	}




}
