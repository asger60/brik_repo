﻿using UnityEngine;
using System.Collections;

public class BlockStackHolder : MonoBehaviour {

	public GameObject groundPlane;


	void Start() {
		groundPlane.GetComponent<MeshRenderer>().material.color = GameHandler.Instance.GetGameWorldSettings().backgroundColor;
		GameHandler.Instance.updateCurrentWorld.AddListener(()=>{
			groundPlane.GetComponent<MeshRenderer>().material.color = GameHandler.Instance.GetGameWorldSettings().backgroundColor;
		});

	}


	// Update is called once per frame
	void Update () {
		//groundPlane.transform.position = transform.position;
	}
}
