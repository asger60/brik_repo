﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class NormalStack : BaseStack {

	//public Text maxNumberText;
	//public Image borderImage;

	public override bool CanAdd (BlockObject blockObject)
	{
		if(blockObjects.Count == 0) return true;
		if(blockObject.blockSettings.blockType == blockObjects[blockObjects.Count-1].blockSettings.blockType) 
			return false;
		
		return true;
	}

	public override void AddBlock (BlockObject blockObject)
	{
		base.AddBlock (blockObject);
		UpdateDragability();
	}
		
	public override void RemoveBlock (BlockObject blockObject)
	{
		base.RemoveBlock (blockObject);
		UpdateBlockHierachy();
		UpdateDragability();
	}

	public override void FadeOut ()
	{
		StartCoroutine(DoFadeOut());
	}

	public override void StackArrive (BlockObject block)
	{
		base.StackArrive (block);
		if(block.childObject == null)
			LevelHandler.Instance.CheckForMatches();
	}

	public void ReorderBlockPositions(Action callback = null) {
		for (int i = 0; i < blockObjects.Count; i++) {
			var newPostion = transform.position - new Vector3(transform.localScale.x,0,0) + ((Vector3.right * overlapWidth) * (i+1));
			blockObjects[i].MoveToPosition(newPostion, ()=> {
				UpdateBlockHierachy();
				UpdateDragability();
				if(callback != null) callback();
			});
		}

	}

	IEnumerator DoFadeOut() {
		float t=0, animTime = 0.75f;
		//Color textStartColor = maxNumberText.color, borderStartColor = borderImage.color;
		while (t<animTime) {
		//	maxNumberText.color = Color.Lerp(textStartColor, new Color(textStartColor.r,textStartColor.g,textStartColor.b,0), t/animTime);
		//	borderImage.color = Color.Lerp(borderStartColor, new Color(borderStartColor.r,borderStartColor.g,borderStartColor.b,0), t/animTime);

			yield return new WaitForFixedUpdate();
			t+=Time.fixedDeltaTime;
		}
		//borderImage.color = Color.clear;
		//maxNumberText.color = Color.clear;
	}

	public void Init(GameWorldSettings settings, int maxBlockNumber) {
		//borderImage.color = settings.greyBlock.c1;
		//maxNumberText.color = settings.greyBlock.c1;
		//maxNumberText.text = maxBlockNumber.ToString();
	}

	public override void CreateStack (BlockObject[] blocks)
	{		
		for (int i = 0; i < blocks.Length; i++) {
			blocks[i].inStartStack = false;
			blocks[i].AddToStack(this);
			blocks[i].transform.position = GetNextStackPosition();
			blocks[i].SetSortingOrder(blockObjects.Count);
			blocks[i].RevealSequence();
		}
	}


	public void RevealNextBlock() {
		if(blockObjects.Count == 0) return;
		blockObjects[blockObjects.Count-1].RevealSequence();
	}

	void UpdateBlockHierachy() {
		if(blockObjects.Count == 0) return;
		blockObjects[blockObjects.Count-1].childObject = null;
	}

	public BlockObject GetParentBlock(BlockObject thisBlock) {
		if(blockObjects.Count == 0) return null;

		for (int i = 0; i < blockObjects.Count; i++) {
			if(blockObjects[i] == thisBlock && i > 0) 
				return blockObjects[i-1];
		}
		return null;
	}


	public override Vector3 GetNextStackPosition() {
		var returnPos = transform.position + ((Vector3.right * overlapWidth) * (blockObjects.Count));
		return returnPos;
	}
}
