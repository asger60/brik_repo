﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections.Generic;



[CustomPropertyDrawer (typeof(GameWorldSettings))]
public class GameHandlerInspector : PropertyDrawer
{

	List<ReorderableList> lists = new List<ReorderableList> ();

	private ReorderableList GetList (SerializedProperty property)
	{
		ReorderableList list;
		list = new ReorderableList (property.serializedObject, property, true, true, true, true);

		foreach (var item in lists) {
			if(item.serializedProperty.propertyPath == property.propertyPath) return item;
		}



		list = new ReorderableList (property.serializedObject, property, true, true, true, true);
		list.drawHeaderCallback = (Rect rect) => {
			EditorGUI.LabelField (rect, property.displayName);
		};
		list.drawElementCallback = (Rect position, int index, bool isActive, bool isFocused) => {
			var element = property.GetArrayElementAtIndex (index);
			DrawBlockSettings(element, position);
		};
		lists.Add (list);
		return list;
	}

	void DrawBlockSettings(SerializedProperty element, Rect position) {
		List<SerializedProperty> properties = new List<SerializedProperty> ();
		properties.Add (element.FindPropertyRelative ("c1"));
		properties.Add (element.FindPropertyRelative ("c2"));
		properties.Add (element.FindPropertyRelative ("c3"));
		properties.Add (element.FindPropertyRelative ("t"));

		// Don't make child fields be indented
		var indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		int fieldSize = (int)position.width / 4;
		for (int i = 0; i < properties.Count; i++) {
			var labelFieldRect = new Rect (position.x + (i * fieldSize), position.y, fieldSize, position.height);
			var colorFieldRect = new Rect (position.x + (i * fieldSize), position.y + 15, fieldSize, position.height - 15);
			EditorGUI.LabelField (labelFieldRect, properties [i].displayName);
			EditorGUI.PropertyField (colorFieldRect, properties [i], GUIContent.none);
		}


		EditorGUI.indentLevel = indent;
	}

	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{
		return 310;
	}

	Color bgColor = Color.clear; 
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{		
		//if(bgColor == Color.clear)
			bgColor = property.FindPropertyRelative ("backgroundColor").colorValue;
		
		EditorGUI.DrawRect(new Rect(0,position.y,Screen.width, GetPropertyHeight(property, label)), bgColor);
		EditorGUI.BeginProperty (position, label, property);

		List<SerializedProperty> properties = new List<SerializedProperty> ();
		properties.Add (property.FindPropertyRelative ("paletteId"));
		properties.Add (property.FindPropertyRelative ("greyBlock"));
		properties.Add (property.FindPropertyRelative ("levelSelectSettings"));
		properties.Add (property.FindPropertyRelative ("worldSprite"));
		properties.Add (property.FindPropertyRelative ("backgroundColor"));
		properties.Add (property.FindPropertyRelative ("blockSprites"));


		int lineHeight = 18;
		for (int i = 0; i < properties.Count; i++) {
			var labelFieldRect = new Rect (0, position.y, position.width, lineHeight);
			EditorGUI.PropertyField (labelFieldRect, properties [i], new GUIContent(properties [i].displayName), true);
			position.y += EditorGUI.GetPropertyHeight(properties[i]) + 2;
			position.height += EditorGUI.GetPropertyHeight(properties[i]);
		}


		var listProperty = property.FindPropertyRelative ("blockSettings");
		var list = GetList (listProperty);
		list.elementHeight = 30;
		list.DoList (position);
		list.displayAdd = false;
		list.displayRemove = false;



		EditorGUI.EndProperty ();

	}
}