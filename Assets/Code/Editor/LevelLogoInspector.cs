﻿using UnityEngine;  
using UnityEditor;  
using UnityEditorInternal;

[CustomEditor(typeof(UIElementAnimator))]
public class LevelLogoInspector : Editor {  
	private ReorderableList list;

	private void OnEnable() {
		list = new ReorderableList(serializedObject, serializedObject.FindProperty("animatedItems"), true, true, true, true);
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();
		list.DoLayoutList();
		serializedObject.ApplyModifiedProperties();
		DrawDefaultInspector();
	}
}