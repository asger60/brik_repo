﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;


// IngredientDrawer
[CustomPropertyDrawer (typeof (BlockSettings))]
public class BlockSettingsDrawer : PropertyDrawer {



	// Draw the property inside the given rect
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {

		EditorGUI.BeginProperty (position, label, property);

		// Draw label
		position = EditorGUI.PrefixLabel (position, label);

		List<SerializedProperty> properties = new List<SerializedProperty>();
		properties.Add(property.FindPropertyRelative ("c1"));
		properties.Add(property.FindPropertyRelative ("c2"));
		properties.Add(property.FindPropertyRelative ("c3"));
		properties.Add(property.FindPropertyRelative ("t"));

		// Don't make child fields be indented
		var indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		int fieldSize = (int)position.width/4;

		for (int i = 0; i < properties.Count; i++) {
			
			var labelFieldRect = new Rect(position.x + (i * fieldSize), position.y, fieldSize, position.height);
			var colorFieldRect = new Rect(position.x + (i * fieldSize), position.y + 15, fieldSize, 15);
			EditorGUI.LabelField(labelFieldRect, properties[i].displayName);
			EditorGUI.PropertyField (colorFieldRect, properties[i], GUIContent.none);
		}


		// Set indent back to what it was
		EditorGUI.indentLevel = indent;

		EditorGUI.EndProperty ();
	}

	public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
		return 40;
	}
}