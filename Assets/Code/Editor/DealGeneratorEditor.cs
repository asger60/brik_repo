﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;



public class GeneratorRangedValue {
	public string name;
	public int minValue = 0, maxValue = 9;
	public float currentMin = 0, currentMax = 0;

	public GeneratorRangedValue(string _name, int min, int max) {
		name = _name;
		currentMin = min; 
		currentMax = max;
		minValue = min;
		maxValue = max;
	}

	public void Draw() {
		currentMin = Mathf.Clamp(EditorGUILayout.IntField("Min " + name, (int)currentMin), minValue, maxValue);
		currentMax = Mathf.Clamp(EditorGUILayout.IntField("Max " + name, (int)currentMax), minValue, maxValue);
		EditorGUILayout.MinMaxSlider(ref currentMin, ref currentMax, minValue, maxValue);
	}
}

class DealGeneratorWindow : EditorWindow {

	//GeneratorRangedValue blockTypes, decksPerType, blocksPerDeck, devilBlocks, stacks;
	bool init = false;
	int generateAmount = 1;
	string namePrefix = "newdeal";


	GeneratorRangedValue blockTypes = new GeneratorRangedValue("Blocks Types", 2, 4);
	GeneratorRangedValue decksPerType = new GeneratorRangedValue("Decks Per Type", 1, 4);
	GeneratorRangedValue blocksPerDeck = new GeneratorRangedValue("Blocks Per Deck", 1, 9);
	GeneratorRangedValue devilBlocks = new GeneratorRangedValue("Devil Blocks", 0, 6);
	GeneratorRangedValue stacks = new GeneratorRangedValue("Stacks", 4, 7);
	GeneratorRangedValue minimumStartBlocks = new GeneratorRangedValue("Minimum Start Blocks", 0, 5);
	//DealGeneratorSettings settings;

	[MenuItem ("ioio/Deal Generator")]
	public static void  ShowWindow () {
		EditorWindow.GetWindow(typeof(DealGeneratorWindow));
	}

	void Init() {
//		blockTypes = new GeneratorRangedValue("Blocks Types", 2, 4);
//		decksPerType = new GeneratorRangedValue("Decks Per Type", 1, 4);
//		blocksPerDeck = new GeneratorRangedValue("Blocks Per Deck", 1, 9);
//		devilBlocks = new GeneratorRangedValue("Devil Blocks", 0, 6);
//		stacks = new GeneratorRangedValue("Stacks", 4, 7);
		init = true;
	}

//	void OnEnable() {
//		Init();
//	}

	void OnGUI () {
		if(!init) {
			Init();	
		}
		EditorGUILayout.LabelField("Deal Generator");

		generateAmount = Mathf.Clamp(EditorGUILayout.IntField("Generate Amount " + name, (int)generateAmount), 1, 20);

		namePrefix = EditorGUILayout.TextField("Name Prefix", namePrefix);

		GUILayout.Space(10);

		blockTypes.Draw();
		decksPerType.Draw();
		blocksPerDeck.Draw();
		devilBlocks.Draw();
		stacks.Draw();
		minimumStartBlocks.Draw();

		GUILayout.Space(10);

		if(GUILayout.Button("Generate Deal(s)")) {
			GenerateDeals();
		}
	}

	void DrawRangedValue(GeneratorRangedValue rangedValue) {
		rangedValue.currentMin = Mathf.Clamp(EditorGUILayout.IntField("Min " + rangedValue.name, (int)rangedValue.currentMin), rangedValue.minValue, rangedValue.maxValue);
		rangedValue.currentMax = Mathf.Clamp(EditorGUILayout.IntField("Max " + rangedValue.name, (int)rangedValue.currentMax), rangedValue.minValue, rangedValue.maxValue);
		EditorGUILayout.MinMaxSlider(ref rangedValue.currentMin, ref rangedValue.currentMax, rangedValue.minValue, rangedValue.maxValue);
	}

	void GenerateDeals() {
		for (int i = 0; i < generateAmount; i++) {
			GenerateDeal();
		}
	}

	void GenerateDeal() {
		var newObj = ScriptableObject.CreateInstance<DealObject>();
		newObj.dealName = NameGenerator.GetName();
		//newObj.world = 0;
		newObj.id = System.Guid.NewGuid().ToString("N");
		newObj.randomSeed = Random.Range(0,1000);
		newObj.blockTypes = Random.Range((int)blockTypes.currentMin, (int)blockTypes.currentMax+1);
		newObj.decksPerType = Random.Range((int)decksPerType.currentMin, (int)decksPerType.currentMax+1);
		//newObj.version = 0;
		//int blocks = Random.Range((int)blocksPerDeck.currentMin, (int)blocksPerDeck.currentMax);
		newObj.minimumStartBlocksPerStack = 0;
		newObj.blocksPerDeck = Random.Range((int)blocksPerDeck.currentMin, (int)blocksPerDeck.currentMax+1);
		newObj.devilBlocks =  Random.Range((int)devilBlocks.currentMin, (int)devilBlocks.currentMax+1);
		newObj.stacks =  Random.Range((int)stacks.currentMin, (int)stacks.currentMax+1);
		newObj.minimumStartBlocksPerStack = Random.Range((int)minimumStartBlocks.currentMin, (int)minimumStartBlocks.currentMax+1);

		string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath("Assets/NewDeals/"+ namePrefix + "_" + newObj.dealName + ".asset" );
		AssetDatabase.CreateAsset(newObj, assetPathAndName );
		AssetDatabase.SaveAssets();
	}
}



public static class NameGenerator {
	static string[] names1   = {"Pamela",
		"Virginia",
		"Rose",
		"Gerald",
		"Anna",
		"Linda",
		"Justin",
		"Lillian",
		"Christine",
		"Rachel",
		"Ruth",
		"Martha",
		"Diane",
		"Marie",
		"Beverly",
		"Carol",
		"Mark",
		"Jesse",
		"Doris",
		"Russell",
		"Judith",
		"Thomas",
		"Russell",
		"Linda",
		"Frank",
		"Julia",
		"Robin",
		"Tammy",
		"Fred",
		"Frank",
		"Maria",
		"Jane",
		"Kathleen",
		"Mark",
		"Henry",
		"Ronald",
		"Jeffrey",
		"Dennis",
		"Joyce",
		"Margaret",
		"Elizabeth",
		"Donald",
		"Carolyn",
		"Katherine",
		"Michelle",
		"Teresa",
		"George",
		"Heather",
		"Julia",
		"Diana",
		"Mark",
		"Phillip",
		"Debra",
		"Brian",
		"Martin",
		"Christine",
		"Heather",
		"Patricia",
		"Timothy",
		"Debra",
		"Beverly",
		"Teresa",
		"Angela",
		"Willie",
		"Samuel",
		"Margaret",
		"Kenneth",
		"Ruby",
		"Patrick",
		"Douglas",
		"Joshua",
		"Anne",
		"Wanda",
		"Kenneth",
		"Albert",
		"Lisa",
		"Betty",
		"Peter",
		"Deborah",
		"Martin",
		"Debra",
		"Mark",
		"Justin",
		"Amanda",
		"Brenda",
		"Melissa",
		"Diane",
		"Todd",
		"Christine",
		"Martha",
		"Dennis",
		"Robin",
		"Brandon",
		"Lisa",
		"Paul",
		"Irene",
		"Howard",
		"Clarence",
		"Ann",
		"Ryan"};
	static string[] names2   = {
		"Yellow",
		"Fuscia",
		"Violet",
		"Orange",
		"Maroon",
		"Mauv",
		"Pink",
		"Maroon",
		"Yellow",
		"Blue",
		"Orange",
		"Puce",
		"Orange",
		"Purple",
		"Violet",
		"Fuscia",
		"Crimson",
		"Purple",
		"Pink",
		"Crimson",
		"Orange",
		"Puce",
		"Green",
		"Red",
		"Teal",
		"Yellow",
		"Aquamarine",
		"Green",
		"Goldenrod",
		"Turquoise",
		"Pink",
		"Violet",
		"Orange",
		"Blue",
		"Turquoise",
		"Crimson",
		"Maroon",
		"Goldenrod",
		"Teal",
		"Mauv",
		"Pink",
		"Maroon",
		"Pink",
		"Orange",
		"Maroon",
		"Fuscia",
		"Pink",
		"Yellow",
		"Violet",
		"Orange",
		"Red",
		"Blue",
		"Purple",
		"Fuscia",
		"Teal",
		"Indigo",
		"Pink",
		"Teal",
		"Green",
		"Puce",
		"Green",
		"Fuscia",
		"Goldenrod",
		"Crimson",
		"Red",
		"Turquoise",
		"Aquamarine",
		"Mauv",
		"Khaki",
		"Blue",};

	public static string[] GetNameList(int amount) {
		List<string> names = new List<string>();

		int t = 0;
		while (t < amount) {
			string newName = GetName();
			if(names.Contains(newName)) {
				newName = GetName();
			} else {
				names.Add(newName);
				t++;
			}

		}

		return names.ToArray();
	}

	public static string GetName() {
		string returnString = "";
		returnString = names1[Random.Range(0, names1.Length - 1)];
		returnString += names2[Random.Range(0, names2.Length - 1)];
		return returnString;
	}
}