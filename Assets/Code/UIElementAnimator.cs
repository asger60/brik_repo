﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class UIElementAnimator : MonoBehaviour {

	public string colorPaletteId; 
	public RectTransform[] animatedItems;
	public AudioObject[] animationAudioObjects;
	public Text numberText;
	Color textColor;
	public bool animate;
	public bool transparent;


	public void OnEnable() {
		if(!animate) return;
		foreach (var item in animatedItems) {
			item.localScale  = Vector2.zero;
		}
		if(textColor == Color.clear)
			textColor = numberText.color;
		numberText.color = Color.clear;
		StopAllCoroutines();
	}

	public void Init(GameWorldSettings settings) {
		this.gameObject.SetActive(true);
		int dealNumber = (DealHandler.Instance.GetDealNumInWorld(GameHandler.Instance.activeDeal)+1);
		if(dealNumber != 0)
			numberText.text = dealNumber.ToString();
		else
			numberText.text = "";
		
		if(transparent) {
			
			animatedItems[0].GetComponent<Image>().color = settings.backgroundColor;
			Color darkColor = settings.greyBlock.t - new Color(0.025f,0.025f,0.025f,0);

			animatedItems[1].GetComponent<Image>().color = new Color(darkColor.r,darkColor.g,darkColor.b, 1);
			animatedItems[2].GetComponent<Image>().color = new Color(darkColor.r,darkColor.g,darkColor.b, 0.6f);
			if(animatedItems.Length > 3)
				animatedItems[3].GetComponent<Image>().color = new Color(darkColor.r,darkColor.g,darkColor.b, 0.4f);

			numberText.color = new Color(darkColor.r,darkColor.g,darkColor.b, 0.4f);
		}


		if(!animate) return;

		float delay = 0.2f;
		for (int i = 0; i < animatedItems.Length; i++) {
			
		
			StartCoroutine(DoBuildUp(animatedItems[i], delay, i));
			delay += 0.3f;
		}
		StartCoroutine(DoTextFadeIn());
	}

	IEnumerator DoTextFadeIn() {
		yield return new WaitForSeconds(0.5f);
		float t=0, animTime = 0.75f;
		Color fadeFrom = new Color(textColor.r, textColor.g,textColor.b, 0);

		while(t<animTime) {
			numberText.color = Color.Lerp(fadeFrom, textColor, t/animTime);

			t+=Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		numberText.color = textColor;
	}

	IEnumerator DoBuildUp(RectTransform transform, float delay, int index) {
		

		numberText.color = Color.clear;
		float t = 0, animTime = 0.75f;

		yield return new WaitForSeconds(delay);

		if(animationAudioObjects[index] != null)
			animationAudioObjects[index].PostAudioEvent();
		
		while(t < animTime) {
			transform.localScale = Vector2.one * (float)Easing.QuadEaseOut(t, 0, 1, animTime);
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate();
		}
		transform.localScale = Vector2.one;

	}
}
