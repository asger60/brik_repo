﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using System.IO;

public class TransitionManager : MonoBehaviour
{

	public Image wipeScreenImage, dissolveImage;
	public UIScreenBase levelSelectScreen, gameIntroScreen, levelTitleScreen, levelWinScreen, inGame, inGameMenu, tutorialProgressScreen, gameLaunch;

	public UIScreenBase[] allScreens;
	UIScreenBase currentScreen;
	static TransitionManager _instance;
	public AudioObject onTransitionEvent;

	public static TransitionManager Instance {
		get {
			return _instance;
		}
	}

	void Awake ()
	{
		_instance = this;
	}

	void Start() {
		allScreens = Resources.FindObjectsOfTypeAll<UIScreenBase> ();
	}

	public void SetScreen (GameHandler.GameState nextState, Action callback)
	{
		var nextScreen = GetUIScreen(nextState);
		SetScreen(nextScreen, ()=>{
			callback();
		});
	}

	public void SetScreen (UIScreenBase nextScreen, Action callback)
	{
		//onTransitionEvent.PostAudioEvent();
		StopAllCoroutines();
		if(currentScreen != null) {
			currentScreen.inputEnabled = false;
			currentScreen.transitionSettings.OnScreenExit();
		}
		nextScreen.inputEnabled = false;

		if(nextScreen == null) {
			HideAllScreens ();
			if (callback != null)
				callback ();

			return;
		}
		var transitionType = nextScreen.transitionSettings.transitionType;
		if(currentScreen is UIInGameMenu && nextScreen is UIInGame)
			transitionType = TransitionSettings.TransitionType.dissolve;

		switch(transitionType) {
		case TransitionSettings.TransitionType.bullet:
			StartCoroutine (DoBulletWipeScreen (() => {
				HideAllScreens ();
				nextScreen.Show();
				StartCoroutine(DoFadeOutWipe());
				if (callback != null)
					callback ();
			}));
			break;
		case TransitionSettings.TransitionType.dissolve:
			StartCoroutine(DoDissolve(nextScreen, currentScreen, ()=>{
				HideAllBut(nextScreen);
				nextScreen.OnTransitionDone();
				if (callback != null)
					callback ();

				currentScreen = nextScreen;
			}));
			break;
		case TransitionSettings.TransitionType.dissolveAditive:
			StartCoroutine(DoDissolveAditive(nextScreen, currentScreen, ()=>{
				HideAllBut(nextScreen);
				nextScreen.OnTransitionDone();
				if (callback != null)
					callback ();

				currentScreen = nextScreen;
			}));
			break;
		case TransitionSettings.TransitionType.none:
			HideAllScreens ();
			nextScreen.Show();
			if (callback != null)
				callback ();

			currentScreen = nextScreen;
			break;
		}

	}

	UIScreenBase GetUIScreen(GameHandler.GameState state) {
		switch (state) {
		case GameHandler.GameState.Splash:
			return gameIntroScreen;
		case GameHandler.GameState.Menu:
			return gameIntroScreen;
		case GameHandler.GameState.LevelSelect:
			return levelSelectScreen;
		case GameHandler.GameState.InGame:
			return inGame;
		case GameHandler.GameState.LevelEnd:
			return levelWinScreen;
		case GameHandler.GameState.LevelTitle:
			return levelTitleScreen;
		case GameHandler.GameState.InGameMenu:
			return inGameMenu;
		case GameHandler.GameState.TutorialProgress:
			return tutorialProgressScreen;
		case GameHandler.GameState.GameLaunch:
			return gameLaunch;

		}
		return null;
	}

	void HideAllScreens ()
	{
		foreach (var item in allScreens) {
			item.Hide ();
		}
	}

	void HideAllBut(UIScreenBase notHide) {
		foreach (var item in allScreens) {
			if(item != notHide) item.Hide();
		}
	}

	IEnumerator DoBulletMask(UIScreenBase nextScreen, UIScreenBase currentScreen, Action callback) {
		yield return new WaitForEndOfFrame();
		int width = Screen.width;
		int height = Screen.height;

		Texture2D screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);
		screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		screenShot.Apply();
		yield return null;
		nextScreen.Show();
		if(currentScreen != null)
			currentScreen.Hide();
		var sprite = Sprite.Create(screenShot, new Rect(0, 0, width, height), dissolveImage.rectTransform.pivot); 
		dissolveImage.sprite = sprite;
		dissolveImage.color = Color.white;

		float t = 0, animTime = .75f;
		while (t < animTime) {
			dissolveImage.color = Color.Lerp(Color.white, new Color(1,1,1,0), t/animTime);
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}

		dissolveImage.color = Color.clear;
		if (callback != null)
			callback ();
	}



	IEnumerator DoDissolve(UIScreenBase nextScreen, UIScreenBase currentScreen, Action callback) {
		yield return new WaitForEndOfFrame();
		int width = Screen.width;
		int height = Screen.height;

		Texture2D screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);
		screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
		screenShot.Apply();
		yield return null;
		nextScreen.Show();
		if(currentScreen != null)
			currentScreen.Hide();
		var sprite = Sprite.Create(screenShot, new Rect(0, 0, width, height), dissolveImage.rectTransform.pivot); 
		dissolveImage.sprite = sprite;
		dissolveImage.color = Color.white;

		float t = 0, animTime = .5f;
		while (t < animTime) {
			dissolveImage.color = Color.Lerp(Color.white, new Color(1,1,1,0), t/animTime);
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}

		dissolveImage.color = Color.clear;
		if (callback != null)
			callback ();
	}

	IEnumerator DoDissolveAditive(UIScreenBase nextScreen, UIScreenBase _currentScreen, Action callback) {
		dissolveImage.sprite = null;

		Color colorFrom = nextScreen.backgroundColor;
		if(_currentScreen != null)
			colorFrom = _currentScreen.backgroundColor; 

		Color colorTo = nextScreen.backgroundColor;
		float t = 0, animTime = .75f;
		while (t < animTime) {
			dissolveImage.color = Color.Lerp(new Color(colorFrom.r,colorFrom.g,colorFrom.b,0), colorTo, t/animTime);
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}
		nextScreen.Show();
		if(_currentScreen != null)
			_currentScreen.Hide();
		yield return new WaitForEndOfFrame();
		t = 0; 
		animTime = 1f;
		while (t < animTime) {
			dissolveImage.color = Color.Lerp(colorTo, new Color(colorTo.r,colorTo.g,colorTo.b,0), t/animTime);
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}


		dissolveImage.color = Color.clear;
		if (callback != null)
			callback ();
	}


	IEnumerator DoBulletWipeScreen (Action callback)
	{
		float t = 0, animTime = .5f;
		Color wipeColor = GameHandler.Instance.GetGameWorldSettings().backgroundColor;
		wipeScreenImage.color = wipeColor;
		float scaleTo = ScreenManager.Instance.scale;
		while (t < animTime) {
			wipeScreenImage.transform.localScale = Vector3.Lerp (Vector3.one*0.01f, Vector3.one * scaleTo, (float)Easing.SineEaseIn(t, 0,1 ,animTime));
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}

		if (callback != null)
			callback ();
	}

	IEnumerator DoFadeOutWipe() {
		float t = 0, fadeTime = 1.5f;
		Color wipeColor = GameHandler.Instance.GetGameWorldSettings().backgroundColor;
		while (t < fadeTime) {
			wipeScreenImage.color = Color.Lerp(wipeColor, new Color(wipeColor.r, wipeColor.g, wipeColor.b, 0f), (float)Easing.QuartEaseIn(t,0,1,fadeTime));
			t += Time.fixedDeltaTime;
			yield return new WaitForFixedUpdate ();
		}

		wipeScreenImage.color = Color.clear;
	}

}
