﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * 2015/08/30 - First version. 
 * **********************************************************/

using System.Reflection;
using UnityEngine;

namespace Assets.ReorderableList.Editor.Helpers
{
    public class ReflectedGUIUtility : ReflectedBase<GUIUtility>
    {
        public static int GetPermanentControlID()
        {
            return Helper.Method<int>(null, MethodBase.GetCurrentMethod());
        }
    }
}