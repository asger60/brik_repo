﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * 2015/08/30 - First version. 
 * **********************************************************/

using System;
using System.Reflection;
using UnityEngine;

namespace Assets.ReorderableList.Editor.Helpers
{
    public class ReflectedGUISlideGroup
    {
        private readonly object _instance;

        public ReflectedGUISlideGroup()
        {
            _instance = Activator.CreateInstance(Helper.ReflectedType, true);
        }

        public Rect GetRect(int id, Rect rect)
        {
            return Helper.Method<Rect>(_instance, MethodBase.GetCurrentMethod(), id, rect);
        }

        public void Reset()
        {
            Helper.Method(_instance, MethodBase.GetCurrentMethod());
        }

        protected static readonly ReflectionHelper Helper = new ReflectionHelper(Type<UnityEditor.Editor>.GetTypeInAssembly("UnityEditor.GUISlideGroup"));
    }
}