﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * 2015/08/30 - First version. 
 * **********************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Assets.ReorderableList.Editor.Helpers
{
    public class ReflectionHelper
    {
        public readonly Type ReflectedType;

        public ReflectionHelper(Type reflectedType)
        {
            if(reflectedType == null) { throw new ArgumentNullException("reflectedType"); }
            ReflectedType = reflectedType;
        }

        public bool Validate()
        {
            var valid = true;

            var properties = ReflectedType.GetProperties(InstanceBinding).ToList();
            properties.AddRange(ReflectedType.GetProperties(StaticBinding));
            foreach(var property in properties)
            {
                try
                {
                    if(GetMemberHelper(property.Name) == null)
                    {
                        valid = false;
                    }
                }
                catch(Exception ex)
                {
                    Debug.LogException(ex);
                    valid = false;
                }
            }

            var propertyAccessors = properties.SelectMany(p => p.GetAccessors()).ToDictionary(a => a);
            var methods = ReflectedType.GetMethods(InstanceBinding).ToList();
            methods.AddRange(ReflectedType.GetMethods(StaticBinding));
            methods.RemoveAll(propertyAccessors.ContainsKey);
            foreach(var method in methods)
            {
                try
                {
                    if(GetMethodInfo(method) == null)
                    {
                        valid = false;
                    }
                }
                catch(Exception ex)
                {
                    Debug.LogException(ex);
                    valid = false;
                }
            }

            if(valid)
            {
                Debug.Log(string.Format("{0} is valid.", ReflectedType.Name));
            }
            else
            {
                Debug.LogWarning(string.Format("{0} is invalid!", ReflectedType.Name));
            }
            return valid;
        }

        public T Get<T>(object instance, MethodBase methodBase)
        {
            return GetMemberHelper(methodBase.Name.Substring(4, methodBase.Name.Length - 4)).Get<T>(instance);
        }

        public void Set<T>(object instance, MethodBase methodBase, T value)
        {
            GetMemberHelper(methodBase.Name.Substring(4, methodBase.Name.Length - 4)).Set(instance, value);
        }

        public T Method<T>(object instance, MethodBase methodBase, params object[] parameters)
        {
            return (T)GetMethodInfo((MethodInfo)methodBase).Invoke(instance, parameters);
        }

        public void Method(object instance, MethodBase methodBase, params object[] parameters)
        {
            GetMethodInfo((MethodInfo)methodBase).Invoke(instance, parameters);
        }

        private readonly Dictionary<MethodInfo, MethodInfo> _methods = new Dictionary<MethodInfo, MethodInfo>();
        private readonly Dictionary<string, MemberHelper> _members = new Dictionary<string, MemberHelper>();
        private const BindingFlags InstanceBinding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
        private const BindingFlags StaticBinding = BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic;

        private MethodInfo GetMethodInfo(MethodInfo expected)
        {
            MethodInfo match;
            if(!_methods.TryGetValue(expected, out match))
            {
                _methods.Add(expected, match = FindMethod(expected));
            }

            return match;
        }

        private MemberHelper GetMemberHelper(string memberName)
        {
            MemberHelper match;
            if(!_members.TryGetValue(memberName, out match))
            {
                _members.Add(memberName, match = FindMember(memberName));
            }
            return match;
        }

        private MethodInfo FindMethod(MethodBase expected)
        {
            var candidateMethods = ReflectedType.GetMethods(InstanceBinding);
            var methodMatch = candidateMethods.FirstOrDefault(m => MatchMethods(expected, m));
            if(methodMatch != null)
            {
                return methodMatch;
            }

            candidateMethods = ReflectedType.GetMethods(StaticBinding);
            methodMatch = candidateMethods.FirstOrDefault(m => MatchMethods(expected, m));
            if(methodMatch != null)
            {
                return methodMatch;
            }

            throw new Exception(string.Format("Could not find {0}.{1} method.", ReflectedType.Name, expected.Name));
        }

        private MemberHelper FindMember(string name)
        {
            var member =
                ReflectedType.GetProperties(InstanceBinding).OfType<MemberInfo>().FirstOrDefault(m => m.Name == name) ??
                ReflectedType.GetProperties(StaticBinding).OfType<MemberInfo>().FirstOrDefault(m => m.Name == name) ??
                ReflectedType.GetFields(InstanceBinding).OfType<MemberInfo>().FirstOrDefault(m => m.Name == name) ??
                ReflectedType.GetFields(StaticBinding).OfType<MemberInfo>().FirstOrDefault(m => m.Name == name);
            if(member == null)
            {
                throw new Exception(string.Format("Could not find {0}.{1} member.", ReflectedType.Name, name));
            }

            return new MemberHelper(member);
        }

        private static bool MatchMethods(MethodBase expected, MethodBase candidate)
        {
            if(expected.Name != candidate.Name)
            {
                return false;
            }

            var expectedParameters = expected.GetParameters();
            var candidateParameters = candidate.GetParameters();
            if(expectedParameters.Length != candidateParameters.Length)
            {
                return false;
            }

            return !expectedParameters.Where((t, i) => t.ParameterType != candidateParameters[i].ParameterType).Any();
        }

        private class MemberHelper
        {
            public T Get<T>(object instance)
            {
                switch(_memberType)
                {
                    case MemberTypes.Field: return (T)_fieldInfo.GetValue(instance);
                    case MemberTypes.Property: return (T)_propertyGet.Invoke(instance, null);
                    default: throw new NotSupportedException(string.Format("Invalid member type '{0}'.", _memberType));
                }
            }

            public void Set<T>(object instance, T value)
            {
                switch(_memberType)
                {
                    case MemberTypes.Field: _fieldInfo.SetValue(instance, value); break;
                    case MemberTypes.Property: _propertySet.Invoke(instance, new object[] { value }); break;
                    default: throw new NotSupportedException(string.Format("Invalid member type '{0}'.", _memberType));
                }
            }

            public MemberHelper(MemberInfo memberInfo)
            {
                _memberType = memberInfo.MemberType;

                switch(_memberType)
                {
                    case MemberTypes.Field: _fieldInfo = (FieldInfo)memberInfo; break;
                    case MemberTypes.Property:
                        var propertyInfo = ((PropertyInfo)memberInfo);
                        _propertyGet = propertyInfo.GetGetMethod() ?? propertyInfo.GetGetMethod(true);
                        _propertySet = propertyInfo.GetSetMethod() ?? propertyInfo.GetSetMethod(true);
                        break;
                    default: throw new NotSupportedException(string.Format("Invalid member type '{0}'.", _memberType));
                }
            }

            private readonly MemberTypes _memberType;
            private readonly FieldInfo _fieldInfo;
            private readonly MethodInfo _propertyGet, _propertySet;
        }
    }

    public abstract class ReflectedBase<TOriginal>
    {
        protected static readonly ReflectionHelper Helper = new ReflectionHelper(typeof(TOriginal));
    }
}