﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * 2015/08/30 - First version. 
 * **********************************************************/

using UnityEditor;

namespace Assets.ReorderableList.Editor.Helpers
{
    public static class SerializedPropertyExtensions
    {
        public static int PropertyHashCode(this SerializedProperty property)
        {
            if(property == null || property.serializedObject == null)
            {
                return 0;
            }

            var hash = property.serializedObject.targetObject.GetInstanceID() ^ property.propertyPath.GetHashCode();
            if(property.propertyType == SerializedPropertyType.ObjectReference)
            {
                hash ^= property.objectReferenceInstanceIDValue;
            }

            return hash;
        }
    }
}