﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * 2015/08/30 - First version. 
 * **********************************************************/

using System;

namespace Assets.ReorderableList.Editor.Helpers
{
    public class Type<T>
    {
        public static readonly Type Value = typeof(T);

        public static Type GetTypeInAssembly(string typeName)
        {
            return Value.Assembly.GetType(typeName);
        }
    }
}