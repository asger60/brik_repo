﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * 2015/08/30 - First version. 
 * **********************************************************/

using System.Reflection;
using UnityEditor;

namespace Assets.ReorderableList.Editor.Helpers
{
    public class ReflectedEditorGUI : ReflectedBase<EditorGUI>
    {
        public static void EndEditingActiveTextField()
        {
            Helper.Method(null, MethodBase.GetCurrentMethod());
        }

        public static void DoPropertyContextMenu(SerializedProperty property)
        {
            Helper.Method(null, MethodBase.GetCurrentMethod(), property);
        }
    }
}