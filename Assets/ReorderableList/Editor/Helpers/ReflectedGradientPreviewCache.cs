﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * 2015/08/30 - First version. 
 * **********************************************************/

using System.Reflection;

namespace Assets.ReorderableList.Editor.Helpers
{
    public class ReflectedGradientPreviewCache
    {
        public static void ClearCache()
        {
            Helper.Method(null, MethodBase.GetCurrentMethod());
        }

        public static ReflectionHelper Helper = new ReflectionHelper(Type<UnityEditorInternal.ReorderableList>.GetTypeInAssembly("UnityEditorInternal.GradientPreviewCache"));
    }
}