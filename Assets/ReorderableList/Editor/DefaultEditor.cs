﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * 2015/08/30 - First version. 
 * **********************************************************/

using UnityEditor;
using UnityEngine;

namespace Assets.ReorderableList.Editor
{
    [CustomEditor(typeof(Object), true, isFallback = true)]
    public class DefaultEditor : UnityEditor.Editor { }
}