﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * Huge credit goes to CDF for his reorderable list implementation:
 * http://forum.unity3d.com/threads/reorderable-list-v2.339717/
 * 
 * 2015/08/30 - First version.
 * 2015/09/05 - Added multiple selection functionality.
 *            - Added expand all toggle functionality.
 *            - Refactored drawer into ScriptableObject for better undo/redo support.
 *            - Simplified indent calculation.
 *            - Fixed delete selection functionality.
 *            - Modified GetPropertyHeight to atttempt to prevent GUI flicker.
 * 2016/10/21 - Fixed error getting control ID in scriptable object field initialization.
 * 2016/10/27 - Modified element labels to use type name instead of generic "Element x".
 *            - Added context menu support for nested list elements.
 * **********************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.ReorderableList.Editor.Helpers;
using Assets.ReorderableList.Helpers;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.ReorderableList.Editor
{
    [CustomPropertyDrawer(typeof(ReorderableList), true)]
    public class ReorderableListPropertyDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            property = property.FindPropertyRelative("_list");
            return GetDrawer(property).GetListHeight(property);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            ClearCache();
            property = property.FindPropertyRelative("_list");
            GetDrawer(property).GUI(position, property, new GUIContent(label));
        }

        #region Private Parts

        private static bool _clearCache;
        private static readonly Dictionary<int, ReoderableListDrawer> ListDrawers = new Dictionary<int, ReoderableListDrawer>();

        private static ReoderableListDrawer GetDrawer(SerializedProperty property)
        {
            var hashCode = property.PropertyHashCode();
            ReoderableListDrawer drawer;
            if(!ListDrawers.TryGetValue(hashCode, out drawer))
            {
                drawer = ScriptableObject.CreateInstance<ReoderableListDrawer>();
                drawer.hideFlags = HideFlags.HideAndDontSave;
                ListDrawers.Add(hashCode, drawer);
            }
            return drawer;
        }

        private static void ClearCache()
        {
            if(_clearCache)
            {
                foreach(var key in ListDrawers.Keys.ToList())
                {
                    var drawer = ListDrawers[key];
                    if(drawer == null || !drawer.IsValid)
                    {
                        ListDrawers.Remove(key);
                        if(drawer != null)
                        {
                            Object.DestroyImmediate(drawer);
                        }
                    }
                }
            }
            _clearCache = false;
        }

        ~ReorderableListPropertyDrawer()
        {
            _clearCache = true;
        }

        #endregion
    }

    public class ReoderableListDrawer : ScriptableObject
    {
        public bool IsValid
        {
            get
            {
                try
                {
                    return _serializedProperty != null && _serializedProperty.propertyPath != null;
                }
                catch(NullReferenceException)
                {
                    return false;
                }
            }
        }

        public void GUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if(property == null) { throw new ArgumentNullException("property"); }

            _serializedProperty = property;
            _expanded.Apply(_serializedProperty);

            position = EditorGUI.IndentedRect(position);
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            var headerRect = new Rect(position.x, position.y, position.width, HeaderHeight);
            var listRect = new Rect(position.x, headerRect.y + headerRect.height, position.width, GetListElementsHeight(_serializedProperty));
            var footerRect = new Rect(position.x, listRect.y + listRect.height, position.width, FooterHeight);

            DoListHeader(headerRect, label);
            DoListElements(listRect);
            DoListFooter(footerRect);

            EditorGUI.indentLevel = indent;
        }

        public float GetListHeight(SerializedProperty property)
        {
            if(property == null)
            {
                return 0f;
            }

            _expanded.Apply(property);
            return HeaderHeight + GetListElementsHeight(property) + (property.isExpanded ? FooterHeight : 0f);
        }

        #region Private Parts

        private int _id;
        private readonly ReflectedGUISlideGroup _slideGroup = new ReflectedGUISlideGroup();
        private readonly List<int> _newOrder = new List<int>();

        [SerializeField]
        private Selection _selection = new Selection();
        [SerializeField]
        private ExpandedArray _expanded = new ExpandedArray();
        private bool _dragging;
        private SerializedProperty _serializedProperty;

        private int Count
        {
            get
            {
                if(!IsValid)
                {
                    return 0;
                }

                return _serializedProperty.hasMultipleDifferentValues
                            ? _serializedProperty.serializedObject.targetObjects.Min(o => new SerializedObject(o).FindProperty(_serializedProperty.propertyPath).arraySize)
                            : _serializedProperty.arraySize;
            }
        }

        private bool Draggable { get { return IsValid && _serializedProperty.editable; } }

        private ReoderableListDrawer() { }

        private void OnEnable()
        {
            _id = ReflectedGUIUtility.GetPermanentControlID();
        }

        private void DoListHeader(Rect headerRect, GUIContent header)
        {
            if(Event.current.type == EventType.Repaint)
            {
                Styles.HeaderBackground.Draw(headerRect, false, false, false, false);
            }

            headerRect.xMin += BorderWidth;
            headerRect.xMax -= BorderWidth;
            headerRect.height -= 2f;
            headerRect.y += 1f;

            if(Event.current.type == EventType.ContextClick && headerRect.Contains(Event.current.mousePosition))
            {
                var containerProperty = _serializedProperty.serializedObject.FindProperty(_serializedProperty.propertyPath.Substring(0, _serializedProperty.propertyPath.Length - @"._list".Length));
                ReflectedEditorGUI.DoPropertyContextMenu(containerProperty);
            }

            EditorGUI.BeginProperty(headerRect, header, _serializedProperty);

            headerRect.xMin += 10f;
            _serializedProperty.isExpanded = EditorGUI.Foldout(headerRect, _serializedProperty.isExpanded, header, true);

            EditorGUI.EndProperty();
        }

        private void DoListElements(Rect listRect)
        {
            if(Event.current.type == EventType.Repaint)
            {
                Styles.BoxBackground.Draw(listRect, false, false, false, false);
            }

            if(!_serializedProperty.isExpanded)
            {
                return;
            }

            var count = Count;
            listRect.yMin += 2f;
            listRect.yMax -= 5f;
            if((_serializedProperty != null && _serializedProperty.isArray) && count > 0)
            {
                if(_dragging)
                {
                    DoElementsDragging(count, listRect);
                }
                else
                {
                    DoElementsNoDragging(count, listRect);
                }
                
                ProcessEvent(listRect);
            }
            else
            {
                DoNoElements(listRect);
            }
        }

        private void DoListFooter(Rect footerRect)
        {
            if(!_serializedProperty.isExpanded)
            {
                return;
            }

            var xMax = footerRect.xMax;
            var left = xMax - 58f;
            footerRect = new Rect(left, footerRect.y, xMax - left, footerRect.height);
            if(Event.current.type == EventType.Repaint)
            {
                Styles.FooterBackground.Draw(footerRect, false, false, false, false);
            }

            if(UnityEngine.GUI.Button(new Rect(left + 4f, footerRect.y - 3f, 25f, 13f), Styles.IconToolbarPlus, Styles.PreButton))
            {
                _expanded.Record(_serializedProperty);
                RecordUndo();

                _expanded.Add();
                _selection.Select(_serializedProperty.arraySize++, SelectAction.Single);
                        
                ClearCache();
                GUIUtility.keyboardControl = _id;
            }

            EditorGUI.BeginDisabledGroup(!_selection.Any());
            if(UnityEngine.GUI.Button(new Rect(xMax - 29f, footerRect.y - 3f, 25f, 13f), Styles.IconToolbarMinus, Styles.PreButton))
            {
                DeleteSelected();
                GUIUtility.keyboardControl = _id;
            }
            EditorGUI.EndDisabledGroup();
        }

        private void DoElementsDragging(int count, Rect listRect)
        {
            _newOrder.Clear();
            var cursorY = Event.current.mousePosition.y;
            var squishTop = cursorY < listRect.yMin;
            var squishBottom = cursorY > listRect.yMax;
            var lastRow = _selection.Latest;

            var deltaRow = GetRow(cursorY - listRect.y) - lastRow;
            var topIndex = _selection.TopRow;
            if(topIndex + deltaRow < 0)
            {
                deltaRow = -topIndex;
            }
            var bottomIndex = _selection.BottomRow;
            if(bottomIndex + deltaRow >= count)
            {
                deltaRow = (count - 1) - bottomIndex;
            }

            var selected = new HashSet<int>();
            var tempRows = new int?[count].ToList();
            foreach(var row in _selection)
            {
                selected.Add(row);
                tempRows[row + deltaRow] = row;
            }

            var unselected = new List<int?>();
            for(var i = 0; i < count; ++i)
            {
                if(!_selection.Contains(i))
                {
                    unselected.Add(i);
                }
            }

            if(squishTop)
            {
                tempRows.AddRange(unselected);
            }
            else if(squishBottom)
            {
                tempRows.InsertRange(0, unselected);
            }
            else
            {
                foreach(var row in unselected)
                {
                    var inserted = false;
                    for(var i = 0; i < tempRows.Count && !inserted; ++i)
                    {
                        if(tempRows[i] == null)
                        {
                            tempRows[i] = row;
                            inserted = true;
                        }
                    }
                }
            }

            var selectedRows = new List<RowRect>();
            var unselectedRows = new List<RowRect>();

            var rect = listRect;
            rect.height = 0f;
            RowRect lastRowRect = null;
            foreach(var temp in tempRows.Where(temp => temp != null))
            {
                if(deltaRow != 0 || squishTop || squishBottom)
                {
                    _newOrder.Add(temp.Value);
                }

                rect.y += rect.height;
                rect.height = GetRowHeight(temp.Value);
                var rowRect = new RowRect
                    {
                        Index = temp.Value,
                        Rect = rect
                    };
                if(lastRow == temp.Value)
                {
                    lastRowRect = rowRect;
                }
                else if(selected.Contains(temp.Value))
                {
                    selectedRows.Add(rowRect);
                }
                else
                {
                    unselectedRows.Add(rowRect);
                }
            }

            if(lastRowRect != null)
            {
                selectedRows.Add(lastRowRect);
            }

            foreach(var row in unselectedRows)
            {
                DrawElement(_slideGroup.GetRect(row.Index, row.Rect), row.Index, false, false);
            }

            foreach(var row in selectedRows)
            {
                DrawElement(_slideGroup.GetRect(row.Index, row.Rect), row.Index, true, true);
            }

            EditorUtility.SetDirty(_serializedProperty.serializedObject.targetObject);
        }

        private void DoElementsNoDragging(int count, Rect listRect)
        {
            var rowRect = listRect;
            for(var i = 0; i < count; ++i)
            {
                var activeElement = _selection.Contains(i);
                var keyboardControl = activeElement && GUIUtility.keyboardControl == _id;

                rowRect.y = listRect.y + GetRowY(i);
                rowRect.height = GetRowHeight(i);
                DrawElement(rowRect, i, activeElement, keyboardControl);
            }
        }

        private void DrawElement(Rect rect, int row, bool active, bool focused)
        {
            if(Event.current.type == EventType.Repaint)
            {
                var backgroundRect = rect;
                if(active)
                {
                    backgroundRect.yMin -= 1;
                    backgroundRect.yMax += 1;
                }
                Styles.ElementBackground.Draw(backgroundRect, false, active, active, focused);
            }

            if(row >= 0)
            {
                if(Event.current.type == EventType.Repaint && Draggable)
                {
                    Styles.DraggingHandle.Draw(new Rect(rect.x + 5f, rect.y + 7f, 10f, 7f), false, false, false, false);
                }

                rect.xMin += ElementIndent;
                rect.xMax -= BorderWidth;
                
                var element = _serializedProperty.GetArrayElementAtIndex(row);
                var isExpanded = element.isExpanded;
                if(element.propertyType == SerializedPropertyType.Generic)
                {
                    rect.xMin += 10f;
                    EditorGUI.PropertyField(rect, element, new GUIContent(string.Format("{0} {1}", _serializedProperty.type, row)), true);
                }
                else
                {
                    EditorGUI.PropertyField(rect, element, true);
                }

                if(Event.current.shift && isExpanded != element.isExpanded)
                {
                    for(var i = 0; i < _serializedProperty.arraySize; ++i)
                    {
                        _serializedProperty.GetArrayElementAtIndex(i).isExpanded = !isExpanded;
                    }
                }
            }
        }

        private void DoNoElements(Rect listRect)
        {
            listRect.height = GetRowHeight(-1);
            DrawElement(listRect, -1, false, false);
            listRect.xMin += BorderWidth;
            listRect.xMax -= BorderWidth;
            EditorGUI.LabelField(listRect, "List is Empty");
        }

        private void ProcessEvent(Rect listRect)
        {
            var current = Event.current;

            switch(current.GetTypeForControl(_id))
            {
                case EventType.MouseDown:
                    if(listRect.Contains(current.mousePosition) && current.button == 0)
                    {
                        current.Use();
                        ReflectedEditorGUI.EndEditingActiveTextField();
                        _selection.Select(GetRow(current.mousePosition.y - listRect.y), GetAction(current), true);
                        if(Draggable)
                        {
                            GUIUtility.hotControl = _id;
                        }
                        GUIUtility.keyboardControl = _id;
                    }
                    break;

                case EventType.MouseUp:
                    if(GUIUtility.hotControl == _id)
                    {
                        if(_dragging)
                        {
                            _dragging = false;
                            current.Use();

                            var modified = false;
                            for(var dest = 0; dest < _newOrder.Count; ++dest)
                            {
                                var source = _newOrder[dest];
                                if(source != dest)
                                {
                                    if(!modified)
                                    {
                                        _expanded.Record(_serializedProperty);
                                        RecordUndo();
                                        modified = true;
                                    }

                                    _serializedProperty.MoveArrayElement(source, dest);
                                    _expanded.Move(source, dest);
                                    _selection.Move(source, dest);

                                    for(var i = dest + 1; i < _newOrder.Count; ++i)
                                    {
                                        if(_newOrder[i] >= dest && _newOrder[i] < source)
                                        {
                                            _newOrder[i] += 1;
                                        }
                                    }
                                }
                            }

                            if(modified)
                            {
                                ClearCache();
                            }
                        }
                        else if(!current.shift && !EditorGUI.actionKey)
                        {
                            _selection.Select(GetRow(current.mousePosition.y - listRect.y), SelectAction.Single);
                        }

                        GUIUtility.hotControl = 0;
                    }
                    break;

                case EventType.MouseDrag:
                    if(Draggable && GUIUtility.hotControl == _id)
                    {
                        if(!_dragging)
                        {
                            _dragging = true;
                            _slideGroup.Reset();
                        }
                        current.Use();
                    }
                    break;

                case EventType.KeyDown:
                    if(GUIUtility.keyboardControl == _id)
                    {
                        var action = current.shift ? SelectAction.Range : SelectAction.Single;
                        switch(current.keyCode)
                        {
                            case KeyCode.UpArrow:
                                current.Use();
                                var top = _selection.TopRow;
                                if(action == SelectAction.Single)
                                {
                                    _selection.Clear();
                                }
                                _selection.Select(Mathf.Max(top > -1 ? top - 1 : 0, 0), action);
                                break;

                            case KeyCode.DownArrow:
                                current.Use();
                                var bottom = _selection.BottomRow;
                                if(action == SelectAction.Single)
                                {
                                    _selection.Clear();
                                }
                                _selection.Select(Mathf.Min(bottom > -1 ? bottom + 1 : Count - 1, Count - 1), action);
                                break;

                            case KeyCode.Escape:
                                current.Use();
                                if(GUIUtility.hotControl == _id)
                                {
                                    GUIUtility.hotControl = 0;
                                }
                                if(!_dragging)
                                {
                                    _selection.Clear();
                                }
                                _dragging = false;
                                break;

                            case KeyCode.Delete:
                                current.Use();
                                if(GUIUtility.hotControl == _id)
                                {
                                    GUIUtility.hotControl = 0;
                                }
                                DeleteSelected();
                                _dragging = false;
                                break;
                        }
                    }
                    break;
            }
        }

        private void DeleteSelected()
        {
            _expanded.Record(_serializedProperty);
            RecordUndo();

            foreach(var row in _selection.OrderByDescending(r => r))
            {
                _expanded.RemoveAt(row);
                _serializedProperty.DeleteArrayElementAtIndex(row);
            }
            _selection.Clear();

            ClearCache();
        }

        private float GetRowHeight(int row)
        {
            var count = Count;
            if(row >= 0 && row < count)
            {
                return GetElementHeight(_serializedProperty.GetArrayElementAtIndex(row));
            }
            return DefaultElementHeight;
        }

        private float GetRowY(int row, IList<int> orderOverride = null)
        {
            var height = 0.0f;

            if(orderOverride == null)
            {
                var count = Count;
                for(var r = 0; r < count; ++r)
                {
                    if(r == row)
                    {
                        return height;
                    }
                    height += GetElementHeight(_serializedProperty.GetArrayElementAtIndex(r));
                }
            }
            else if(orderOverride.Any())
            {
                foreach(var r in orderOverride)
                {
                    if(r == row)
                    {
                        return height;
                    }
                    height += GetElementHeight(_serializedProperty.GetArrayElementAtIndex(r));
                }
            }

            return height;
        }

        private int GetRow(float localY)
        {
            var count = Count;

            int heightRow;
            var height = 0.0f;
            for(heightRow = 0; heightRow < count; ++heightRow)
            {
                height += GetElementHeight(_serializedProperty.GetArrayElementAtIndex(heightRow));
                if(localY < height)
                {
                    break;
                }
            }

            return Mathf.Clamp(heightRow, 0, count - 1);
        }

        private void RecordUndo()
        {
            Undo.RecordObject(this, Undo.GetCurrentGroupName());
        }

        #endregion

        #region Static

        private const float BorderWidth = 6f;
        private const float ElementIndent = 20f;
        private const float HeaderHeight = 18f;
        private const float FooterHeight = 13f;
        private const float DefaultElementHeight = 21f;

        private static float GetListElementsHeight(SerializedProperty property)
        {
            if(!property.isExpanded)
            {
                return 5f;
            }

            var count = property.arraySize;
            if(count > 0)
            {
                var height = 7f;
                for(var i = 0; i < count; ++i)
                {
                    height += GetElementHeight(property.GetArrayElementAtIndex(i));
                }
                return height;
            }

            if(count == 0)
            {
                count = 1;
            }
            return (DefaultElementHeight * count) + 7.0f;
        }

        private static float GetElementHeight(SerializedProperty element)
        {
            return element == null ? DefaultElementHeight : EditorGUI.GetPropertyHeight(element) + 5.0f;
        }

        private static void ClearCache()
        {
            ReflectedAnimationCurvePreviewCache.ClearCache();
            ReflectedGradientPreviewCache.ClearCache();
        }

        private static SelectAction GetAction(Event current)
        {
            if(current != null)
            {
                if(EditorGUI.actionKey)
                {
                    return SelectAction.Toggle;
                }
                if(current.shift)
                {
                    return SelectAction.Range;
                }
            }

            return SelectAction.Single;
        }

        private static class Styles
        {
            public static readonly GUIContent IconToolbarPlus = EditorGUIUtility.IconContent("Toolbar Plus", "Add to list");
            public static readonly GUIContent IconToolbarMinus = EditorGUIUtility.IconContent("Toolbar Minus", "Remove selection from list");
            public static readonly GUIStyle BoxBackground = "RL Background";
            public static readonly GUIStyle DraggingHandle = "RL DragHandle";
            public static readonly GUIStyle HeaderBackground = "RL Header";
            public static readonly GUIStyle FooterBackground = "RL Footer";
            public static readonly GUIStyle PreButton = "RL FooterButton";
            public static readonly GUIStyle ElementBackground = "RL Element";
        }

        #endregion

        #region Definitions

        private enum SelectAction
        {
            Single,
            Toggle,
            Range
        }

        private class RowRect
        {
            public int Index;
            public Rect Rect;
        }

        [Serializable]
        private class Selection : IEnumerable<int>
        {
            public int Latest { get { return _rows.Any() ? _rows.Last() : -1; } }

            public int TopRow
            {
                get
                {
                    int? row = null;
                    foreach(var r in _rows.Where(r => row == null || r < row.Value))
                    {
                        row = r;
                    }
                    return row != null ? row.Value : -1;
                }
            }

            public int BottomRow
            {
                get
                {
                    int? row = null;
                    foreach(var r in _rows.Where(r => row == null || r > row.Value))
                    {
                        row = r;
                    }
                    return row != null ? row.Value : -1;
                }
            }

            public void Select(int index, SelectAction action, bool bumpExistingSingle = false)
            {
                switch(action)
                {
                    case SelectAction.Single:
                        if(bumpExistingSingle && _rows.Contains(index))
                        {
                            _rows.RemoveAll(r => r == index);
                        }
                        else
                        {
                            _rows.Clear();
                        }
                        _rows.Add(index);
                        break;

                    case SelectAction.Toggle:
                        if(_rows.Contains(index))
                        {
                            _rows.Remove(index);
                        }
                        else
                        {
                            _rows.Add(index);
                        }
                        break;

                    case SelectAction.Range:
                        var direction = Mathf.Clamp(index - Math.Max(0, Latest), -1, 1);
                        if(direction != 0)
                        {
                            for(var i = Math.Max(0, Latest) + direction; i != index + direction; i += direction)
                            {
                                if(!_rows.Contains(i))
                                {
                                    _rows.Add(i);
                                }
                            }
                        }
                        break;
                }
            }

            public void Move(int source, int dest)
            {
                var direction = Mathf.Clamp(dest - source, -1, 1);
                if(direction != 0)
                {
                    for(var i = 0; i < _rows.Count; ++i)
                    {
                        if(_rows[i] == source)
                        {
                            _rows[i] = dest;
                        }
                        else if(_rows[i] == dest ||
                                (direction < 0 && _rows[i] > dest && _rows[i] < source) ||
                                (direction > 0 && _rows[i] > source && _rows[i] < dest))
                        {
                            _rows[i] -= direction;
                        }
                    }
                }
            }

            public void Clear()
            {
                _rows.Clear();
            }

            public IEnumerator<int> GetEnumerator()
            {
                return _rows.GetEnumerator();
            }

            [SerializeField]
            private List<int> _rows = new List<int>();

            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        [Serializable]
        private class ExpandedArray
        {
            public void Record(SerializedProperty property)
            {
                _isExpanded = property.isExpanded;
                _elements = new List<ExpandedElement>();
                for(var i = 0; i < property.arraySize; ++i)
                {
                    _elements.Add(new ExpandedElement(property.GetArrayElementAtIndex(i)));
                }
                _refreshRemapping = true;
            }

            public void Apply(SerializedProperty property)
            {
                if(_refreshRemapping)
                {
                    property.isExpanded = _isExpanded;
                    for(var i = 0; i < _elements.Count && i < property.arraySize; ++i)
                    {
                        _elements[i].Apply(property.GetArrayElementAtIndex(i));
                    }
                }
                _refreshRemapping = false;
            }

            public void RemoveAt(int index)
            {
                _elements.RemoveAt(index);
                _refreshRemapping = true;
            }

            public void Move(int from, int to)
            {
                _elements.Move(from, to);
                _refreshRemapping = true;
            }

            public void Add()
            {
                if(_elements.Count > 0)
                {
                    _elements.Add(_elements.Last().CreateCopy());
                }
                _refreshRemapping = true;
            }

            [SerializeField]
            private bool _isExpanded;
            [SerializeField]
            private List<ExpandedElement> _elements = new List<ExpandedElement>();
            [SerializeField]
            private bool _refreshRemapping;

            [Serializable]
            private class ExpandedElement
            {
                public ExpandedElement(SerializedProperty property)
                {
                    _isExpanded = property.isExpanded;
                    var rootPath = property.propertyPath;
                    while(property.Next(true) && property.propertyPath.StartsWith(rootPath))
                    {
                        _properties.Add(new ExpandedProperty
                        {
                            IsExpanded = property.isExpanded,
                            Path = property.propertyPath.Remove(0, rootPath.Length)
                        });
                    }
                }

                public void Apply(SerializedProperty property)
                {
                    property.isExpanded = _isExpanded;
                    var rootPath = property.propertyPath;
                    var dictionary = _properties.ToDictionary(p => p.Path, p => p.IsExpanded);
                    while(property.Next(true) && property.propertyPath.StartsWith(rootPath))
                    {
                        bool isExpanded;
                        if(dictionary.TryGetValue(property.propertyPath.Remove(0, rootPath.Length), out isExpanded))
                        {
                            property.isExpanded = isExpanded;
                        }
                    }
                }

                public ExpandedElement CreateCopy()
                {
                    var copy = new ExpandedElement
                    {
                        _isExpanded = _isExpanded
                    };
                    foreach(var property in _properties)
                    {
                        copy._properties.Add(new ExpandedProperty
                        {
                            IsExpanded = property.IsExpanded,
                            Path = property.Path
                        });
                    }
                    return copy;
                }

                [SerializeField]
                private bool _isExpanded;
                [SerializeField]
                private List<ExpandedProperty> _properties = new List<ExpandedProperty>();

                private ExpandedElement() { }

                [Serializable]
                private class ExpandedProperty
                {
                    public string Path;
                    public bool IsExpanded;
                }
            }
        }

        #endregion
    }
}