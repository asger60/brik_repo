﻿// ReSharper disable RedundantExtendsListEntry
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

namespace Assets.ReorderableList
{
    public abstract class ReorderableDictionary
    {
        private ReorderableDictionary() {}

        public class Of<TKey, TValue> : ReorderableDictionary, IEnumerable, ISerializable, ICollection, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IDictionary<TKey, TValue>, IDictionary, IDeserializationCallback, ISerializationCallbackReceiver
        {
            private Dictionary<TKey, TValue> _dictionary
            {
                get
                {
                    if(__dictionary == null)
                    {
                        __dictionary = new Dictionary<TKey, TValue>();
                        using(var key = _keys.GetEnumerator())
                        using(var value = _values.GetEnumerator())
                        {
                            while(key.MoveNext() && value.MoveNext())
                            {
                                __dictionary.Add(key.Current, value.Current);
                            }
                        }
                    }
                    return __dictionary;
                }
            }
            private Dictionary<TKey, TValue> __dictionary;

            [SerializeField]
            private List<TKey> _keys;
            [SerializeField]
            private List<TValue> _values;

            #region Forwarding Implementation

            ICollection<TKey> IDictionary<TKey, TValue>.Keys { get { return _dictionary.Keys; } }
            ICollection<TValue> IDictionary<TKey, TValue>.Values { get { return _dictionary.Values; } }
            ICollection IDictionary.Keys { get { return _dictionary.Keys; } }
            ICollection IDictionary.Values { get { return _dictionary.Values; } }
            bool IDictionary.IsFixedSize { get { return ((IDictionary) _dictionary).IsFixedSize; } }
            bool IDictionary.IsReadOnly { get { return ((IDictionary) _dictionary).IsReadOnly; } }
            bool ICollection.IsSynchronized { get { return ((ICollection) _dictionary).IsSynchronized; } }
            object ICollection.SyncRoot { get { return ((ICollection) _dictionary).SyncRoot; } }
            bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly { get { return ((ICollection<KeyValuePair<TKey, TValue>>) _dictionary).IsReadOnly; } }

            /// <summary>
            /// Gets the number of key/value pairs contained in the <see cref="T:System.Collections.Generic.Dictionary`2"/>.
            /// </summary>
            /// 
            /// <returns>
            /// The number of key/value pairs contained in the <see cref="T:System.Collections.Generic.Dictionary`2"/>.
            /// </returns>
            public int Count { get { return _dictionary.Count; } }

            /// <summary>
            /// Gets or sets the value associated with the specified key.
            /// </summary>
            /// 
            /// <returns>
            /// The value associated with the specified key. If the specified key is not found, a get operation throws a <see cref="T:System.Collections.Generic.KeyNotFoundException"/>, and a set operation creates a new element with the specified key.
            /// </returns>
            /// <param name="key">The key of the value to get or set.</param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null.</exception><exception cref="T:System.Collections.Generic.KeyNotFoundException">The property is retrieved and <paramref name="key"/> does not exist in the collection.</exception>
            public TValue this[TKey key]
            {
                get { return _dictionary[key]; }
                set { _dictionary[key] = value; }
            }

            /// <summary>
            /// Gets the <see cref="T:System.Collections.Generic.IEqualityComparer`1"/> that is used to determine equality of keys for the dictionary.
            /// </summary>
            /// 
            /// <returns>
            /// The <see cref="T:System.Collections.Generic.IEqualityComparer`1"/> generic interface implementation that is used to determine equality of keys for the current <see cref="T:System.Collections.Generic.Dictionary`2"/> and to provide hash values for the keys.
            /// </returns>
            public IEqualityComparer<TKey> Comparer { get { return _dictionary.Comparer; } }

            /// <summary>
            /// Gets a collection containing the keys in the <see cref="T:System.Collections.Generic.Dictionary`2"/>.
            /// </summary>
            /// 
            /// <returns>
            /// A <see cref="T:System.Collections.Generic.Dictionary`2.KeyCollection"/> containing the keys in the <see cref="T:System.Collections.Generic.Dictionary`2"/>.
            /// </returns>
            public Dictionary<TKey, TValue>.KeyCollection Keys { get { return _dictionary.Keys; } }

            /// <summary>
            /// Gets a collection containing the values in the <see cref="T:System.Collections.Generic.Dictionary`2"/>.
            /// </summary>
            /// 
            /// <returns>
            /// A <see cref="T:System.Collections.Generic.Dictionary`2.ValueCollection"/> containing the values in the <see cref="T:System.Collections.Generic.Dictionary`2"/>.
            /// </returns>
            public Dictionary<TKey, TValue>.ValueCollection Values { get { return _dictionary.Values; } }

            ///// <summary>
            ///// Initializes a new instance of the <see cref="T:System.Collections.Generic.Dictionary`2"/> class that is empty, has the default initial capacity, and uses the default equality comparer for the key type.
            ///// </summary>
            //public Dictionary()
            //{
            //}
            //
            ///// <summary>
            ///// Initializes a new instance of the <see cref="T:System.Collections.Generic.Dictionary`2"/> class that is empty, has the default initial capacity, and uses the specified <see cref="T:System.Collections.Generic.IEqualityComparer`1"/>.
            ///// </summary>
            ///// <param name="comparer">The <see cref="T:System.Collections.Generic.IEqualityComparer`1"/> implementation to use when comparing keys, or null to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"/> for the type of the key.</param>
            //public Dictionary(IEqualityComparer<TKey> comparer)
            //{
            //}
            //
            ///// <summary>
            ///// Initializes a new instance of the <see cref="T:System.Collections.Generic.Dictionary`2"/> class that contains elements copied from the specified <see cref="T:System.Collections.Generic.IDictionary`2"/> and uses the default equality comparer for the key type.
            ///// </summary>
            ///// <param name="dictionary">The <see cref="T:System.Collections.Generic.IDictionary`2"/> whose elements are copied to the new <see cref="T:System.Collections.Generic.Dictionary`2"/>.</param><exception cref="T:System.ArgumentNullException"><paramref name="dictionary"/> is null.</exception><exception cref="T:System.ArgumentException"><paramref name="dictionary"/> contains one or more duplicate keys.</exception>
            //public Dictionary(IDictionary<TKey, TValue> dictionary)
            //{
            //}
            //
            ///// <summary>
            ///// Initializes a new instance of the <see cref="T:System.Collections.Generic.Dictionary`2"/> class that is empty, has the specified initial capacity, and uses the default equality comparer for the key type.
            ///// </summary>
            ///// <param name="capacity">The initial number of elements that the <see cref="T:System.Collections.Generic.Dictionary`2"/> can contain.</param><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="capacity"/> is less than 0.</exception>
            //public Dictionary(int capacity)
            //{
            //}
            //
            ///// <summary>
            ///// Initializes a new instance of the <see cref="T:System.Collections.Generic.Dictionary`2"/> class that contains elements copied from the specified <see cref="T:System.Collections.Generic.IDictionary`2"/> and uses the specified <see cref="T:System.Collections.Generic.IEqualityComparer`1"/>.
            ///// </summary>
            ///// <param name="dictionary">The <see cref="T:System.Collections.Generic.IDictionary`2"/> whose elements are copied to the new <see cref="T:System.Collections.Generic.Dictionary`2"/>.</param><param name="comparer">The <see cref="T:System.Collections.Generic.IEqualityComparer`1"/> implementation to use when comparing keys, or null to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"/> for the type of the key.</param><exception cref="T:System.ArgumentNullException"><paramref name="dictionary"/> is null.</exception><exception cref="T:System.ArgumentException"><paramref name="dictionary"/> contains one or more duplicate keys.</exception>
            //public Dictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer)
            //{
            //}
            //
            ///// <summary>
            ///// Initializes a new instance of the <see cref="T:System.Collections.Generic.Dictionary`2"/> class that is empty, has the specified initial capacity, and uses the specified <see cref="T:System.Collections.Generic.IEqualityComparer`1"/>.
            ///// </summary>
            ///// <param name="capacity">The initial number of elements that the <see cref="T:System.Collections.Generic.Dictionary`2"/> can contain.</param><param name="comparer">The <see cref="T:System.Collections.Generic.IEqualityComparer`1"/> implementation to use when comparing keys, or null to use the default <see cref="T:System.Collections.Generic.EqualityComparer`1"/> for the type of the key.</param><exception cref="T:System.ArgumentOutOfRangeException"><paramref name="capacity"/> is less than 0.</exception>
            //public Dictionary(int capacity, IEqualityComparer<TKey> comparer)
            //{
            //}
            //
            ///// <summary>
            ///// Initializes a new instance of the <see cref="T:System.Collections.Generic.Dictionary`2"/> class with serialized data.
            ///// </summary>
            ///// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo"/> object containing the information required to serialize the <see cref="T:System.Collections.Generic.Dictionary`2"/>.</param><param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext"/> structure containing the source and destination of the serialized stream associated with the <see cref="T:System.Collections.Generic.Dictionary`2"/>.</param>
            //protected Dictionary(SerializationInfo info, StreamingContext context)
            //{
            //}

            object IDictionary.this[object key]
            {
                get { return ((IDictionary) _dictionary)[key]; }
                set { ((IDictionary) _dictionary)[key] = value; }
            }

            void IDictionary.Add(object key, object value)
            {
                ((IDictionary) _dictionary).Add(key, value);
            }

            bool IDictionary.Contains(object key)
            {
                return ((IDictionary) _dictionary).Contains(key);
            }

            void IDictionary.Remove(object key)
            {
                ((IDictionary) _dictionary).Remove(key);
            }

            void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> keyValuePair)
            {
                ((ICollection<KeyValuePair<TKey, TValue>>) _dictionary).Add(keyValuePair);
            }

            bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> keyValuePair)
            {
                return ((ICollection<KeyValuePair<TKey, TValue>>) _dictionary).Contains(keyValuePair);
            }

            void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int index)
            {
                ((ICollection<KeyValuePair<TKey, TValue>>) _dictionary).CopyTo(array, index);
            }

            bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> keyValuePair)
            {
                return ((ICollection<KeyValuePair<TKey, TValue>>) _dictionary).Remove(keyValuePair);
            }

            void ICollection.CopyTo(Array array, int index)
            {
                ((ICollection) _dictionary).CopyTo(array, index);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return ((IEnumerable) _dictionary).GetEnumerator();
            }

            IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
            {
                return ((IEnumerable<KeyValuePair<TKey, TValue>>) _dictionary).GetEnumerator();
            }

            IDictionaryEnumerator IDictionary.GetEnumerator()
            {
                return ((IDictionary) _dictionary).GetEnumerator();
            }

            /// <summary>
            /// Adds the specified key and value to the dictionary.
            /// </summary>
            /// <param name="key">The key of the element to add.</param><param name="value">The value of the element to add. The value can be null for reference types.</param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null.</exception><exception cref="T:System.ArgumentException">An element with the same key already exists in the <see cref="T:System.Collections.Generic.Dictionary`2"/>.</exception>
            public void Add(TKey key, TValue value)
            {
                _dictionary.Add(key, value);
            }

            /// <summary>
            /// Removes all keys and values from the <see cref="T:System.Collections.Generic.Dictionary`2"/>.
            /// </summary>
            public void Clear()
            {
                _dictionary.Clear();
            }

            /// <summary>
            /// Determines whether the <see cref="T:System.Collections.Generic.Dictionary`2"/> contains the specified key.
            /// </summary>
            /// 
            /// <returns>
            /// true if the <see cref="T:System.Collections.Generic.Dictionary`2"/> contains an element with the specified key; otherwise, false.
            /// </returns>
            /// <param name="key">The key to locate in the <see cref="T:System.Collections.Generic.Dictionary`2"/>.</param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null.</exception>
            public bool ContainsKey(TKey key)
            {
                return _dictionary.ContainsKey(key);
            }

            /// <summary>
            /// Determines whether the <see cref="T:System.Collections.Generic.Dictionary`2"/> contains a specific value.
            /// </summary>
            /// 
            /// <returns>
            /// true if the <see cref="T:System.Collections.Generic.Dictionary`2"/> contains an element with the specified value; otherwise, false.
            /// </returns>
            /// <param name="value">The value to locate in the <see cref="T:System.Collections.Generic.Dictionary`2"/>. The value can be null for reference types.</param>
            public bool ContainsValue(TValue value)
            {
                return _dictionary.ContainsValue(value);
            }

            /// <summary>
            /// Implements the <see cref="T:System.Runtime.Serialization.ISerializable"/> interface and returns the data needed to serialize the <see cref="T:System.Collections.Generic.Dictionary`2"/> instance.
            /// </summary>
            /// <param name="info">A <see cref="T:System.Runtime.Serialization.SerializationInfo"/> object that contains the information required to serialize the <see cref="T:System.Collections.Generic.Dictionary`2"/> instance.</param><param name="context">A <see cref="T:System.Runtime.Serialization.StreamingContext"/> structure that contains the source and destination of the serialized stream associated with the <see cref="T:System.Collections.Generic.Dictionary`2"/> instance.</param><exception cref="T:System.ArgumentNullException"><paramref name="info"/> is null.</exception>
            public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
            {
                _dictionary.GetObjectData(info, context);
            }

            /// <summary>
            /// Implements the <see cref="T:System.Runtime.Serialization.ISerializable"/> interface and raises the deserialization event when the deserialization is complete.
            /// </summary>
            /// <param name="sender">The source of the deserialization event.</param><exception cref="T:System.Runtime.Serialization.SerializationException">The <see cref="T:System.Runtime.Serialization.SerializationInfo"/> object associated with the current <see cref="T:System.Collections.Generic.Dictionary`2"/> instance is invalid.</exception>
            public virtual void OnDeserialization(object sender)
            {
                _dictionary.OnDeserialization(sender);
            }

            /// <summary>
            /// Removes the value with the specified key from the <see cref="T:System.Collections.Generic.Dictionary`2"/>.
            /// </summary>
            /// 
            /// <returns>
            /// true if the element is successfully found and removed; otherwise, false.  This method returns false if <paramref name="key"/> is not found in the <see cref="T:System.Collections.Generic.Dictionary`2"/>.
            /// </returns>
            /// <param name="key">The key of the element to remove.</param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null.</exception>
            public bool Remove(TKey key)
            {
                return _dictionary.Remove(key);
            }

            /// <summary>
            /// Gets the value associated with the specified key.
            /// </summary>
            /// 
            /// <returns>
            /// true if the <see cref="T:System.Collections.Generic.Dictionary`2"/> contains an element with the specified key; otherwise, false.
            /// </returns>
            /// <param name="key">The key of the value to get.</param><param name="value">When this method returns, contains the value associated with the specified key, if the key is found; otherwise, the default value for the type of the <paramref name="value"/> parameter. This parameter is passed uninitialized.</param><exception cref="T:System.ArgumentNullException"><paramref name="key"/> is null.</exception>
            public bool TryGetValue(TKey key, out TValue value)
            {
                return _dictionary.TryGetValue(key, out value);
            }

            /// <summary>
            /// Returns an enumerator that iterates through the <see cref="T:System.Collections.Generic.Dictionary`2"/>.
            /// </summary>
            /// 
            /// <returns>
            /// A <see cref="T:System.Collections.Generic.Dictionary`2.Enumerator"/> structure for the <see cref="T:System.Collections.Generic.Dictionary`2"/>.
            /// </returns>
            public Dictionary<TKey, TValue>.Enumerator GetEnumerator()
            {
                return _dictionary.GetEnumerator();
            }

            #endregion

            void ISerializationCallbackReceiver.OnBeforeSerialize()
            {
            }

            void ISerializationCallbackReceiver.OnAfterDeserialize()
            {
            }
        }
    }
}
// ReSharper restore RedundantExtendsListEntry