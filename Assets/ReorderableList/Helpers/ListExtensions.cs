﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * 2015/08/30 - First version. 
 * **********************************************************/

using System.Collections;
using System.Collections.Generic;

namespace Assets.ReorderableList.Helpers
{
    public static class ListExtensions
    {
        public static ReorderableList.Of<T>.Adaptor As<T>(this List<T> list)
        {
            return new ReorderableList.Of<T>.Adaptor(list);
        }

        public static TList Move<TList>(this TList list, int from, int to)
            where TList : IList
        {
            var item = list[from];
            list.RemoveAt(from);
            list.Insert(to, item);
            return list;
        }
    }
}