﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * 2015/08/30 - First version. 
 * **********************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Assets.ReorderableList.Helpers
{
    public class ListSerializationHelper
    {
        public static void TransferLists(object obj)
        {
            new ListSerializationHelper().TraverseObject(obj);
        }

        private readonly HashSet<object> _visited = new HashSet<object>();

        private ListSerializationHelper() { }

        private void TraverseObject(object obj)
        {
            if(obj == null || _visited.Contains(obj))
            {
                return;
            }
            _visited.Add(obj);

            var fields = obj.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).ToDictionary(f => f.Name, f => f);
            foreach(var fieldInfo in fields.Values)
            {
                if(typeof(ReorderableList).IsAssignableFrom(fieldInfo.FieldType))
                {
                    var transferAttribute = fieldInfo.GetCustomAttributes(typeof(TransferListAttribute), true)
                        .OfType<TransferListAttribute>()
                        .FirstOrDefault(t => !string.IsNullOrEmpty(t.SourceListName));
                    if(transferAttribute != null)
                    {
                        FieldInfo sourceFieldInfo;
                        if(fields.TryGetValue(transferAttribute.SourceListName, out sourceFieldInfo))
                        {
                            Transfer(obj, sourceFieldInfo, fieldInfo, transferAttribute.ClearFirst);
                        }
                        else
                        {
                            Debug.LogWarning(string.Format("Could not find field '{0}'.", transferAttribute.SourceListName));
                        }
                    }
                }

                TraverseObject(fieldInfo.GetValue(obj));
            }
        }

        private static void Transfer(object instance, FieldInfo sourceInfo, FieldInfo destinationInfo, bool clearFirst)
        {
            if(!typeof(IEnumerable).IsAssignableFrom(sourceInfo.FieldType))
            {
                Debug.LogWarning(string.Format("Source field '{0}' is not an enumerable type.", sourceInfo.Name));
            }

            var source = sourceInfo.GetValue(instance) as IEnumerable;
            if(source == null)
            {
                return;
            }

            var destination = destinationInfo.GetValue(instance);
            if(destination == null)
            {
                destination = Activator.CreateInstance(destinationInfo.FieldType);
                destinationInfo.SetValue(instance, destination);
            }

            if(clearFirst)
            {
                var clearMethod = destinationInfo.FieldType.GetMethod("Clear");
                if(clearMethod == null)
                {
                    throw new Exception(string.Format("Could not find 'Clear' method in '{0}'.", destinationInfo.FieldType.Name));
                }
                clearMethod.Invoke(destination, null);
            }

            var addMethod = destinationInfo.FieldType.GetMethod("Add");
            if(addMethod == null)
            {
                throw new Exception(string.Format("Could not find 'Add' method in '{0}'.", destinationInfo.FieldType.Name));
            }

            foreach(var sourceElement in source)
            {
                addMethod.Invoke(destination, new[] { sourceElement });
            }
        }
    }
}