﻿using System;

namespace Assets.ReorderableList.Helpers
{
    [AttributeUsage(AttributeTargets.Field)]
    public class TransferListAttribute : Attribute
    {
        public readonly string SourceListName;
        public bool ClearFirst = true;

        public TransferListAttribute(string sourceListName, bool clearFirst = true)
        {
            SourceListName = sourceListName;
            ClearFirst = clearFirst;
        }
    }
}