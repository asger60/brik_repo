﻿/************************************************************
 * Roman Issa (Unity Forums: Roland1234)
 * Use as you like - credit where due would be appreciated :D
 * 
 * 2015/08/30 - First version. 
 * **********************************************************/

using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.ReorderableList
{
    [Serializable]
    public class ListOfInt : ReorderableList.Of<int> { }

    [Serializable]
    public class ListOfBool : ReorderableList.Of<bool> { }

    [Serializable]
    public class ListOfFloat : ReorderableList.Of<float> { }

    [Serializable]
    public class ListOfString : ReorderableList.Of<string> { }

    [Serializable]
    public class ListOfObject : ReorderableList.Of<Object> { }

    [Serializable]
    public class ListOfGameObject : ReorderableList.Of<GameObject> { }

    [Serializable]
    public class ListOfMonoBehaviour : ReorderableList.Of<MonoBehaviour> { }

    [Serializable]
    public class ListOfColor : ReorderableList.Of<Color> { }

    [Serializable]
    public class ListOfLayerMas : ReorderableList.Of<LayerMask> { }

    [Serializable]
    public class ListOfVector2 : ReorderableList.Of<Vector2> { }

    [Serializable]
    public class ListOfVector3 : ReorderableList.Of<Vector3> { }

    [Serializable]
    public class ListOfVector4 : ReorderableList.Of<Vector4> { }

    [Serializable]
    public class ListOfRect : ReorderableList.Of<Rect> { }

    [Serializable]
    public class ListOfChar : ReorderableList.Of<char> { }

    [Serializable]
    public class ListOfAnimationCurve : ReorderableList.Of<AnimationCurve> { }

    [Serializable]
    public class ListOfBounds : ReorderableList.Of<Bounds> { }

    [Serializable]
    public class ListOfGradient : ReorderableList.Of<Gradient> { }

    [Serializable]
    public class ListOfQuaternion : ReorderableList.Of<Quaternion> { }
}
